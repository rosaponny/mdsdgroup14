package se.chalmers.cse.mdsd1617.yakindu;

public class Assignment3Complete {
	private long currentBookingId = 0;
	private long currentReservationNumber = 0;
	private final int MAX_ROOMS = 5;
	private long[] roomBooked = new long[MAX_ROOMS];
	private long[] checkedInRooms = new long[MAX_ROOMS];
	
	 

	

	public long initiateBooking() {
		return ++currentBookingId;
	}

	public boolean addRoomToBooking(long bookingId) {
		System.out.println("add");
		if (bookingId < 1 || bookingId > currentBookingId) {
			return false;
		} else if (currentReservationNumber >= MAX_ROOMS) {
			return false;
		} else {
			++currentReservationNumber;
			return true;
		}
	}
	//Assigns a room to a booking id.
	public long doAssignFreeRoom(long bookingId) {
		for(int i = 0; i< roomBooked.length; i++) {
			if(roomBooked[i] == 0) {
				roomBooked[i] = bookingId;
				return i;
			}
		}
		return -1;	
	}
	//Returns the id of the first room booked by an user
	public long doCheckInRoom(long bookingId) {
		for(int i = 0; i< roomBooked.length; i++) {
			if(roomBooked[i] == bookingId && checkedInRooms[i] == 0) {
				checkedInRooms[i] = bookingId;
				return i;
			}
		}
		return -1;
	}
	
	public long doCheckInSpecifiedRoom(long bookingId, long roomNumber) {
		if (roomBooked[(int)roomNumber] == bookingId){
			checkedInRooms[(int)roomNumber] = bookingId;
			return roomNumber;
		}
	
		return -1;
	}
	
	//Returns the id of the first room to be checked out.
	public long doCheckOutRoom(long bookingId) {
		for(int i = 0; i< roomBooked.length; i++) {
			if(roomBooked[i] == bookingId && checkedInRooms[i] == bookingId) {
				checkedInRooms[i] = 0;
				roomBooked[i] = 0;
				currentReservationNumber--;
				return i;
			}
		}
		return -1;
	}
	
	public long doCheckOutSpecifiedRoom(long bookingId, long roomNumber) {
		if (checkedInRooms[(int)roomNumber] == bookingId){
			checkedInRooms[(int)roomNumber] = 0;
			roomBooked[(int)roomNumber] = 0;
			currentReservationNumber--;
			return roomNumber;
		}
	
		return -1;
	}
	
	public boolean hasLeftAllRooms(long bookingId) {
		for (long id : roomBooked){
			if (bookingId == id){
				return false;
			}
		}
		for (long id : checkedInRooms){
			if (bookingId == id){
				return false;
			}
		}
		return true;
	}
	
	public boolean hasCheckedInAll(long bookingId, long nbrOfRooms) {
		long rooms = 0;
		for (long id : checkedInRooms){
			if (bookingId == id){
				rooms++;
			}
		}
		return rooms == nbrOfRooms;
	}
}
