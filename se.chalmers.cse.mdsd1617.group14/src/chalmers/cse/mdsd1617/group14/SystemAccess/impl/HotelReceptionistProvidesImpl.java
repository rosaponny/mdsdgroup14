/**
 */
package chalmers.cse.mdsd1617.group14.SystemAccess.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple;
import chalmers.cse.mdsd1617.group14.SystemAccess.HotelReceptionistProvides;
import chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Receptionist Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class HotelReceptionistProvidesImpl extends HotelCustomerProvidesImpl implements HotelReceptionistProvides {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected HotelReceptionistProvidesImpl() {
		super();
		ibookingprovider  = chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingFactory.eINSTANCE.createBookingProvider();
		iroomprovider     = chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomFactory.eINSTANCE.createRoomProvider();
		iroomtypeprovider = chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeFactory.eINSTANCE.createRoomTypeProvider();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SystemAccessPackage.Literals.HOTEL_RECEPTIONIST_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<TwoIntTuple> getOccupiedRooms(String occupiedDate) {
		return ibookingprovider.getOccupiedRooms(occupiedDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoomFromBooking(String roomTypeDescription, int bookingID) {
		if (!ibookingprovider.isValid(bookingID)) return false;
		return ibookingprovider.removeRoomFromBooking(roomTypeDescription, bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateNameOfBooking(int bookingID, String firstName, String lastName) {
		if (!ibookingprovider.isValid(bookingID)) return false;
		return ibookingprovider.updateNameOfBooking(bookingID, firstName, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateDateOfBooking(int bookingID, String startDate, String endDate) {
		if (!ibookingprovider.isValid(bookingID)) return false;
		if (startDate.compareTo(endDate)>0) return false;
		return ibookingprovider.updateDateOfBooking(bookingID, startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean cancelBooking(int bookingID) {
		if (!ibookingprovider.isValid(bookingID)) return false;
		return ibookingprovider.cancelBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkOutBooking(int bookingID) {
		if (!ibookingprovider.isValid(bookingID)) return false;
		return ibookingprovider.checkOutBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> getBooking(String roomTypeDescription, String startDate, String endDate) {
		if (startDate.compareTo(endDate)>0) return new BasicEList<Integer>();
		return ibookingprovider.getBooking(roomTypeDescription, startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> listCheckIns(String day) {
		return ibookingprovider.listCheckIns(day);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> listCheckOuts(String day) {
		return ibookingprovider.listCheckOuts(day);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkInBooking(int bookingID) {
		if (!ibookingprovider.isValid(bookingID)) return false;
		return ibookingprovider.checkInBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addExtraCostToRoom(int bookingID, int roomNumber, String roomTypeDescription, int price) {
		if (!ibookingprovider.isValid(bookingID)) return false;
		return ibookingprovider.addExtraCostToRoom(bookingID, roomNumber, roomTypeDescription, price);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<String> listBookings() {
		return ibookingprovider.listBookings();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> listCheckOuts(String startDate, String endDate) {
		return ibookingprovider.listCheckOuts(startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> listCheckIns(String startDate, String endDate) {
		return ibookingprovider.listCheckIns(startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case SystemAccessPackage.HOTEL_RECEPTIONIST_PROVIDES___GET_OCCUPIED_ROOMS__STRING:
				return getOccupiedRooms((String)arguments.get(0));
			case SystemAccessPackage.HOTEL_RECEPTIONIST_PROVIDES___REMOVE_ROOM_FROM_BOOKING__STRING_INT:
				return removeRoomFromBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case SystemAccessPackage.HOTEL_RECEPTIONIST_PROVIDES___UPDATE_NAME_OF_BOOKING__INT_STRING_STRING:
				return updateNameOfBooking((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case SystemAccessPackage.HOTEL_RECEPTIONIST_PROVIDES___UPDATE_DATE_OF_BOOKING__INT_STRING_STRING:
				return updateDateOfBooking((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case SystemAccessPackage.HOTEL_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT:
				return cancelBooking((Integer)arguments.get(0));
			case SystemAccessPackage.HOTEL_RECEPTIONIST_PROVIDES___CHECK_OUT_BOOKING__INT:
				return checkOutBooking((Integer)arguments.get(0));
			case SystemAccessPackage.HOTEL_RECEPTIONIST_PROVIDES___GET_BOOKING__STRING_STRING_STRING:
				return getBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case SystemAccessPackage.HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__STRING:
				return listCheckIns((String)arguments.get(0));
			case SystemAccessPackage.HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__STRING:
				return listCheckOuts((String)arguments.get(0));
			case SystemAccessPackage.HOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_BOOKING__INT:
				return checkInBooking((Integer)arguments.get(0));
			case SystemAccessPackage.HOTEL_RECEPTIONIST_PROVIDES___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_INT:
				return addExtraCostToRoom((Integer)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3));
			case SystemAccessPackage.HOTEL_RECEPTIONIST_PROVIDES___LIST_BOOKINGS:
				return listBookings();
			case SystemAccessPackage.HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__STRING_STRING:
				return listCheckOuts((String)arguments.get(0), (String)arguments.get(1));
			case SystemAccessPackage.HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__STRING_STRING:
				return listCheckIns((String)arguments.get(0), (String)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelReceptionistProvidesImpl
