/**
 */
package chalmers.cse.mdsd1617.group14.SystemAccess.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider;

import chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.Feature;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider;

import chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides;
import chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelStartupProvidesImpl#getIroomprovider <em>Iroomprovider</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelStartupProvidesImpl#getIroomtypeprovider <em>Iroomtypeprovider</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelStartupProvidesImpl#getIbookingprovider <em>Ibookingprovider</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelStartupProvidesImpl extends MinimalEObjectImpl.Container implements HotelStartupProvides {
	/**
	 * The cached value of the '{@link #getIroomprovider() <em>Iroomprovider</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroomprovider()
	 * @generated
	 * @ordered
	 */
	protected IRoomProvider iroomprovider;

	/**
	 * The cached value of the '{@link #getIroomtypeprovider() <em>Iroomtypeprovider</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroomtypeprovider()
	 * @generated
	 * @ordered
	 */
	protected IRoomTypeProvider iroomtypeprovider;

	/**
	 * The cached value of the '{@link #getIbookingprovider() <em>Ibookingprovider</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIbookingprovider()
	 * @generated
	 * @ordered
	 */
	protected IBookingProvider ibookingprovider;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected HotelStartupProvidesImpl() {
		super();
		ibookingprovider  = chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingFactory.eINSTANCE.createBookingProvider();
		iroomprovider     = chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomFactory.eINSTANCE.createRoomProvider();
		iroomtypeprovider = chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeFactory.eINSTANCE.createRoomTypeProvider();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SystemAccessPackage.Literals.HOTEL_STARTUP_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomProvider getIroomprovider() {
		if (iroomprovider != null && iroomprovider.eIsProxy()) {
			InternalEObject oldIroomprovider = (InternalEObject)iroomprovider;
			iroomprovider = (IRoomProvider)eResolveProxy(oldIroomprovider);
			if (iroomprovider != oldIroomprovider) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IROOMPROVIDER, oldIroomprovider, iroomprovider));
			}
		}
		return iroomprovider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomProvider basicGetIroomprovider() {
		return iroomprovider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIroomprovider(IRoomProvider newIroomprovider) {
		IRoomProvider oldIroomprovider = iroomprovider;
		iroomprovider = newIroomprovider;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IROOMPROVIDER, oldIroomprovider, iroomprovider));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeProvider getIroomtypeprovider() {
		if (iroomtypeprovider != null && iroomtypeprovider.eIsProxy()) {
			InternalEObject oldIroomtypeprovider = (InternalEObject)iroomtypeprovider;
			iroomtypeprovider = (IRoomTypeProvider)eResolveProxy(oldIroomtypeprovider);
			if (iroomtypeprovider != oldIroomtypeprovider) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IROOMTYPEPROVIDER, oldIroomtypeprovider, iroomtypeprovider));
			}
		}
		return iroomtypeprovider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeProvider basicGetIroomtypeprovider() {
		return iroomtypeprovider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIroomtypeprovider(IRoomTypeProvider newIroomtypeprovider) {
		IRoomTypeProvider oldIroomtypeprovider = iroomtypeprovider;
		iroomtypeprovider = newIroomtypeprovider;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IROOMTYPEPROVIDER, oldIroomtypeprovider, iroomtypeprovider));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookingProvider getIbookingprovider() {
		if (ibookingprovider != null && ibookingprovider.eIsProxy()) {
			InternalEObject oldIbookingprovider = (InternalEObject)ibookingprovider;
			ibookingprovider = (IBookingProvider)eResolveProxy(oldIbookingprovider);
			if (ibookingprovider != oldIbookingprovider) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IBOOKINGPROVIDER, oldIbookingprovider, ibookingprovider));
			}
		}
		return ibookingprovider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookingProvider basicGetIbookingprovider() {
		return ibookingprovider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIbookingprovider(IBookingProvider newIbookingprovider) {
		IBookingProvider oldIbookingprovider = ibookingprovider;
		ibookingprovider = newIbookingprovider;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IBOOKINGPROVIDER, oldIbookingprovider, ibookingprovider));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void startup(int numRooms) {
		iroomprovider.reset();
		iroomtypeprovider.reset();
		ibookingprovider.reset();
		String roomTypeDescription = "Default";
		iroomtypeprovider.createRoomType(roomTypeDescription, 1000, 2, (new BasicEList<Feature>()));
		
		for (int i = 0; i < numRooms; i++) {
			iroomprovider.createRoom(i);
			iroomtypeprovider.setTypeOfRoom(roomTypeDescription, i);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IROOMPROVIDER:
				if (resolve) return getIroomprovider();
				return basicGetIroomprovider();
			case SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IROOMTYPEPROVIDER:
				if (resolve) return getIroomtypeprovider();
				return basicGetIroomtypeprovider();
			case SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IBOOKINGPROVIDER:
				if (resolve) return getIbookingprovider();
				return basicGetIbookingprovider();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IROOMPROVIDER:
				setIroomprovider((IRoomProvider)newValue);
				return;
			case SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IROOMTYPEPROVIDER:
				setIroomtypeprovider((IRoomTypeProvider)newValue);
				return;
			case SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IBOOKINGPROVIDER:
				setIbookingprovider((IBookingProvider)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IROOMPROVIDER:
				setIroomprovider((IRoomProvider)null);
				return;
			case SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IROOMTYPEPROVIDER:
				setIroomtypeprovider((IRoomTypeProvider)null);
				return;
			case SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IBOOKINGPROVIDER:
				setIbookingprovider((IBookingProvider)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IROOMPROVIDER:
				return iroomprovider != null;
			case SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IROOMTYPEPROVIDER:
				return iroomtypeprovider != null;
			case SystemAccessPackage.HOTEL_STARTUP_PROVIDES__IBOOKINGPROVIDER:
				return ibookingprovider != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case SystemAccessPackage.HOTEL_STARTUP_PROVIDES___STARTUP__INT:
				startup((Integer)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelStartupProvidesImpl
