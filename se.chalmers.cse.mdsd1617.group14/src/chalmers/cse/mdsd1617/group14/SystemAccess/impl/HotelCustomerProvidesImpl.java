/**
 */
package chalmers.cse.mdsd1617.group14.SystemAccess.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.StringIntTuple;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeFactory;
import chalmers.cse.mdsd1617.group14.SystemAccess.HotelCustomerProvides;
import chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Customer Provides</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelCustomerProvidesImpl#getIroomprovider <em>Iroomprovider</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelCustomerProvidesImpl#getIroomtypeprovider <em>Iroomtypeprovider</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelCustomerProvidesImpl#getIbookingprovider <em>Ibookingprovider</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelCustomerProvidesImpl extends MinimalEObjectImpl.Container implements HotelCustomerProvides {
	/**
	 * The cached value of the '{@link #getIroomprovider() <em>Iroomprovider</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroomprovider()
	 * @generated
	 * @ordered
	 */
	protected IRoomProvider iroomprovider;

	/**
	 * The cached value of the '{@link #getIroomtypeprovider() <em>Iroomtypeprovider</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroomtypeprovider()
	 * @generated
	 * @ordered
	 */
	protected IRoomTypeProvider iroomtypeprovider;

	/**
	 * The cached value of the '{@link #getIbookingprovider() <em>Ibookingprovider</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIbookingprovider()
	 * @generated
	 * @ordered
	 */
	protected IBookingProvider ibookingprovider;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected HotelCustomerProvidesImpl() {
		super();
		ibookingprovider  = chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingFactory.eINSTANCE.createBookingProvider();
		iroomprovider     = chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomFactory.eINSTANCE.createRoomProvider();
		iroomtypeprovider = chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeFactory.eINSTANCE.createRoomTypeProvider();
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SystemAccessPackage.Literals.HOTEL_CUSTOMER_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomProvider getIroomprovider() {
		if (iroomprovider != null && iroomprovider.eIsProxy()) {
			InternalEObject oldIroomprovider = (InternalEObject)iroomprovider;
			iroomprovider = (IRoomProvider)eResolveProxy(oldIroomprovider);
			if (iroomprovider != oldIroomprovider) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IROOMPROVIDER, oldIroomprovider, iroomprovider));
			}
		}
		return iroomprovider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomProvider basicGetIroomprovider() {
		return iroomprovider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIroomprovider(IRoomProvider newIroomprovider) {
		IRoomProvider oldIroomprovider = iroomprovider;
		iroomprovider = newIroomprovider;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IROOMPROVIDER, oldIroomprovider, iroomprovider));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeProvider getIroomtypeprovider() {
		if (iroomtypeprovider != null && iroomtypeprovider.eIsProxy()) {
			InternalEObject oldIroomtypeprovider = (InternalEObject)iroomtypeprovider;
			iroomtypeprovider = (IRoomTypeProvider)eResolveProxy(oldIroomtypeprovider);
			if (iroomtypeprovider != oldIroomtypeprovider) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IROOMTYPEPROVIDER, oldIroomtypeprovider, iroomtypeprovider));
			}
		}
		return iroomtypeprovider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeProvider basicGetIroomtypeprovider() {
		return iroomtypeprovider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIroomtypeprovider(IRoomTypeProvider newIroomtypeprovider) {
		IRoomTypeProvider oldIroomtypeprovider = iroomtypeprovider;
		iroomtypeprovider = newIroomtypeprovider;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IROOMTYPEPROVIDER, oldIroomtypeprovider, iroomtypeprovider));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookingProvider getIbookingprovider() {
		if (ibookingprovider != null && ibookingprovider.eIsProxy()) {
			InternalEObject oldIbookingprovider = (InternalEObject)ibookingprovider;
			ibookingprovider = (IBookingProvider)eResolveProxy(oldIbookingprovider);
			if (ibookingprovider != oldIbookingprovider) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IBOOKINGPROVIDER, oldIbookingprovider, ibookingprovider));
			}
		}
		return ibookingprovider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public IBookingProvider basicGetIbookingprovider() {
		return ibookingprovider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIbookingprovider(IBookingProvider newIbookingprovider) {
		IBookingProvider oldIbookingprovider = ibookingprovider;
		ibookingprovider = newIbookingprovider;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IBOOKINGPROVIDER, oldIbookingprovider, ibookingprovider));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<FreeRoomTypesDTO> getFreeRooms(int numberOfBeds, String startDate, String endDate) {
		if (startDate.compareTo(endDate)>0) return new BasicEList<FreeRoomTypesDTO>();
		 EList<StringIntTuple> sit = ibookingprovider.getFreeRooms(startDate, endDate, numberOfBeds);
		 EList<FreeRoomTypesDTO> freeRoomTypes = new BasicEList<FreeRoomTypesDTO>();
		 for(StringIntTuple s:sit){
			 FreeRoomTypesDTO dto = (FreeRoomTypesDTO) RoomTypeFactory.eINSTANCE.createFreeRoomTypesDTO();
			 dto.init(s.getS());
			 dto.setNumFreeRooms(s.getI());
			 freeRoomTypes.add(dto);
		 }
		 return freeRoomTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {
		if (firstName == null || startDate == null || endDate == null || lastName == null) return -1;
		if (firstName == ""   || startDate == ""   || endDate == ""   || lastName == ""  ) return -1;
		if (startDate.compareTo(endDate)>0) return -1;
		return ibookingprovider.initiateBooking(firstName, lastName, startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		if (!ibookingprovider.isValid(bookingID)) return false;
		if (!iroomtypeprovider.isValid(roomTypeDescription)) return false;
		return ibookingprovider.addRoomToBooking(roomTypeDescription, bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		if (!ibookingprovider.isValid(bookingID)) return false;
		return ibookingprovider.confirmBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateCheckout(int bookingID) {	
		ibookingprovider.checkOutBooking(bookingID);
		double priceForBooking= ibookingprovider.getTabOfBooking(bookingID).calculateTotal();
		return priceForBooking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		return ibookingprovider.payDuringCheckout(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateRoomCheckout(int roomNumber, int bookingId) {
		ibookingprovider.checkOutRoom(bookingId, roomNumber);
		double priceForRoom= ibookingprovider.getTabOfRoom(bookingId, roomNumber).calculateTotal();
		return priceForRoom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		if (!iroomprovider.isValid(roomNumber)) return false;
		return ibookingprovider.payRoomDuringCheckout(roomNumber, ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(String roomTypeDescription, int bookingId) {
		if (!iroomtypeprovider.isValid(roomTypeDescription)) return -1;
		if (!ibookingprovider.isValid(bookingId))            return -1;
		return ibookingprovider.checkInRoom(bookingId, roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IROOMPROVIDER:
				if (resolve) return getIroomprovider();
				return basicGetIroomprovider();
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IROOMTYPEPROVIDER:
				if (resolve) return getIroomtypeprovider();
				return basicGetIroomtypeprovider();
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IBOOKINGPROVIDER:
				if (resolve) return getIbookingprovider();
				return basicGetIbookingprovider();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IROOMPROVIDER:
				setIroomprovider((IRoomProvider)newValue);
				return;
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IROOMTYPEPROVIDER:
				setIroomtypeprovider((IRoomTypeProvider)newValue);
				return;
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IBOOKINGPROVIDER:
				setIbookingprovider((IBookingProvider)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IROOMPROVIDER:
				setIroomprovider((IRoomProvider)null);
				return;
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IROOMTYPEPROVIDER:
				setIroomtypeprovider((IRoomTypeProvider)null);
				return;
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IBOOKINGPROVIDER:
				setIbookingprovider((IBookingProvider)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IROOMPROVIDER:
				return iroomprovider != null;
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IROOMTYPEPROVIDER:
				return iroomtypeprovider != null;
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES__IBOOKINGPROVIDER:
				return ibookingprovider != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING:
				return getFreeRooms((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelCustomerProvidesImpl
