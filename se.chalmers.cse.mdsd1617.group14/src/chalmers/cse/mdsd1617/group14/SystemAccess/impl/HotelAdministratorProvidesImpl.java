/**
 */
package chalmers.cse.mdsd1617.group14.SystemAccess.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider;

import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.Feature;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider;

import chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides;
import chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides;
import chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Administrator Provides</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelAdministratorProvidesImpl#getIroomprovider <em>Iroomprovider</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelAdministratorProvidesImpl#getIroomtypeprovider <em>Iroomtypeprovider</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelAdministratorProvidesImpl#getHotelstartupprovides <em>Hotelstartupprovides</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelAdministratorProvidesImpl extends MinimalEObjectImpl.Container implements HotelAdministratorProvides {
	/**
	 * The cached value of the '{@link #getIroomprovider() <em>Iroomprovider</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroomprovider()
	 * @generated
	 * @ordered
	 */
	protected IRoomProvider iroomprovider;

	/**
	 * The cached value of the '{@link #getIroomtypeprovider() <em>Iroomtypeprovider</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroomtypeprovider()
	 * @generated
	 * @ordered
	 */
	protected IRoomTypeProvider iroomtypeprovider;

	/**
	 * The cached value of the '{@link #getHotelstartupprovides() <em>Hotelstartupprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHotelstartupprovides()
	 * @generated
	 * @ordered
	 */
	protected HotelStartupProvides hotelstartupprovides;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected HotelAdministratorProvidesImpl() {
		super();
		hotelstartupprovides  = chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessFactory.eINSTANCE.createHotelStartupProvides();
		iroomprovider         = chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomFactory.eINSTANCE.createRoomProvider();
		iroomtypeprovider     = chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeFactory.eINSTANCE.createRoomTypeProvider();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SystemAccessPackage.Literals.HOTEL_ADMINISTRATOR_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomProvider getIroomprovider() {
		if (iroomprovider != null && iroomprovider.eIsProxy()) {
			InternalEObject oldIroomprovider = (InternalEObject)iroomprovider;
			iroomprovider = (IRoomProvider)eResolveProxy(oldIroomprovider);
			if (iroomprovider != oldIroomprovider) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__IROOMPROVIDER, oldIroomprovider, iroomprovider));
			}
		}
		return iroomprovider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomProvider basicGetIroomprovider() {
		return iroomprovider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIroomprovider(IRoomProvider newIroomprovider) {
		IRoomProvider oldIroomprovider = iroomprovider;
		iroomprovider = newIroomprovider;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__IROOMPROVIDER, oldIroomprovider, iroomprovider));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeProvider getIroomtypeprovider() {
		if (iroomtypeprovider != null && iroomtypeprovider.eIsProxy()) {
			InternalEObject oldIroomtypeprovider = (InternalEObject)iroomtypeprovider;
			iroomtypeprovider = (IRoomTypeProvider)eResolveProxy(oldIroomtypeprovider);
			if (iroomtypeprovider != oldIroomtypeprovider) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__IROOMTYPEPROVIDER, oldIroomtypeprovider, iroomtypeprovider));
			}
		}
		return iroomtypeprovider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeProvider basicGetIroomtypeprovider() {
		return iroomtypeprovider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIroomtypeprovider(IRoomTypeProvider newIroomtypeprovider) {
		IRoomTypeProvider oldIroomtypeprovider = iroomtypeprovider;
		iroomtypeprovider = newIroomtypeprovider;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__IROOMTYPEPROVIDER, oldIroomtypeprovider, iroomtypeprovider));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelStartupProvides getHotelstartupprovides() {
		if (hotelstartupprovides != null && hotelstartupprovides.eIsProxy()) {
			InternalEObject oldHotelstartupprovides = (InternalEObject)hotelstartupprovides;
			hotelstartupprovides = (HotelStartupProvides)eResolveProxy(oldHotelstartupprovides);
			if (hotelstartupprovides != oldHotelstartupprovides) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__HOTELSTARTUPPROVIDES, oldHotelstartupprovides, hotelstartupprovides));
			}
		}
		return hotelstartupprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelStartupProvides basicGetHotelstartupprovides() {
		return hotelstartupprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHotelstartupprovides(HotelStartupProvides newHotelstartupprovides) {
		HotelStartupProvides oldHotelstartupprovides = hotelstartupprovides;
		hotelstartupprovides = newHotelstartupprovides;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__HOTELSTARTUPPROVIDES, oldHotelstartupprovides, hotelstartupprovides));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void startup(int numRooms) {
		hotelstartupprovides.startup(numRooms);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomType(String name, int price, int numberOfBeds, EList<Feature> features) {
		return iroomtypeprovider.createRoomType(name, price, numberOfBeds, features);
		}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateRoomType(String roomTypeDescription, String newRoomTypeName, int price, int numberOfBeds, EList<Feature> features) {
		if (!iroomtypeprovider.isValid(roomTypeDescription)) return false;
		return iroomtypeprovider.updateRoomType(roomTypeDescription, newRoomTypeName, price, numberOfBeds, features); // TODO: Should take entire list
		}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoomType(String name) {
		if (!iroomtypeprovider.isValid(name)) return false;
		return iroomtypeprovider.removeRoomType(name);
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoom(int roomNumber, String roomTypeDescription) {
		if (iroomprovider.isValid(roomNumber)) return false;
		if (!iroomtypeprovider.isValid(roomTypeDescription)) return false;
		iroomprovider.createRoom(roomNumber); 
		return iroomtypeprovider.setTypeOfRoom(roomTypeDescription, roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoom(int roomNumber) {
		if (!iroomprovider.isValid(roomNumber)) return false;
		return iroomtypeprovider.removeRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean blockRoom(int roomNumber) {
		return iroomprovider.blockRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean unblockRoom(int roomNumber) {
		return iroomprovider.unblockRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean setTypeOfRoom(int roomNumber, String roomTypeDescription) {
		return iroomtypeprovider.setTypeOfRoom(roomTypeDescription, roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getTypeOfRoom(int roomNumber) {
		return iroomtypeprovider.getTypeOfRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__IROOMPROVIDER:
				if (resolve) return getIroomprovider();
				return basicGetIroomprovider();
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__IROOMTYPEPROVIDER:
				if (resolve) return getIroomtypeprovider();
				return basicGetIroomtypeprovider();
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__HOTELSTARTUPPROVIDES:
				if (resolve) return getHotelstartupprovides();
				return basicGetHotelstartupprovides();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__IROOMPROVIDER:
				setIroomprovider((IRoomProvider)newValue);
				return;
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__IROOMTYPEPROVIDER:
				setIroomtypeprovider((IRoomTypeProvider)newValue);
				return;
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__HOTELSTARTUPPROVIDES:
				setHotelstartupprovides((HotelStartupProvides)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__IROOMPROVIDER:
				setIroomprovider((IRoomProvider)null);
				return;
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__IROOMTYPEPROVIDER:
				setIroomtypeprovider((IRoomTypeProvider)null);
				return;
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__HOTELSTARTUPPROVIDES:
				setHotelstartupprovides((HotelStartupProvides)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__IROOMPROVIDER:
				return iroomprovider != null;
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__IROOMTYPEPROVIDER:
				return iroomtypeprovider != null;
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES__HOTELSTARTUPPROVIDES:
				return hotelstartupprovides != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES___STARTUP__INT:
				startup((Integer)arguments.get(0));
				return null;
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES___ADD_ROOM_TYPE__STRING_INT_INT_ELIST:
				return addRoomType((String)arguments.get(0), (Integer)arguments.get(1), (Integer)arguments.get(2), (EList<Feature>)arguments.get(3));
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES___UPDATE_ROOM_TYPE__STRING_STRING_INT_INT_ELIST:
				return updateRoomType((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (EList<Feature>)arguments.get(4));
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES___ADD_ROOM__INT_STRING:
				return addRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES___REMOVE_ROOM__INT:
				return removeRoom((Integer)arguments.get(0));
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES___BLOCK_ROOM__INT:
				return blockRoom((Integer)arguments.get(0));
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES___UNBLOCK_ROOM__INT:
				return unblockRoom((Integer)arguments.get(0));
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES___SET_TYPE_OF_ROOM__INT_STRING:
				return setTypeOfRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES___GET_TYPE_OF_ROOM__INT:
				return getTypeOfRoom((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelAdministratorProvidesImpl
