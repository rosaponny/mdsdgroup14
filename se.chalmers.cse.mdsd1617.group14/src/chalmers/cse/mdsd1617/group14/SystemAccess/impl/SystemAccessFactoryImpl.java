/**
 */
package chalmers.cse.mdsd1617.group14.SystemAccess.impl;

import chalmers.cse.mdsd1617.group14.SystemAccess.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SystemAccessFactoryImpl extends EFactoryImpl implements SystemAccessFactory {
	
	HotelCustomerProvides hotelCustomerProvides;
	HotelStartupProvides hotelStartupProvides;
	HotelReceptionistProvides hotelReceptionistProvides;
	HotelAdministratorProvides hotelAdministratorProvides;
	
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SystemAccessFactory init() {
		try {
			SystemAccessFactory theSystemAccessFactory = (SystemAccessFactory)EPackage.Registry.INSTANCE.getEFactory(SystemAccessPackage.eNS_URI);
			if (theSystemAccessFactory != null) {
				return theSystemAccessFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SystemAccessFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemAccessFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES: return createHotelCustomerProvides();
			case SystemAccessPackage.HOTEL_RECEPTIONIST_PROVIDES: return createHotelReceptionistProvides();
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES: return createHotelAdministratorProvides();
			case SystemAccessPackage.HOTEL_STARTUP_PROVIDES: return createHotelStartupProvides();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public HotelCustomerProvides createHotelCustomerProvides() {
		if (hotelCustomerProvides != null){
			return hotelCustomerProvides;
		}
		hotelCustomerProvides = new HotelCustomerProvidesImpl();
		return hotelCustomerProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelReceptionistProvides createHotelReceptionistProvides() {
		HotelReceptionistProvidesImpl hotelReceptionistProvides = new HotelReceptionistProvidesImpl();
		return hotelReceptionistProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public HotelAdministratorProvides createHotelAdministratorProvides() {
		if (hotelAdministratorProvides != null){
			return hotelAdministratorProvides;
		}
		hotelAdministratorProvides = new HotelAdministratorProvidesImpl();
		return hotelAdministratorProvides;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelStartupProvides createHotelStartupProvides() {
		HotelStartupProvidesImpl hotelStartupProvides = new HotelStartupProvidesImpl();
		return hotelStartupProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemAccessPackage getSystemAccessPackage() {
		return (SystemAccessPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SystemAccessPackage getPackage() {
		return SystemAccessPackage.eINSTANCE;
	}

} //SystemAccessFactoryImpl
