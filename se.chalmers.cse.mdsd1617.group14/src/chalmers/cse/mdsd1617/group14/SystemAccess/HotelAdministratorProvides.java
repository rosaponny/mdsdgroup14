/**
 */
package chalmers.cse.mdsd1617.group14.SystemAccess;

import chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider;

import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Administrator Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides#getIroomprovider <em>Iroomprovider</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides#getIroomtypeprovider <em>Iroomtypeprovider</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides#getHotelstartupprovides <em>Hotelstartupprovides</em>}</li>
 * </ul>
 *
 * @see chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage#getHotelAdministratorProvides()
 * @model
 * @generated
 */
public interface HotelAdministratorProvides extends IHotelAdministratorProvides {
	/**
	 * Returns the value of the '<em><b>Iroomprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroomprovider</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroomprovider</em>' reference.
	 * @see #setIroomprovider(IRoomProvider)
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage#getHotelAdministratorProvides_Iroomprovider()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomProvider getIroomprovider();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides#getIroomprovider <em>Iroomprovider</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iroomprovider</em>' reference.
	 * @see #getIroomprovider()
	 * @generated
	 */
	void setIroomprovider(IRoomProvider value);

	/**
	 * Returns the value of the '<em><b>Iroomtypeprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroomtypeprovider</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroomtypeprovider</em>' reference.
	 * @see #setIroomtypeprovider(IRoomTypeProvider)
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage#getHotelAdministratorProvides_Iroomtypeprovider()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomTypeProvider getIroomtypeprovider();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides#getIroomtypeprovider <em>Iroomtypeprovider</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iroomtypeprovider</em>' reference.
	 * @see #getIroomtypeprovider()
	 * @generated
	 */
	void setIroomtypeprovider(IRoomTypeProvider value);

	/**
	 * Returns the value of the '<em><b>Hotelstartupprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hotelstartupprovides</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hotelstartupprovides</em>' reference.
	 * @see #setHotelstartupprovides(HotelStartupProvides)
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage#getHotelAdministratorProvides_Hotelstartupprovides()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	HotelStartupProvides getHotelstartupprovides();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides#getHotelstartupprovides <em>Hotelstartupprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hotelstartupprovides</em>' reference.
	 * @see #getHotelstartupprovides()
	 * @generated
	 */
	void setHotelstartupprovides(HotelStartupProvides value);

} // HotelAdministratorProvides
