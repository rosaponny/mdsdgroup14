/**
 */
package chalmers.cse.mdsd1617.group14.SystemAccess;

import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider;

import chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider;

import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides#getIroomprovider <em>Iroomprovider</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides#getIroomtypeprovider <em>Iroomtypeprovider</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides#getIbookingprovider <em>Ibookingprovider</em>}</li>
 * </ul>
 *
 * @see chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage#getHotelStartupProvides()
 * @model
 * @generated
 */
public interface HotelStartupProvides extends IHotelStartupProvides {
	/**
	 * Returns the value of the '<em><b>Iroomprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroomprovider</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroomprovider</em>' reference.
	 * @see #setIroomprovider(IRoomProvider)
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage#getHotelStartupProvides_Iroomprovider()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomProvider getIroomprovider();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides#getIroomprovider <em>Iroomprovider</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iroomprovider</em>' reference.
	 * @see #getIroomprovider()
	 * @generated
	 */
	void setIroomprovider(IRoomProvider value);

	/**
	 * Returns the value of the '<em><b>Iroomtypeprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroomtypeprovider</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroomtypeprovider</em>' reference.
	 * @see #setIroomtypeprovider(IRoomTypeProvider)
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage#getHotelStartupProvides_Iroomtypeprovider()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomTypeProvider getIroomtypeprovider();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides#getIroomtypeprovider <em>Iroomtypeprovider</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iroomtypeprovider</em>' reference.
	 * @see #getIroomtypeprovider()
	 * @generated
	 */
	void setIroomtypeprovider(IRoomTypeProvider value);

	/**
	 * Returns the value of the '<em><b>Ibookingprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ibookingprovider</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ibookingprovider</em>' reference.
	 * @see #setIbookingprovider(IBookingProvider)
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage#getHotelStartupProvides_Ibookingprovider()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IBookingProvider getIbookingprovider();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides#getIbookingprovider <em>Ibookingprovider</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ibookingprovider</em>' reference.
	 * @see #getIbookingprovider()
	 * @generated
	 */
	void setIbookingprovider(IBookingProvider value);

} // HotelStartupProvides
