/**
 */
package chalmers.cse.mdsd1617.group14.SystemAccess;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Receptionist Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage#getHotelReceptionistProvides()
 * @model
 * @generated
 */
public interface HotelReceptionistProvides extends IHotelReceptionistProvides {
} // HotelReceptionistProvides
