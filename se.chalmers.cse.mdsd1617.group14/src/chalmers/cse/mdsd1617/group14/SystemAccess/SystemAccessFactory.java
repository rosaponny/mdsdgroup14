/**
 */
package chalmers.cse.mdsd1617.group14.SystemAccess;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage
 * @generated
 */
public interface SystemAccessFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SystemAccessFactory eINSTANCE = chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Hotel Customer Provides</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hotel Customer Provides</em>'.
	 * @generated
	 */
	HotelCustomerProvides createHotelCustomerProvides();

	/**
	 * Returns a new object of class '<em>Hotel Receptionist Provides</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hotel Receptionist Provides</em>'.
	 * @generated
	 */
	HotelReceptionistProvides createHotelReceptionistProvides();

	/**
	 * Returns a new object of class '<em>Hotel Administrator Provides</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hotel Administrator Provides</em>'.
	 * @generated
	 */
	HotelAdministratorProvides createHotelAdministratorProvides();

	/**
	 * Returns a new object of class '<em>Hotel Startup Provides</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hotel Startup Provides</em>'.
	 * @generated
	 */
	HotelStartupProvides createHotelStartupProvides();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	SystemAccessPackage getSystemAccessPackage();

} //SystemAccessFactory
