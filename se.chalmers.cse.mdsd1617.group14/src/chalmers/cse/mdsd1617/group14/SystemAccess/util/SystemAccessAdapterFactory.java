/**
 */
package chalmers.cse.mdsd1617.group14.SystemAccess.util;

import chalmers.cse.mdsd1617.group14.SystemAccess.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage
 * @generated
 */
public class SystemAccessAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SystemAccessPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemAccessAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = SystemAccessPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SystemAccessSwitch<Adapter> modelSwitch =
		new SystemAccessSwitch<Adapter>() {
			@Override
			public Adapter caseHotelCustomerProvides(HotelCustomerProvides object) {
				return createHotelCustomerProvidesAdapter();
			}
			@Override
			public Adapter caseIHotelCustomerProvides(IHotelCustomerProvides object) {
				return createIHotelCustomerProvidesAdapter();
			}
			@Override
			public Adapter caseHotelReceptionistProvides(HotelReceptionistProvides object) {
				return createHotelReceptionistProvidesAdapter();
			}
			@Override
			public Adapter caseIHotelReceptionistProvides(IHotelReceptionistProvides object) {
				return createIHotelReceptionistProvidesAdapter();
			}
			@Override
			public Adapter caseIHotelAdministratorProvides(IHotelAdministratorProvides object) {
				return createIHotelAdministratorProvidesAdapter();
			}
			@Override
			public Adapter caseIHotelStartupProvides(IHotelStartupProvides object) {
				return createIHotelStartupProvidesAdapter();
			}
			@Override
			public Adapter caseHotelAdministratorProvides(HotelAdministratorProvides object) {
				return createHotelAdministratorProvidesAdapter();
			}
			@Override
			public Adapter caseHotelStartupProvides(HotelStartupProvides object) {
				return createHotelStartupProvidesAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelCustomerProvides <em>Hotel Customer Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.HotelCustomerProvides
	 * @generated
	 */
	public Adapter createHotelCustomerProvidesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides <em>IHotel Customer Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides
	 * @generated
	 */
	public Adapter createIHotelCustomerProvidesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelReceptionistProvides <em>Hotel Receptionist Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.HotelReceptionistProvides
	 * @generated
	 */
	public Adapter createHotelReceptionistProvidesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides <em>IHotel Receptionist Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides
	 * @generated
	 */
	public Adapter createIHotelReceptionistProvidesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides <em>IHotel Administrator Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides
	 * @generated
	 */
	public Adapter createIHotelAdministratorProvidesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides <em>Hotel Startup Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides
	 * @generated
	 */
	public Adapter createHotelStartupProvidesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelStartupProvides <em>IHotel Startup Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelStartupProvides
	 * @generated
	 */
	public Adapter createIHotelStartupProvidesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides <em>Hotel Administrator Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides
	 * @generated
	 */
	public Adapter createHotelAdministratorProvidesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //SystemAccessAdapterFactory
