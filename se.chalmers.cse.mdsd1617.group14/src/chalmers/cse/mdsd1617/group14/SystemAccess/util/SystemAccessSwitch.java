/**
 */
package chalmers.cse.mdsd1617.group14.SystemAccess.util;

import chalmers.cse.mdsd1617.group14.SystemAccess.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage
 * @generated
 */
public class SystemAccessSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SystemAccessPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemAccessSwitch() {
		if (modelPackage == null) {
			modelPackage = SystemAccessPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case SystemAccessPackage.HOTEL_CUSTOMER_PROVIDES: {
				HotelCustomerProvides hotelCustomerProvides = (HotelCustomerProvides)theEObject;
				T result = caseHotelCustomerProvides(hotelCustomerProvides);
				if (result == null) result = caseIHotelCustomerProvides(hotelCustomerProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SystemAccessPackage.IHOTEL_CUSTOMER_PROVIDES: {
				IHotelCustomerProvides iHotelCustomerProvides = (IHotelCustomerProvides)theEObject;
				T result = caseIHotelCustomerProvides(iHotelCustomerProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SystemAccessPackage.HOTEL_RECEPTIONIST_PROVIDES: {
				HotelReceptionistProvides hotelReceptionistProvides = (HotelReceptionistProvides)theEObject;
				T result = caseHotelReceptionistProvides(hotelReceptionistProvides);
				if (result == null) result = caseIHotelReceptionistProvides(hotelReceptionistProvides);
				if (result == null) result = caseHotelCustomerProvides(hotelReceptionistProvides);
				if (result == null) result = caseIHotelCustomerProvides(hotelReceptionistProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SystemAccessPackage.IHOTEL_RECEPTIONIST_PROVIDES: {
				IHotelReceptionistProvides iHotelReceptionistProvides = (IHotelReceptionistProvides)theEObject;
				T result = caseIHotelReceptionistProvides(iHotelReceptionistProvides);
				if (result == null) result = caseHotelCustomerProvides(iHotelReceptionistProvides);
				if (result == null) result = caseIHotelCustomerProvides(iHotelReceptionistProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SystemAccessPackage.IHOTEL_ADMINISTRATOR_PROVIDES: {
				IHotelAdministratorProvides iHotelAdministratorProvides = (IHotelAdministratorProvides)theEObject;
				T result = caseIHotelAdministratorProvides(iHotelAdministratorProvides);
				if (result == null) result = caseIHotelStartupProvides(iHotelAdministratorProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SystemAccessPackage.IHOTEL_STARTUP_PROVIDES: {
				IHotelStartupProvides iHotelStartupProvides = (IHotelStartupProvides)theEObject;
				T result = caseIHotelStartupProvides(iHotelStartupProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SystemAccessPackage.HOTEL_ADMINISTRATOR_PROVIDES: {
				HotelAdministratorProvides hotelAdministratorProvides = (HotelAdministratorProvides)theEObject;
				T result = caseHotelAdministratorProvides(hotelAdministratorProvides);
				if (result == null) result = caseIHotelAdministratorProvides(hotelAdministratorProvides);
				if (result == null) result = caseIHotelStartupProvides(hotelAdministratorProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case SystemAccessPackage.HOTEL_STARTUP_PROVIDES: {
				HotelStartupProvides hotelStartupProvides = (HotelStartupProvides)theEObject;
				T result = caseHotelStartupProvides(hotelStartupProvides);
				if (result == null) result = caseIHotelStartupProvides(hotelStartupProvides);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hotel Customer Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hotel Customer Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHotelCustomerProvides(HotelCustomerProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IHotel Customer Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IHotel Customer Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIHotelCustomerProvides(IHotelCustomerProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hotel Receptionist Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hotel Receptionist Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHotelReceptionistProvides(HotelReceptionistProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IHotel Receptionist Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IHotel Receptionist Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIHotelReceptionistProvides(IHotelReceptionistProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IHotel Administrator Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IHotel Administrator Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIHotelAdministratorProvides(IHotelAdministratorProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hotel Startup Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hotel Startup Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHotelStartupProvides(HotelStartupProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IHotel Startup Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IHotel Startup Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIHotelStartupProvides(IHotelStartupProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hotel Administrator Provides</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hotel Administrator Provides</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHotelAdministratorProvides(HotelAdministratorProvides object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //SystemAccessSwitch
