/**
 */
package chalmers.cse.mdsd1617.group14.SystemAccess;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessFactory
 * @model kind="package"
 * @generated
 */
public interface SystemAccessPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "SystemAccess";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///chalmers/cse/mdsd1617/group14/SystemAccess.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "chalmers.cse.mdsd1617.group14.SystemAccess";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SystemAccessPackage eINSTANCE = chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl.init();

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides <em>IHotel Customer Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl#getIHotelCustomerProvides()
	 * @generated
	 */
	int IHOTEL_CUSTOMER_PROVIDES = 1;

	/**
	 * The number of structural features of the '<em>IHotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = 0;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = 1;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = 2;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = 3;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = 4;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = 5;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = 6;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = 7;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = 8;

	/**
	 * The number of operations of the '<em>IHotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT = 9;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelCustomerProvidesImpl <em>Hotel Customer Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelCustomerProvidesImpl
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl#getHotelCustomerProvides()
	 * @generated
	 */
	int HOTEL_CUSTOMER_PROVIDES = 0;

	/**
	 * The feature id for the '<em><b>Iroomprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES__IROOMPROVIDER = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Iroomtypeprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES__IROOMTYPEPROVIDER = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ibookingprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES__IBOOKINGPROVIDER = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Hotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT;

	/**
	 * The number of operations of the '<em>Hotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides <em>IHotel Receptionist Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl#getIHotelReceptionistProvides()
	 * @generated
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES = 3;

	/**
	 * The feature id for the '<em><b>Iroomprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES__IROOMPROVIDER = HOTEL_CUSTOMER_PROVIDES__IROOMPROVIDER;

	/**
	 * The feature id for the '<em><b>Iroomtypeprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES__IROOMTYPEPROVIDER = HOTEL_CUSTOMER_PROVIDES__IROOMTYPEPROVIDER;

	/**
	 * The feature id for the '<em><b>Ibookingprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES__IBOOKINGPROVIDER = HOTEL_CUSTOMER_PROVIDES__IBOOKINGPROVIDER;

	/**
	 * The number of structural features of the '<em>IHotel Receptionist Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES_FEATURE_COUNT = HOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = HOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = HOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = HOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___CONFIRM_BOOKING__INT = HOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___INITIATE_CHECKOUT__INT = HOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = HOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = HOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = HOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_ROOM__STRING_INT = HOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT;

	/**
	 * The operation id for the '<em>Get Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___GET_OCCUPIED_ROOMS__STRING = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Remove Room From Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___REMOVE_ROOM_FROM_BOOKING__STRING_INT = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Update Name Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___UPDATE_NAME_OF_BOOKING__INT_STRING_STRING = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Update Date Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___UPDATE_DATE_OF_BOOKING__INT_STRING_STRING = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Check Out Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___CHECK_OUT_BOOKING__INT = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Get Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___GET_BOOKING__STRING_STRING_STRING = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__STRING = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__STRING = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_BOOKING__INT = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>Add Extra Cost To Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_INT = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 10;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___LIST_BOOKINGS = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 11;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__STRING_STRING = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 12;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__STRING_STRING = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 13;

	/**
	 * The number of operations of the '<em>IHotel Receptionist Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES_OPERATION_COUNT = HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 14;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelReceptionistProvidesImpl <em>Hotel Receptionist Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelReceptionistProvidesImpl
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl#getHotelReceptionistProvides()
	 * @generated
	 */
	int HOTEL_RECEPTIONIST_PROVIDES = 2;

	/**
	 * The feature id for the '<em><b>Iroomprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES__IROOMPROVIDER = IHOTEL_RECEPTIONIST_PROVIDES__IROOMPROVIDER;

	/**
	 * The feature id for the '<em><b>Iroomtypeprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES__IROOMTYPEPROVIDER = IHOTEL_RECEPTIONIST_PROVIDES__IROOMTYPEPROVIDER;

	/**
	 * The feature id for the '<em><b>Ibookingprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES__IBOOKINGPROVIDER = IHOTEL_RECEPTIONIST_PROVIDES__IBOOKINGPROVIDER;

	/**
	 * The number of structural features of the '<em>Hotel Receptionist Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES_FEATURE_COUNT = IHOTEL_RECEPTIONIST_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = IHOTEL_RECEPTIONIST_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IHOTEL_RECEPTIONIST_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = IHOTEL_RECEPTIONIST_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___CONFIRM_BOOKING__INT = IHOTEL_RECEPTIONIST_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___INITIATE_CHECKOUT__INT = IHOTEL_RECEPTIONIST_PROVIDES___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = IHOTEL_RECEPTIONIST_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = IHOTEL_RECEPTIONIST_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = IHOTEL_RECEPTIONIST_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_ROOM__STRING_INT = IHOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_ROOM__STRING_INT;

	/**
	 * The operation id for the '<em>Get Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___GET_OCCUPIED_ROOMS__STRING = IHOTEL_RECEPTIONIST_PROVIDES___GET_OCCUPIED_ROOMS__STRING;

	/**
	 * The operation id for the '<em>Remove Room From Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___REMOVE_ROOM_FROM_BOOKING__STRING_INT = IHOTEL_RECEPTIONIST_PROVIDES___REMOVE_ROOM_FROM_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Update Name Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___UPDATE_NAME_OF_BOOKING__INT_STRING_STRING = IHOTEL_RECEPTIONIST_PROVIDES___UPDATE_NAME_OF_BOOKING__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Update Date Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___UPDATE_DATE_OF_BOOKING__INT_STRING_STRING = IHOTEL_RECEPTIONIST_PROVIDES___UPDATE_DATE_OF_BOOKING__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT = IHOTEL_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT;

	/**
	 * The operation id for the '<em>Check Out Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___CHECK_OUT_BOOKING__INT = IHOTEL_RECEPTIONIST_PROVIDES___CHECK_OUT_BOOKING__INT;

	/**
	 * The operation id for the '<em>Get Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___GET_BOOKING__STRING_STRING_STRING = IHOTEL_RECEPTIONIST_PROVIDES___GET_BOOKING__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__STRING = IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__STRING;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__STRING = IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__STRING;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_BOOKING__INT = IHOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_BOOKING__INT;

	/**
	 * The operation id for the '<em>Add Extra Cost To Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_INT = IHOTEL_RECEPTIONIST_PROVIDES___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_INT;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___LIST_BOOKINGS = IHOTEL_RECEPTIONIST_PROVIDES___LIST_BOOKINGS;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__STRING_STRING = IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__STRING_STRING;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__STRING_STRING = IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__STRING_STRING;

	/**
	 * The number of operations of the '<em>Hotel Receptionist Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_PROVIDES_OPERATION_COUNT = IHOTEL_RECEPTIONIST_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides <em>IHotel Administrator Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl#getIHotelAdministratorProvides()
	 * @generated
	 */
	int IHOTEL_ADMINISTRATOR_PROVIDES = 4;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelStartupProvides <em>IHotel Startup Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelStartupProvides
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl#getIHotelStartupProvides()
	 * @generated
	 */
	int IHOTEL_STARTUP_PROVIDES = 5;

	/**
	 * The number of structural features of the '<em>IHotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES___STARTUP__INT = 0;

	/**
	 * The number of operations of the '<em>IHotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT = 1;

	/**
	 * The number of structural features of the '<em>IHotel Administrator Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMINISTRATOR_PROVIDES_FEATURE_COUNT = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMINISTRATOR_PROVIDES___STARTUP__INT = IHOTEL_STARTUP_PROVIDES___STARTUP__INT;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMINISTRATOR_PROVIDES___ADD_ROOM_TYPE__STRING_INT_INT_ELIST = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMINISTRATOR_PROVIDES___UPDATE_ROOM_TYPE__STRING_STRING_INT_INT_ELIST = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMINISTRATOR_PROVIDES___REMOVE_ROOM_TYPE__STRING = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMINISTRATOR_PROVIDES___ADD_ROOM__INT_STRING = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMINISTRATOR_PROVIDES___REMOVE_ROOM__INT = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMINISTRATOR_PROVIDES___BLOCK_ROOM__INT = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMINISTRATOR_PROVIDES___UNBLOCK_ROOM__INT = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Set Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMINISTRATOR_PROVIDES___SET_TYPE_OF_ROOM__INT_STRING = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Get Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMINISTRATOR_PROVIDES___GET_TYPE_OF_ROOM__INT = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 8;

	/**
	 * The number of operations of the '<em>IHotel Administrator Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMINISTRATOR_PROVIDES_OPERATION_COUNT = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 9;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelAdministratorProvidesImpl <em>Hotel Administrator Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelAdministratorProvidesImpl
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl#getHotelAdministratorProvides()
	 * @generated
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES = 6;

	/**
	 * The feature id for the '<em><b>Iroomprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES__IROOMPROVIDER = IHOTEL_ADMINISTRATOR_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Iroomtypeprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES__IROOMTYPEPROVIDER = IHOTEL_ADMINISTRATOR_PROVIDES_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Hotelstartupprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES__HOTELSTARTUPPROVIDES = IHOTEL_ADMINISTRATOR_PROVIDES_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Hotel Administrator Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES_FEATURE_COUNT = IHOTEL_ADMINISTRATOR_PROVIDES_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___STARTUP__INT = IHOTEL_ADMINISTRATOR_PROVIDES___STARTUP__INT;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___ADD_ROOM_TYPE__STRING_INT_INT_ELIST = IHOTEL_ADMINISTRATOR_PROVIDES___ADD_ROOM_TYPE__STRING_INT_INT_ELIST;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___UPDATE_ROOM_TYPE__STRING_STRING_INT_INT_ELIST = IHOTEL_ADMINISTRATOR_PROVIDES___UPDATE_ROOM_TYPE__STRING_STRING_INT_INT_ELIST;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___REMOVE_ROOM_TYPE__STRING = IHOTEL_ADMINISTRATOR_PROVIDES___REMOVE_ROOM_TYPE__STRING;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___ADD_ROOM__INT_STRING = IHOTEL_ADMINISTRATOR_PROVIDES___ADD_ROOM__INT_STRING;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___REMOVE_ROOM__INT = IHOTEL_ADMINISTRATOR_PROVIDES___REMOVE_ROOM__INT;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___BLOCK_ROOM__INT = IHOTEL_ADMINISTRATOR_PROVIDES___BLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___UNBLOCK_ROOM__INT = IHOTEL_ADMINISTRATOR_PROVIDES___UNBLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Set Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___SET_TYPE_OF_ROOM__INT_STRING = IHOTEL_ADMINISTRATOR_PROVIDES___SET_TYPE_OF_ROOM__INT_STRING;

	/**
	 * The operation id for the '<em>Get Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES___GET_TYPE_OF_ROOM__INT = IHOTEL_ADMINISTRATOR_PROVIDES___GET_TYPE_OF_ROOM__INT;

	/**
	 * The number of operations of the '<em>Hotel Administrator Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMINISTRATOR_PROVIDES_OPERATION_COUNT = IHOTEL_ADMINISTRATOR_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelStartupProvidesImpl <em>Hotel Startup Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelStartupProvidesImpl
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl#getHotelStartupProvides()
	 * @generated
	 */
	int HOTEL_STARTUP_PROVIDES = 7;

	/**
	 * The feature id for the '<em><b>Iroomprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES__IROOMPROVIDER = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Iroomtypeprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES__IROOMTYPEPROVIDER = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Ibookingprovider</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES__IBOOKINGPROVIDER = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Hotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES_FEATURE_COUNT = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES___STARTUP__INT = IHOTEL_STARTUP_PROVIDES___STARTUP__INT;

	/**
	 * The number of operations of the '<em>Hotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_STARTUP_PROVIDES_OPERATION_COUNT = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelCustomerProvides <em>Hotel Customer Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Customer Provides</em>'.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.HotelCustomerProvides
	 * @generated
	 */
	EClass getHotelCustomerProvides();

	/**
	 * Returns the meta object for the reference '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelCustomerProvides#getIroomprovider <em>Iroomprovider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iroomprovider</em>'.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.HotelCustomerProvides#getIroomprovider()
	 * @see #getHotelCustomerProvides()
	 * @generated
	 */
	EReference getHotelCustomerProvides_Iroomprovider();

	/**
	 * Returns the meta object for the reference '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelCustomerProvides#getIroomtypeprovider <em>Iroomtypeprovider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iroomtypeprovider</em>'.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.HotelCustomerProvides#getIroomtypeprovider()
	 * @see #getHotelCustomerProvides()
	 * @generated
	 */
	EReference getHotelCustomerProvides_Iroomtypeprovider();

	/**
	 * Returns the meta object for the reference '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelCustomerProvides#getIbookingprovider <em>Ibookingprovider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ibookingprovider</em>'.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.HotelCustomerProvides#getIbookingprovider()
	 * @see #getHotelCustomerProvides()
	 * @generated
	 */
	EReference getHotelCustomerProvides_Ibookingprovider();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides <em>IHotel Customer Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Customer Provides</em>'.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides
	 * @generated
	 */
	EClass getIHotelCustomerProvides();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#getFreeRooms(int, java.lang.String, java.lang.String) <em>Get Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#getFreeRooms(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__GetFreeRooms__int_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Initiate Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateBooking__String_String_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#addRoomToBooking(java.lang.String, int) <em>Add Room To Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room To Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#addRoomToBooking(java.lang.String, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__AddRoomToBooking__String_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#confirmBooking(int) <em>Confirm Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#confirmBooking(int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__ConfirmBooking__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#initiateCheckout(int) <em>Initiate Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Checkout</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#initiateCheckout(int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateCheckout__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay During Checkout</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__PayDuringCheckout__String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#initiateRoomCheckout(int, int) <em>Initiate Room Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Room Checkout</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#initiateRoomCheckout(int, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateRoomCheckout__int_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay Room During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Room During Checkout</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__PayRoomDuringCheckout__int_String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#checkInRoom(java.lang.String, int) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides#checkInRoom(java.lang.String, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__CheckInRoom__String_int();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelReceptionistProvides <em>Hotel Receptionist Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Receptionist Provides</em>'.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.HotelReceptionistProvides
	 * @generated
	 */
	EClass getHotelReceptionistProvides();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides <em>IHotel Receptionist Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Receptionist Provides</em>'.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides
	 * @generated
	 */
	EClass getIHotelReceptionistProvides();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#getOccupiedRooms(java.lang.String) <em>Get Occupied Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Occupied Rooms</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#getOccupiedRooms(java.lang.String)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__GetOccupiedRooms__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#removeRoomFromBooking(java.lang.String, int) <em>Remove Room From Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room From Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#removeRoomFromBooking(java.lang.String, int)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__RemoveRoomFromBooking__String_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#updateNameOfBooking(int, java.lang.String, java.lang.String) <em>Update Name Of Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Name Of Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#updateNameOfBooking(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__UpdateNameOfBooking__int_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#updateDateOfBooking(int, java.lang.String, java.lang.String) <em>Update Date Of Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Date Of Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#updateDateOfBooking(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__UpdateDateOfBooking__int_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#cancelBooking(int) <em>Cancel Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#cancelBooking(int)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__CancelBooking__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#checkOutBooking(int) <em>Check Out Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#checkOutBooking(int)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__CheckOutBooking__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#getBooking(java.lang.String, java.lang.String, java.lang.String) <em>Get Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#getBooking(java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__GetBooking__String_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#listCheckIns(java.lang.String) <em>List Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#listCheckIns(java.lang.String)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__ListCheckIns__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#listCheckOuts(java.lang.String) <em>List Check Outs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#listCheckOuts(java.lang.String)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__ListCheckOuts__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#checkInBooking(int) <em>Check In Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#checkInBooking(int)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__CheckInBooking__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#addExtraCostToRoom(int, int, java.lang.String, int) <em>Add Extra Cost To Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra Cost To Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#addExtraCostToRoom(int, int, java.lang.String, int)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__AddExtraCostToRoom__int_int_String_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#listBookings() <em>List Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Bookings</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#listBookings()
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__ListBookings();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#listCheckOuts(java.lang.String, java.lang.String) <em>List Check Outs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#listCheckOuts(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__ListCheckOuts__String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#listCheckIns(java.lang.String, java.lang.String) <em>List Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides#listCheckIns(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__ListCheckIns__String_String();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides <em>IHotel Administrator Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Administrator Provides</em>'.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides
	 * @generated
	 */
	EClass getIHotelAdministratorProvides();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#addRoomType(java.lang.String, int, int, org.eclipse.emf.common.util.EList) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#addRoomType(java.lang.String, int, int, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getIHotelAdministratorProvides__AddRoomType__String_int_int_EList();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#updateRoomType(java.lang.String, java.lang.String, int, int, org.eclipse.emf.common.util.EList) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#updateRoomType(java.lang.String, java.lang.String, int, int, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getIHotelAdministratorProvides__UpdateRoomType__String_String_int_int_EList();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getIHotelAdministratorProvides__RemoveRoomType__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#addRoom(int, java.lang.String) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#addRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelAdministratorProvides__AddRoom__int_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#removeRoom(int)
	 * @generated
	 */
	EOperation getIHotelAdministratorProvides__RemoveRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#blockRoom(int) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#blockRoom(int)
	 * @generated
	 */
	EOperation getIHotelAdministratorProvides__BlockRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#unblockRoom(int) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#unblockRoom(int)
	 * @generated
	 */
	EOperation getIHotelAdministratorProvides__UnblockRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#setTypeOfRoom(int, java.lang.String) <em>Set Type Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Type Of Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#setTypeOfRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelAdministratorProvides__SetTypeOfRoom__int_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#getTypeOfRoom(int) <em>Get Type Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Type Of Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides#getTypeOfRoom(int)
	 * @generated
	 */
	EOperation getIHotelAdministratorProvides__GetTypeOfRoom__int();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides <em>Hotel Startup Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Startup Provides</em>'.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides
	 * @generated
	 */
	EClass getHotelStartupProvides();

	/**
	 * Returns the meta object for the reference '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides#getIroomprovider <em>Iroomprovider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iroomprovider</em>'.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides#getIroomprovider()
	 * @see #getHotelStartupProvides()
	 * @generated
	 */
	EReference getHotelStartupProvides_Iroomprovider();

	/**
	 * Returns the meta object for the reference '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides#getIroomtypeprovider <em>Iroomtypeprovider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iroomtypeprovider</em>'.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides#getIroomtypeprovider()
	 * @see #getHotelStartupProvides()
	 * @generated
	 */
	EReference getHotelStartupProvides_Iroomtypeprovider();

	/**
	 * Returns the meta object for the reference '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides#getIbookingprovider <em>Ibookingprovider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ibookingprovider</em>'.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.HotelStartupProvides#getIbookingprovider()
	 * @see #getHotelStartupProvides()
	 * @generated
	 */
	EReference getHotelStartupProvides_Ibookingprovider();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelStartupProvides <em>IHotel Startup Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Startup Provides</em>'.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelStartupProvides
	 * @generated
	 */
	EClass getIHotelStartupProvides();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelStartupProvides#startup(int) <em>Startup</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Startup</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelStartupProvides#startup(int)
	 * @generated
	 */
	EOperation getIHotelStartupProvides__Startup__int();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides <em>Hotel Administrator Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Administrator Provides</em>'.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides
	 * @generated
	 */
	EClass getHotelAdministratorProvides();

	/**
	 * Returns the meta object for the reference '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides#getIroomprovider <em>Iroomprovider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iroomprovider</em>'.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides#getIroomprovider()
	 * @see #getHotelAdministratorProvides()
	 * @generated
	 */
	EReference getHotelAdministratorProvides_Iroomprovider();

	/**
	 * Returns the meta object for the reference '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides#getIroomtypeprovider <em>Iroomtypeprovider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iroomtypeprovider</em>'.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides#getIroomtypeprovider()
	 * @see #getHotelAdministratorProvides()
	 * @generated
	 */
	EReference getHotelAdministratorProvides_Iroomtypeprovider();

	/**
	 * Returns the meta object for the reference '{@link chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides#getHotelstartupprovides <em>Hotelstartupprovides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Hotelstartupprovides</em>'.
	 * @see chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides#getHotelstartupprovides()
	 * @see #getHotelAdministratorProvides()
	 * @generated
	 */
	EReference getHotelAdministratorProvides_Hotelstartupprovides();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SystemAccessFactory getSystemAccessFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelCustomerProvidesImpl <em>Hotel Customer Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelCustomerProvidesImpl
		 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl#getHotelCustomerProvides()
		 * @generated
		 */
		EClass HOTEL_CUSTOMER_PROVIDES = eINSTANCE.getHotelCustomerProvides();

		/**
		 * The meta object literal for the '<em><b>Iroomprovider</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_CUSTOMER_PROVIDES__IROOMPROVIDER = eINSTANCE.getHotelCustomerProvides_Iroomprovider();

		/**
		 * The meta object literal for the '<em><b>Iroomtypeprovider</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_CUSTOMER_PROVIDES__IROOMTYPEPROVIDER = eINSTANCE.getHotelCustomerProvides_Iroomtypeprovider();

		/**
		 * The meta object literal for the '<em><b>Ibookingprovider</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_CUSTOMER_PROVIDES__IBOOKINGPROVIDER = eINSTANCE.getHotelCustomerProvides_Ibookingprovider();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides <em>IHotel Customer Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides
		 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl#getIHotelCustomerProvides()
		 * @generated
		 */
		EClass IHOTEL_CUSTOMER_PROVIDES = eINSTANCE.getIHotelCustomerProvides();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__GetFreeRooms__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Initiate Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__InitiateBooking__String_String_String_String();

		/**
		 * The meta object literal for the '<em><b>Add Room To Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = eINSTANCE.getIHotelCustomerProvides__AddRoomToBooking__String_int();

		/**
		 * The meta object literal for the '<em><b>Confirm Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = eINSTANCE.getIHotelCustomerProvides__ConfirmBooking__int();

		/**
		 * The meta object literal for the '<em><b>Initiate Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = eINSTANCE.getIHotelCustomerProvides__InitiateCheckout__int();

		/**
		 * The meta object literal for the '<em><b>Pay During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__PayDuringCheckout__String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Initiate Room Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = eINSTANCE.getIHotelCustomerProvides__InitiateRoomCheckout__int_int();

		/**
		 * The meta object literal for the '<em><b>Pay Room During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__PayRoomDuringCheckout__int_String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = eINSTANCE.getIHotelCustomerProvides__CheckInRoom__String_int();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelReceptionistProvidesImpl <em>Hotel Receptionist Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelReceptionistProvidesImpl
		 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl#getHotelReceptionistProvides()
		 * @generated
		 */
		EClass HOTEL_RECEPTIONIST_PROVIDES = eINSTANCE.getHotelReceptionistProvides();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides <em>IHotel Receptionist Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelReceptionistProvides
		 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl#getIHotelReceptionistProvides()
		 * @generated
		 */
		EClass IHOTEL_RECEPTIONIST_PROVIDES = eINSTANCE.getIHotelReceptionistProvides();

		/**
		 * The meta object literal for the '<em><b>Get Occupied Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___GET_OCCUPIED_ROOMS__STRING = eINSTANCE.getIHotelReceptionistProvides__GetOccupiedRooms__String();

		/**
		 * The meta object literal for the '<em><b>Remove Room From Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___REMOVE_ROOM_FROM_BOOKING__STRING_INT = eINSTANCE.getIHotelReceptionistProvides__RemoveRoomFromBooking__String_int();

		/**
		 * The meta object literal for the '<em><b>Update Name Of Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___UPDATE_NAME_OF_BOOKING__INT_STRING_STRING = eINSTANCE.getIHotelReceptionistProvides__UpdateNameOfBooking__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Update Date Of Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___UPDATE_DATE_OF_BOOKING__INT_STRING_STRING = eINSTANCE.getIHotelReceptionistProvides__UpdateDateOfBooking__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT = eINSTANCE.getIHotelReceptionistProvides__CancelBooking__int();

		/**
		 * The meta object literal for the '<em><b>Check Out Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___CHECK_OUT_BOOKING__INT = eINSTANCE.getIHotelReceptionistProvides__CheckOutBooking__int();

		/**
		 * The meta object literal for the '<em><b>Get Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___GET_BOOKING__STRING_STRING_STRING = eINSTANCE.getIHotelReceptionistProvides__GetBooking__String_String_String();

		/**
		 * The meta object literal for the '<em><b>List Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__STRING = eINSTANCE.getIHotelReceptionistProvides__ListCheckIns__String();

		/**
		 * The meta object literal for the '<em><b>List Check Outs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__STRING = eINSTANCE.getIHotelReceptionistProvides__ListCheckOuts__String();

		/**
		 * The meta object literal for the '<em><b>Check In Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_BOOKING__INT = eINSTANCE.getIHotelReceptionistProvides__CheckInBooking__int();

		/**
		 * The meta object literal for the '<em><b>Add Extra Cost To Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_INT = eINSTANCE.getIHotelReceptionistProvides__AddExtraCostToRoom__int_int_String_int();

		/**
		 * The meta object literal for the '<em><b>List Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___LIST_BOOKINGS = eINSTANCE.getIHotelReceptionistProvides__ListBookings();

		/**
		 * The meta object literal for the '<em><b>List Check Outs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__STRING_STRING = eINSTANCE.getIHotelReceptionistProvides__ListCheckOuts__String_String();

		/**
		 * The meta object literal for the '<em><b>List Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__STRING_STRING = eINSTANCE.getIHotelReceptionistProvides__ListCheckIns__String_String();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides <em>IHotel Administrator Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelAdministratorProvides
		 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl#getIHotelAdministratorProvides()
		 * @generated
		 */
		EClass IHOTEL_ADMINISTRATOR_PROVIDES = eINSTANCE.getIHotelAdministratorProvides();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_ADMINISTRATOR_PROVIDES___ADD_ROOM_TYPE__STRING_INT_INT_ELIST = eINSTANCE.getIHotelAdministratorProvides__AddRoomType__String_int_int_EList();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_ADMINISTRATOR_PROVIDES___UPDATE_ROOM_TYPE__STRING_STRING_INT_INT_ELIST = eINSTANCE.getIHotelAdministratorProvides__UpdateRoomType__String_String_int_int_EList();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_ADMINISTRATOR_PROVIDES___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getIHotelAdministratorProvides__RemoveRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_ADMINISTRATOR_PROVIDES___ADD_ROOM__INT_STRING = eINSTANCE.getIHotelAdministratorProvides__AddRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_ADMINISTRATOR_PROVIDES___REMOVE_ROOM__INT = eINSTANCE.getIHotelAdministratorProvides__RemoveRoom__int();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_ADMINISTRATOR_PROVIDES___BLOCK_ROOM__INT = eINSTANCE.getIHotelAdministratorProvides__BlockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_ADMINISTRATOR_PROVIDES___UNBLOCK_ROOM__INT = eINSTANCE.getIHotelAdministratorProvides__UnblockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Set Type Of Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_ADMINISTRATOR_PROVIDES___SET_TYPE_OF_ROOM__INT_STRING = eINSTANCE.getIHotelAdministratorProvides__SetTypeOfRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Get Type Of Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_ADMINISTRATOR_PROVIDES___GET_TYPE_OF_ROOM__INT = eINSTANCE.getIHotelAdministratorProvides__GetTypeOfRoom__int();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelStartupProvidesImpl <em>Hotel Startup Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelStartupProvidesImpl
		 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl#getHotelStartupProvides()
		 * @generated
		 */
		EClass HOTEL_STARTUP_PROVIDES = eINSTANCE.getHotelStartupProvides();

		/**
		 * The meta object literal for the '<em><b>Iroomprovider</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_STARTUP_PROVIDES__IROOMPROVIDER = eINSTANCE.getHotelStartupProvides_Iroomprovider();

		/**
		 * The meta object literal for the '<em><b>Iroomtypeprovider</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_STARTUP_PROVIDES__IROOMTYPEPROVIDER = eINSTANCE.getHotelStartupProvides_Iroomtypeprovider();

		/**
		 * The meta object literal for the '<em><b>Ibookingprovider</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_STARTUP_PROVIDES__IBOOKINGPROVIDER = eINSTANCE.getHotelStartupProvides_Ibookingprovider();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.IHotelStartupProvides <em>IHotel Startup Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.SystemAccess.IHotelStartupProvides
		 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl#getIHotelStartupProvides()
		 * @generated
		 */
		EClass IHOTEL_STARTUP_PROVIDES = eINSTANCE.getIHotelStartupProvides();

		/**
		 * The meta object literal for the '<em><b>Startup</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_STARTUP_PROVIDES___STARTUP__INT = eINSTANCE.getIHotelStartupProvides__Startup__int();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelAdministratorProvidesImpl <em>Hotel Administrator Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.HotelAdministratorProvidesImpl
		 * @see chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl#getHotelAdministratorProvides()
		 * @generated
		 */
		EClass HOTEL_ADMINISTRATOR_PROVIDES = eINSTANCE.getHotelAdministratorProvides();

		/**
		 * The meta object literal for the '<em><b>Iroomprovider</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_ADMINISTRATOR_PROVIDES__IROOMPROVIDER = eINSTANCE.getHotelAdministratorProvides_Iroomprovider();

		/**
		 * The meta object literal for the '<em><b>Iroomtypeprovider</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_ADMINISTRATOR_PROVIDES__IROOMTYPEPROVIDER = eINSTANCE.getHotelAdministratorProvides_Iroomtypeprovider();

		/**
		 * The meta object literal for the '<em><b>Hotelstartupprovides</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_ADMINISTRATOR_PROVIDES__HOTELSTARTUPPROVIDES = eINSTANCE.getHotelAdministratorProvides_Hotelstartupprovides();

	}

} //SystemAccessPackage
