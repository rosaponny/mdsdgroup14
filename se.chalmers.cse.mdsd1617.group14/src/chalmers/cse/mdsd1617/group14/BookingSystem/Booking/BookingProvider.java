/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Booking;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingProvider#getIbookingtable <em>Ibookingtable</em>}</li>
 * </ul>
 *
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage#getBookingProvider()
 * @model
 * @generated
 */
public interface BookingProvider extends IBookingProvider {

	/**
	 * Returns the value of the '<em><b>Ibookingtable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ibookingtable</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ibookingtable</em>' reference.
	 * @see #setIbookingtable(IBookingTable)
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage#getBookingProvider_Ibookingtable()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IBookingTable getIbookingtable();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingProvider#getIbookingtable <em>Ibookingtable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ibookingtable</em>' reference.
	 * @see #getIbookingtable()
	 * @generated
	 */
	void setIbookingtable(IBookingTable value);
} // BookingProvider
