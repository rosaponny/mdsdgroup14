/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IntDTOTuple;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Int DTO Tuple</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.IntDTOTupleImpl#getI1 <em>I1</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.IntDTOTupleImpl#getFreeRoomTypesDTO <em>Free Room Types DTO</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IntDTOTupleImpl extends MinimalEObjectImpl.Container implements IntDTOTuple {
	/**
	 * The default value of the '{@link #getI1() <em>I1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getI1()
	 * @generated
	 * @ordered
	 */
	protected static final int I1_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getI1() <em>I1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getI1()
	 * @generated
	 * @ordered
	 */
	protected int i1 = I1_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFreeRoomTypesDTO() <em>Free Room Types DTO</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 * @ordered
	 */
	protected FreeRoomTypesDTO freeRoomTypesDTO;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntDTOTupleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.INT_DTO_TUPLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getI1() {
		return i1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setI1(int newI1) {
		int oldI1 = i1;
		i1 = newI1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.INT_DTO_TUPLE__I1, oldI1, i1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FreeRoomTypesDTO getFreeRoomTypesDTO() {
		return freeRoomTypesDTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFreeRoomTypesDTO(FreeRoomTypesDTO newFreeRoomTypesDTO, NotificationChain msgs) {
		FreeRoomTypesDTO oldFreeRoomTypesDTO = freeRoomTypesDTO;
		freeRoomTypesDTO = newFreeRoomTypesDTO;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, BookingPackage.INT_DTO_TUPLE__FREE_ROOM_TYPES_DTO, oldFreeRoomTypesDTO, newFreeRoomTypesDTO);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFreeRoomTypesDTO(FreeRoomTypesDTO newFreeRoomTypesDTO) {
		if (newFreeRoomTypesDTO != freeRoomTypesDTO) {
			NotificationChain msgs = null;
			if (freeRoomTypesDTO != null)
				msgs = ((InternalEObject)freeRoomTypesDTO).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - BookingPackage.INT_DTO_TUPLE__FREE_ROOM_TYPES_DTO, null, msgs);
			if (newFreeRoomTypesDTO != null)
				msgs = ((InternalEObject)newFreeRoomTypesDTO).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - BookingPackage.INT_DTO_TUPLE__FREE_ROOM_TYPES_DTO, null, msgs);
			msgs = basicSetFreeRoomTypesDTO(newFreeRoomTypesDTO, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.INT_DTO_TUPLE__FREE_ROOM_TYPES_DTO, newFreeRoomTypesDTO, newFreeRoomTypesDTO));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BookingPackage.INT_DTO_TUPLE__FREE_ROOM_TYPES_DTO:
				return basicSetFreeRoomTypesDTO(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.INT_DTO_TUPLE__I1:
				return getI1();
			case BookingPackage.INT_DTO_TUPLE__FREE_ROOM_TYPES_DTO:
				return getFreeRoomTypesDTO();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.INT_DTO_TUPLE__I1:
				setI1((Integer)newValue);
				return;
			case BookingPackage.INT_DTO_TUPLE__FREE_ROOM_TYPES_DTO:
				setFreeRoomTypesDTO((FreeRoomTypesDTO)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.INT_DTO_TUPLE__I1:
				setI1(I1_EDEFAULT);
				return;
			case BookingPackage.INT_DTO_TUPLE__FREE_ROOM_TYPES_DTO:
				setFreeRoomTypesDTO((FreeRoomTypesDTO)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.INT_DTO_TUPLE__I1:
				return i1 != I1_EDEFAULT;
			case BookingPackage.INT_DTO_TUPLE__FREE_ROOM_TYPES_DTO:
				return freeRoomTypesDTO != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (i1: ");
		result.append(i1);
		result.append(')');
		return result.toString();
	}

} //IntDTOTupleImpl
