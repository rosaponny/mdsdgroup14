/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.StringIntTuple;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Provider</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingProviderImpl#getIbookingtable <em>Ibookingtable</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingProviderImpl extends MinimalEObjectImpl.Container implements BookingProvider {
	
	protected IBookingTable ibookingtable;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected BookingProviderImpl() {
		super();
		
		ibookingtable = BookingFactory.eINSTANCE.createBookingTable();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.BOOKING_PROVIDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookingTable getIbookingtable() {
		if (ibookingtable != null && ibookingtable.eIsProxy()) {
			InternalEObject oldIbookingtable = (InternalEObject)ibookingtable;
			ibookingtable = (IBookingTable)eResolveProxy(oldIbookingtable);
			if (ibookingtable != oldIbookingtable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackage.BOOKING_PROVIDER__IBOOKINGTABLE, oldIbookingtable, ibookingtable));
			}
		}
		return ibookingtable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookingTable basicGetIbookingtable() {
		return ibookingtable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIbookingtable(IBookingTable newIbookingtable) {
		IBookingTable oldIbookingtable = ibookingtable;
		ibookingtable = newIbookingtable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING_PROVIDER__IBOOKINGTABLE, oldIbookingtable, ibookingtable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> listCheckIns(String day) {
		return ibookingtable.listCheckIns(day);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<String> listBookings() {
		return ibookingtable.listBookings();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addExtraCostToRoom(int bookingID, int roomNumber, String roomTypeDescription, int price) {
		if (!ibookingtable.isValid(bookingID)) return false;
		return ibookingtable.addExtraCostToRoom(bookingID, roomNumber, roomTypeDescription, price);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String lastName, String startDate, String endDate) {
		if (!(isADate(startDate) && isADate(endDate))) return -1;
		return ibookingtable.initiateBooking(firstName, lastName, startDate, endDate);
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		return ibookingtable.confirmBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoomFromBooking(String roomTypeDescription, int booking) {
		return ibookingtable.removeRoomFromBooking(roomTypeDescription, booking);
		
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingId) {
		if(!ibookingtable.isValid(bookingId)) return false;
		return ibookingtable.addRoomToBooking(roomTypeDescription, bookingId);		
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean cancelBooking(int bookingID) {
		if(!ibookingtable.isValid(bookingID)) return false;
		return ibookingtable.cancelBooking(bookingID);		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkInBooking(int bookingID) {
		if(!ibookingtable.isValid(bookingID)) return false;
		return ibookingtable.checkInBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkOutBooking(int bookingID) {
		if(!ibookingtable.isValid(bookingID)) return false;
			return ibookingtable.checkOutBooking(bookingID);	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(int bookingID, String roomType) {
		if(!ibookingtable.isValid(bookingID)) return -1;
		return ibookingtable.checkInRoom(bookingID, roomType);			
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkOutRoom(int bookingID, int roomNumber) {
		if(!ibookingtable.isValid(bookingID)) return false;
		return ibookingtable.checkOutRoom(bookingID, roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ITab getTabOfRoom(int bookingID, int roomNumber) {
		return ibookingtable.getTabOfRoom(bookingID, roomNumber);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ITab getTabOfBooking(int bookingID) {
		return ibookingtable.getTabOfBooking(bookingID);
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<TwoIntTuple> getOccupiedRooms(String date) {
		return ibookingtable.getOccupiedRooms(date);
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<StringIntTuple> getFreeRooms(String startDate, String endDate, int numberOfBeds) {
		if (!(isADate(startDate) && isADate(endDate))) return new BasicEList<StringIntTuple>();
		return (ibookingtable.getFreeRooms(startDate, endDate, numberOfBeds));
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getFreeRoomsForRoomType(String roomTypeDescription, String startDate, String endDate) {
		return ibookingtable.getFreeRoomsForRoomType(roomTypeDescription, startDate, endDate);	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> listCheckIns(String startDate, String endDate) {
		return ibookingtable.listCheckIns( startDate, endDate);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> listCheckOuts(String startDate, String endDate) {
		return ibookingtable.listCheckOuts(startDate, endDate);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateNameOfBooking(int bookingID, String firstName, String lastName) {
		if(ibookingtable.isValid(bookingID)){
		return ibookingtable.updateNameOfBooking(bookingID, firstName, lastName);
		}
		else{
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateDateOfBooking(int bookingID, String startDate, String endDate) {
		if(ibookingtable.isValid(bookingID)){
		return ibookingtable.updateDateOfBooking(bookingID, startDate, endDate);
		}
		else{
		 return false;
		}
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isValid(int bookingID) {
	 return ibookingtable.isValid(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		return ibookingtable.payDuringCheckout(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isBlockable(int roomNumber) {
		return ibookingtable.isBlockable(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean reset() {
		this.ibookingtable.reset();
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		return ibookingtable.payRoomDuringCheckout(roomNumber, ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> getBooking(String roomTypeDescription, String startDate, String endDate) {
		return ibookingtable.getBooking(roomTypeDescription, startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> listCheckOuts(String day) {
		return ibookingtable.listCheckOuts(day);
	}
	
	private boolean isADate(String str){
		try {
			int year  = Integer.parseInt(str.substring(0, 4));
			int month = Integer.parseInt(str.substring(4, 6));
			int day   = Integer.parseInt(str.substring(6, 8));
			
			if (year < 0 || month < 0 || month > 12 || day < 0) return false;
			switch (month){
				case 2:
					return day <= 29;
				case 4:
				case 6:
				case 9:
				case 11:
					return day <= 30;
				default :
					return day <= 31 ;
			}
		} catch (NumberFormatException nfe){
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.BOOKING_PROVIDER__IBOOKINGTABLE:
				if (resolve) return getIbookingtable();
				return basicGetIbookingtable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.BOOKING_PROVIDER__IBOOKINGTABLE:
				setIbookingtable((IBookingTable)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING_PROVIDER__IBOOKINGTABLE:
				setIbookingtable((IBookingTable)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING_PROVIDER__IBOOKINGTABLE:
				return ibookingtable != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackage.BOOKING_PROVIDER___LIST_CHECK_INS__STRING:
				return listCheckIns((String)arguments.get(0));
			case BookingPackage.BOOKING_PROVIDER___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case BookingPackage.BOOKING_PROVIDER___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKING_PROVIDER___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKING_PROVIDER___REMOVE_ROOM_FROM_BOOKING__STRING_INT:
				return removeRoomFromBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKING_PROVIDER___CANCEL_BOOKING__INT:
				return cancelBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKING_PROVIDER___CHECK_IN_BOOKING__INT:
				return checkInBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKING_PROVIDER___CHECK_OUT_BOOKING__INT:
				return checkOutBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKING_PROVIDER___CHECK_IN_ROOM__INT_STRING:
				return checkInRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case BookingPackage.BOOKING_PROVIDER___CHECK_OUT_ROOM__INT_INT:
				return checkOutRoom((Integer)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKING_PROVIDER___GET_TAB_OF_ROOM__INT_INT:
				return getTabOfRoom((Integer)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKING_PROVIDER___GET_TAB_OF_BOOKING__INT:
				return getTabOfBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKING_PROVIDER___GET_OCCUPIED_ROOMS__STRING:
				return getOccupiedRooms((String)arguments.get(0));
			case BookingPackage.BOOKING_PROVIDER___GET_FREE_ROOMS__STRING_STRING_INT:
				return getFreeRooms((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2));
			case BookingPackage.BOOKING_PROVIDER___GET_FREE_ROOMS_FOR_ROOM_TYPE__STRING_STRING_STRING:
				return getFreeRoomsForRoomType((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case BookingPackage.BOOKING_PROVIDER___LIST_CHECK_INS__STRING_STRING:
				return listCheckIns((String)arguments.get(0), (String)arguments.get(1));
			case BookingPackage.BOOKING_PROVIDER___LIST_CHECK_OUTS__STRING_STRING:
				return listCheckOuts((String)arguments.get(0), (String)arguments.get(1));
			case BookingPackage.BOOKING_PROVIDER___UPDATE_NAME_OF_BOOKING__INT_STRING_STRING:
				return updateNameOfBooking((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case BookingPackage.BOOKING_PROVIDER___UPDATE_DATE_OF_BOOKING__INT_STRING_STRING:
				return updateDateOfBooking((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case BookingPackage.BOOKING_PROVIDER___IS_VALID__INT:
				return isValid((Integer)arguments.get(0));
			case BookingPackage.BOOKING_PROVIDER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case BookingPackage.BOOKING_PROVIDER___IS_BLOCKABLE__INT:
				return isBlockable((Integer)arguments.get(0));
			case BookingPackage.BOOKING_PROVIDER___RESET:
				return reset();
			case BookingPackage.BOOKING_PROVIDER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case BookingPackage.BOOKING_PROVIDER___GET_BOOKING__STRING_STRING_STRING:
				return getBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case BookingPackage.BOOKING_PROVIDER___LIST_CHECK_OUTS__STRING:
				return listCheckOuts((String)arguments.get(0));
			case BookingPackage.BOOKING_PROVIDER___LIST_BOOKINGS:
				return listBookings();
			case BookingPackage.BOOKING_PROVIDER___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_INT:
				return addExtraCostToRoom((Integer)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3));
		}
		return super.eInvoke(operationID, arguments);
	}

} //BookingProviderImpl
