/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Booking;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room Type Int Tuple</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage#getRoomTypeIntTuple()
 * @model
 * @generated
 */
public interface RoomTypeIntTuple extends EObject {
} // RoomTypeIntTuple
