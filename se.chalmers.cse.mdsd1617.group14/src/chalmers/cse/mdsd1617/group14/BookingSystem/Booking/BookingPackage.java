/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Booking;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingFactory
 * @model kind="package"
 * @generated
 */
public interface BookingPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Booking";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///chalmers/cse/mdsd1617/group14/BookingSystem/Booking.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "chalmers.cse.mdsd1617.group14.BookingSystem.Booking";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BookingPackage eINSTANCE = chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl.init();

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking <em>IBooking</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getIBooking()
	 * @generated
	 */
	int IBOOKING = 0;

	/**
	 * The number of structural features of the '<em>IBooking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___INITIATE_BOOKING__STRING_STRING_STRING_STRING = 0;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___CONFIRM_BOOKING = 1;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___ADD_ROOM_TO_BOOKING__STRING = 2;

	/**
	 * The operation id for the '<em>Remove Room From Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___REMOVE_ROOM_FROM_BOOKING__STRING = 3;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___CANCEL_BOOKING = 4;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___CHECK_IN_BOOKING = 5;

	/**
	 * The operation id for the '<em>Check Out Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___CHECK_OUT_BOOKING = 6;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___CHECK_IN_ROOM__STRING = 7;

	/**
	 * The operation id for the '<em>Check Out Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___CHECK_OUT_ROOM__INT = 8;

	/**
	 * The operation id for the '<em>Get Tab Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___GET_TAB_OF_ROOM__INT = 9;

	/**
	 * The operation id for the '<em>Get ID</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___GET_ID = 10;

	/**
	 * The operation id for the '<em>Assign Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___ASSIGN_ROOM__INT = 11;

	/**
	 * The operation id for the '<em>Get Booking ID</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___GET_BOOKING_ID = 12;

	/**
	 * The operation id for the '<em>Get Tab Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___GET_TAB_OF_BOOKING = 13;

	/**
	 * The operation id for the '<em>List Assigned Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___LIST_ASSIGNED_ROOMS = 14;

	/**
	 * The operation id for the '<em>Get State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___GET_STATE = 15;

	/**
	 * The operation id for the '<em>Set State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___SET_STATE__STATE = 16;

	/**
	 * The operation id for the '<em>Get Start Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___GET_START_DATE = 17;

	/**
	 * The operation id for the '<em>Get End Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___GET_END_DATE = 18;

	/**
	 * The operation id for the '<em>Get Room Type Amount Tuples</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___GET_ROOM_TYPE_AMOUNT_TUPLES = 19;

	/**
	 * The operation id for the '<em>Unassign Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___UNASSIGN_ROOM__INT = 20;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING___TO_STRING = 21;

	/**
	 * The number of operations of the '<em>IBooking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_OPERATION_COUNT = 22;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider <em>IBooking Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getIBookingProvider()
	 * @generated
	 */
	int IBOOKING_PROVIDER = 2;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.TwoIntTupleImpl <em>Two Int Tuple</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.TwoIntTupleImpl
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getTwoIntTuple()
	 * @generated
	 */
	int TWO_INT_TUPLE = 3;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingProviderImpl <em>Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingProviderImpl
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getBookingProvider()
	 * @generated
	 */
	int BOOKING_PROVIDER = 4;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingImpl <em>Booking</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingImpl
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getBooking()
	 * @generated
	 */
	int BOOKING = 6;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable <em>IBooking Table</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getIBookingTable()
	 * @generated
	 */
	int IBOOKING_TABLE = 5;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.StringIntTupleImpl <em>String Int Tuple</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.StringIntTupleImpl
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getStringIntTuple()
	 * @generated
	 */
	int STRING_INT_TUPLE = 1;

	/**
	 * The feature id for the '<em><b>S</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_INT_TUPLE__S = 0;

	/**
	 * The feature id for the '<em><b>I</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_INT_TUPLE__I = 1;

	/**
	 * The number of structural features of the '<em>String Int Tuple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_INT_TUPLE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>String Int Tuple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_INT_TUPLE_OPERATION_COUNT = 0;

	/**
	 * The number of structural features of the '<em>IBooking Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___LIST_CHECK_INS__STRING = 0;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = 1;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___CONFIRM_BOOKING__INT = 2;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___ADD_ROOM_TO_BOOKING__STRING_INT = 3;

	/**
	 * The operation id for the '<em>Remove Room From Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___REMOVE_ROOM_FROM_BOOKING__STRING_INT = 4;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___CANCEL_BOOKING__INT = 5;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___CHECK_IN_BOOKING__INT = 6;

	/**
	 * The operation id for the '<em>Check Out Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___CHECK_OUT_BOOKING__INT = 7;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___CHECK_IN_ROOM__INT_STRING = 8;

	/**
	 * The operation id for the '<em>Check Out Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___CHECK_OUT_ROOM__INT_INT = 9;

	/**
	 * The operation id for the '<em>Get Tab Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___GET_TAB_OF_ROOM__INT_INT = 10;

	/**
	 * The operation id for the '<em>Get Tab Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___GET_TAB_OF_BOOKING__INT = 11;

	/**
	 * The operation id for the '<em>Get Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___GET_OCCUPIED_ROOMS__STRING = 12;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___GET_FREE_ROOMS__STRING_STRING_INT = 13;

	/**
	 * The operation id for the '<em>Get Free Rooms For Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___GET_FREE_ROOMS_FOR_ROOM_TYPE__STRING_STRING_STRING = 14;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___LIST_CHECK_INS__STRING_STRING = 15;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___LIST_CHECK_OUTS__STRING_STRING = 16;

	/**
	 * The operation id for the '<em>Update Name Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___UPDATE_NAME_OF_BOOKING__INT_STRING_STRING = 17;

	/**
	 * The operation id for the '<em>Update Date Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___UPDATE_DATE_OF_BOOKING__INT_STRING_STRING = 18;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___IS_VALID__INT = 19;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = 20;

	/**
	 * The operation id for the '<em>Is Blockable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___IS_BLOCKABLE__INT = 21;

	/**
	 * The operation id for the '<em>Reset</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___RESET = 22;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = 23;

	/**
	 * The operation id for the '<em>Get Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___GET_BOOKING__STRING_STRING_STRING = 24;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___LIST_CHECK_OUTS__STRING = 25;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___LIST_BOOKINGS = 26;

	/**
	 * The operation id for the '<em>Add Extra Cost To Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_INT = 27;

	/**
	 * The number of operations of the '<em>IBooking Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_PROVIDER_OPERATION_COUNT = 28;

	/**
	 * The feature id for the '<em><b>I1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TWO_INT_TUPLE__I1 = 0;

	/**
	 * The feature id for the '<em><b>I2</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TWO_INT_TUPLE__I2 = 1;

	/**
	 * The number of structural features of the '<em>Two Int Tuple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TWO_INT_TUPLE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Two Int Tuple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TWO_INT_TUPLE_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Ibookingtable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER__IBOOKINGTABLE = IBOOKING_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER_FEATURE_COUNT = IBOOKING_PROVIDER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___LIST_CHECK_INS__STRING = IBOOKING_PROVIDER___LIST_CHECK_INS__STRING;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IBOOKING_PROVIDER___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___CONFIRM_BOOKING__INT = IBOOKING_PROVIDER___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___ADD_ROOM_TO_BOOKING__STRING_INT = IBOOKING_PROVIDER___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Remove Room From Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___REMOVE_ROOM_FROM_BOOKING__STRING_INT = IBOOKING_PROVIDER___REMOVE_ROOM_FROM_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___CANCEL_BOOKING__INT = IBOOKING_PROVIDER___CANCEL_BOOKING__INT;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___CHECK_IN_BOOKING__INT = IBOOKING_PROVIDER___CHECK_IN_BOOKING__INT;

	/**
	 * The operation id for the '<em>Check Out Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___CHECK_OUT_BOOKING__INT = IBOOKING_PROVIDER___CHECK_OUT_BOOKING__INT;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___CHECK_IN_ROOM__INT_STRING = IBOOKING_PROVIDER___CHECK_IN_ROOM__INT_STRING;

	/**
	 * The operation id for the '<em>Check Out Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___CHECK_OUT_ROOM__INT_INT = IBOOKING_PROVIDER___CHECK_OUT_ROOM__INT_INT;

	/**
	 * The operation id for the '<em>Get Tab Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___GET_TAB_OF_ROOM__INT_INT = IBOOKING_PROVIDER___GET_TAB_OF_ROOM__INT_INT;

	/**
	 * The operation id for the '<em>Get Tab Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___GET_TAB_OF_BOOKING__INT = IBOOKING_PROVIDER___GET_TAB_OF_BOOKING__INT;

	/**
	 * The operation id for the '<em>Get Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___GET_OCCUPIED_ROOMS__STRING = IBOOKING_PROVIDER___GET_OCCUPIED_ROOMS__STRING;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___GET_FREE_ROOMS__STRING_STRING_INT = IBOOKING_PROVIDER___GET_FREE_ROOMS__STRING_STRING_INT;

	/**
	 * The operation id for the '<em>Get Free Rooms For Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___GET_FREE_ROOMS_FOR_ROOM_TYPE__STRING_STRING_STRING = IBOOKING_PROVIDER___GET_FREE_ROOMS_FOR_ROOM_TYPE__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___LIST_CHECK_INS__STRING_STRING = IBOOKING_PROVIDER___LIST_CHECK_INS__STRING_STRING;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___LIST_CHECK_OUTS__STRING_STRING = IBOOKING_PROVIDER___LIST_CHECK_OUTS__STRING_STRING;

	/**
	 * The operation id for the '<em>Update Name Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___UPDATE_NAME_OF_BOOKING__INT_STRING_STRING = IBOOKING_PROVIDER___UPDATE_NAME_OF_BOOKING__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Update Date Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___UPDATE_DATE_OF_BOOKING__INT_STRING_STRING = IBOOKING_PROVIDER___UPDATE_DATE_OF_BOOKING__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___IS_VALID__INT = IBOOKING_PROVIDER___IS_VALID__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = IBOOKING_PROVIDER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Is Blockable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___IS_BLOCKABLE__INT = IBOOKING_PROVIDER___IS_BLOCKABLE__INT;

	/**
	 * The operation id for the '<em>Reset</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___RESET = IBOOKING_PROVIDER___RESET;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = IBOOKING_PROVIDER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Get Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___GET_BOOKING__STRING_STRING_STRING = IBOOKING_PROVIDER___GET_BOOKING__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___LIST_CHECK_OUTS__STRING = IBOOKING_PROVIDER___LIST_CHECK_OUTS__STRING;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___LIST_BOOKINGS = IBOOKING_PROVIDER___LIST_BOOKINGS;

	/**
	 * The operation id for the '<em>Add Extra Cost To Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_INT = IBOOKING_PROVIDER___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_INT;

	/**
	 * The number of operations of the '<em>Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_PROVIDER_OPERATION_COUNT = IBOOKING_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IBooking Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___LIST_CHECK_INS__STRING = 0;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___INITIATE_BOOKING__STRING_STRING_STRING_STRING = 1;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___CONFIRM_BOOKING__INT = 2;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___ADD_ROOM_TO_BOOKING__STRING_INT = 3;

	/**
	 * The operation id for the '<em>Remove Room From Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___REMOVE_ROOM_FROM_BOOKING__STRING_INT = 4;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___CANCEL_BOOKING__INT = 5;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___CHECK_IN_BOOKING__INT = 6;

	/**
	 * The operation id for the '<em>Check Out Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___CHECK_OUT_BOOKING__INT = 7;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___CHECK_IN_ROOM__INT_STRING = 8;

	/**
	 * The operation id for the '<em>Check Out Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___CHECK_OUT_ROOM__INT_INT = 9;

	/**
	 * The operation id for the '<em>Get Tab Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___GET_TAB_OF_ROOM__INT_INT = 10;

	/**
	 * The operation id for the '<em>Get Tab Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___GET_TAB_OF_BOOKING__INT = 11;

	/**
	 * The operation id for the '<em>Get Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___GET_OCCUPIED_ROOMS__STRING = 12;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___GET_FREE_ROOMS__STRING_STRING_INT = 13;

	/**
	 * The operation id for the '<em>Get Free Rooms For Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___GET_FREE_ROOMS_FOR_ROOM_TYPE__STRING_STRING_STRING = 14;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___LIST_CHECK_INS__STRING_STRING = 15;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___LIST_CHECK_OUTS__STRING_STRING = 16;

	/**
	 * The operation id for the '<em>Update Name Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___UPDATE_NAME_OF_BOOKING__INT_STRING_STRING = 17;

	/**
	 * The operation id for the '<em>Update Date Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___UPDATE_DATE_OF_BOOKING__INT_STRING_STRING = 18;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___IS_VALID__INT = 19;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = 20;

	/**
	 * The operation id for the '<em>Is Blockable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___IS_BLOCKABLE__INT = 21;

	/**
	 * The operation id for the '<em>Reset</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___RESET = 22;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = 23;

	/**
	 * The operation id for the '<em>Get Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___GET_BOOKING__STRING_STRING_STRING = 24;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___LIST_BOOKINGS = 25;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___LIST_CHECK_OUTS__STRING = 26;

	/**
	 * The operation id for the '<em>Add Extra Cost To Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_INT = 27;

	/**
	 * The number of operations of the '<em>IBooking Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_TABLE_OPERATION_COUNT = 28;

	/**
	 * The feature id for the '<em><b>Number Of Rooms</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__NUMBER_OF_ROOMS = IBOOKING_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Assigned Rooms</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__ASSIGNED_ROOMS = IBOOKING_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Booking ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__BOOKING_ID = IBOOKING_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Room Type Amount Tuples</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__ROOM_TYPE_AMOUNT_TUPLES = IBOOKING_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>End Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__END_DATE = IBOOKING_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__FIRST_NAME = IBOOKING_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__LAST_NAME = IBOOKING_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__STATE = IBOOKING_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Start Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__START_DATE = IBOOKING_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_FEATURE_COUNT = IBOOKING_FEATURE_COUNT + 9;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IBOOKING___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CONFIRM_BOOKING = IBOOKING___CONFIRM_BOOKING;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___ADD_ROOM_TO_BOOKING__STRING = IBOOKING___ADD_ROOM_TO_BOOKING__STRING;

	/**
	 * The operation id for the '<em>Remove Room From Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___REMOVE_ROOM_FROM_BOOKING__STRING = IBOOKING___REMOVE_ROOM_FROM_BOOKING__STRING;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CANCEL_BOOKING = IBOOKING___CANCEL_BOOKING;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CHECK_IN_BOOKING = IBOOKING___CHECK_IN_BOOKING;

	/**
	 * The operation id for the '<em>Check Out Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CHECK_OUT_BOOKING = IBOOKING___CHECK_OUT_BOOKING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CHECK_IN_ROOM__STRING = IBOOKING___CHECK_IN_ROOM__STRING;

	/**
	 * The operation id for the '<em>Check Out Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___CHECK_OUT_ROOM__INT = IBOOKING___CHECK_OUT_ROOM__INT;

	/**
	 * The operation id for the '<em>Get Tab Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_TAB_OF_ROOM__INT = IBOOKING___GET_TAB_OF_ROOM__INT;

	/**
	 * The operation id for the '<em>Get ID</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_ID = IBOOKING___GET_ID;

	/**
	 * The operation id for the '<em>Assign Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___ASSIGN_ROOM__INT = IBOOKING___ASSIGN_ROOM__INT;

	/**
	 * The operation id for the '<em>Get Booking ID</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_BOOKING_ID = IBOOKING___GET_BOOKING_ID;

	/**
	 * The operation id for the '<em>Get Tab Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_TAB_OF_BOOKING = IBOOKING___GET_TAB_OF_BOOKING;

	/**
	 * The operation id for the '<em>List Assigned Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___LIST_ASSIGNED_ROOMS = IBOOKING___LIST_ASSIGNED_ROOMS;

	/**
	 * The operation id for the '<em>Get State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_STATE = IBOOKING___GET_STATE;

	/**
	 * The operation id for the '<em>Set State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___SET_STATE__STATE = IBOOKING___SET_STATE__STATE;

	/**
	 * The operation id for the '<em>Get Start Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_START_DATE = IBOOKING___GET_START_DATE;

	/**
	 * The operation id for the '<em>Get End Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_END_DATE = IBOOKING___GET_END_DATE;

	/**
	 * The operation id for the '<em>Get Room Type Amount Tuples</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___GET_ROOM_TYPE_AMOUNT_TUPLES = IBOOKING___GET_ROOM_TYPE_AMOUNT_TUPLES;

	/**
	 * The operation id for the '<em>Unassign Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___UNASSIGN_ROOM__INT = IBOOKING___UNASSIGN_ROOM__INT;

	/**
	 * The operation id for the '<em>To String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___TO_STRING = IBOOKING___TO_STRING;

	/**
	 * The number of operations of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_OPERATION_COUNT = IBOOKING_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingTableImpl <em>Table</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingTableImpl
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getBookingTable()
	 * @generated
	 */
	int BOOKING_TABLE = 7;

	/**
	 * The feature id for the '<em><b>Bookings</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE__BOOKINGS = IBOOKING_TABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE_FEATURE_COUNT = IBOOKING_TABLE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___LIST_CHECK_INS__STRING = IBOOKING_TABLE___LIST_CHECK_INS__STRING;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IBOOKING_TABLE___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___CONFIRM_BOOKING__INT = IBOOKING_TABLE___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___ADD_ROOM_TO_BOOKING__STRING_INT = IBOOKING_TABLE___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Remove Room From Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___REMOVE_ROOM_FROM_BOOKING__STRING_INT = IBOOKING_TABLE___REMOVE_ROOM_FROM_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___CANCEL_BOOKING__INT = IBOOKING_TABLE___CANCEL_BOOKING__INT;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___CHECK_IN_BOOKING__INT = IBOOKING_TABLE___CHECK_IN_BOOKING__INT;

	/**
	 * The operation id for the '<em>Check Out Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___CHECK_OUT_BOOKING__INT = IBOOKING_TABLE___CHECK_OUT_BOOKING__INT;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___CHECK_IN_ROOM__INT_STRING = IBOOKING_TABLE___CHECK_IN_ROOM__INT_STRING;

	/**
	 * The operation id for the '<em>Check Out Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___CHECK_OUT_ROOM__INT_INT = IBOOKING_TABLE___CHECK_OUT_ROOM__INT_INT;

	/**
	 * The operation id for the '<em>Get Tab Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___GET_TAB_OF_ROOM__INT_INT = IBOOKING_TABLE___GET_TAB_OF_ROOM__INT_INT;

	/**
	 * The operation id for the '<em>Get Tab Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___GET_TAB_OF_BOOKING__INT = IBOOKING_TABLE___GET_TAB_OF_BOOKING__INT;

	/**
	 * The operation id for the '<em>Get Occupied Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___GET_OCCUPIED_ROOMS__STRING = IBOOKING_TABLE___GET_OCCUPIED_ROOMS__STRING;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___GET_FREE_ROOMS__STRING_STRING_INT = IBOOKING_TABLE___GET_FREE_ROOMS__STRING_STRING_INT;

	/**
	 * The operation id for the '<em>Get Free Rooms For Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___GET_FREE_ROOMS_FOR_ROOM_TYPE__STRING_STRING_STRING = IBOOKING_TABLE___GET_FREE_ROOMS_FOR_ROOM_TYPE__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___LIST_CHECK_INS__STRING_STRING = IBOOKING_TABLE___LIST_CHECK_INS__STRING_STRING;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___LIST_CHECK_OUTS__STRING_STRING = IBOOKING_TABLE___LIST_CHECK_OUTS__STRING_STRING;

	/**
	 * The operation id for the '<em>Update Name Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___UPDATE_NAME_OF_BOOKING__INT_STRING_STRING = IBOOKING_TABLE___UPDATE_NAME_OF_BOOKING__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Update Date Of Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___UPDATE_DATE_OF_BOOKING__INT_STRING_STRING = IBOOKING_TABLE___UPDATE_DATE_OF_BOOKING__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___IS_VALID__INT = IBOOKING_TABLE___IS_VALID__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = IBOOKING_TABLE___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Is Blockable</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___IS_BLOCKABLE__INT = IBOOKING_TABLE___IS_BLOCKABLE__INT;

	/**
	 * The operation id for the '<em>Reset</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___RESET = IBOOKING_TABLE___RESET;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = IBOOKING_TABLE___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Get Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___GET_BOOKING__STRING_STRING_STRING = IBOOKING_TABLE___GET_BOOKING__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___LIST_BOOKINGS = IBOOKING_TABLE___LIST_BOOKINGS;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___LIST_CHECK_OUTS__STRING = IBOOKING_TABLE___LIST_CHECK_OUTS__STRING;

	/**
	 * The operation id for the '<em>Add Extra Cost To Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_INT = IBOOKING_TABLE___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_INT;

	/**
	 * The number of operations of the '<em>Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_TABLE_OPERATION_COUNT = IBOOKING_TABLE_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.IntDTOTupleImpl <em>Int DTO Tuple</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.IntDTOTupleImpl
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getIntDTOTuple()
	 * @generated
	 */
	int INT_DTO_TUPLE = 8;

	/**
	 * The feature id for the '<em><b>I1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_DTO_TUPLE__I1 = 0;

	/**
	 * The feature id for the '<em><b>Free Room Types DTO</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_DTO_TUPLE__FREE_ROOM_TYPES_DTO = 1;

	/**
	 * The number of structural features of the '<em>Int DTO Tuple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_DTO_TUPLE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Int DTO Tuple</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_DTO_TUPLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.State <em>State</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.State
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getState()
	 * @generated
	 */
	int STATE = 9;


	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking <em>IBooking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IBooking</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking
	 * @generated
	 */
	EClass getIBooking();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Initiate Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBooking__InitiateBooking__String_String_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#confirmBooking() <em>Confirm Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#confirmBooking()
	 * @generated
	 */
	EOperation getIBooking__ConfirmBooking();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#addRoomToBooking(java.lang.String) <em>Add Room To Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room To Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#addRoomToBooking(java.lang.String)
	 * @generated
	 */
	EOperation getIBooking__AddRoomToBooking__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#removeRoomFromBooking(java.lang.String) <em>Remove Room From Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room From Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#removeRoomFromBooking(java.lang.String)
	 * @generated
	 */
	EOperation getIBooking__RemoveRoomFromBooking__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#cancelBooking() <em>Cancel Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#cancelBooking()
	 * @generated
	 */
	EOperation getIBooking__CancelBooking();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#checkInBooking() <em>Check In Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#checkInBooking()
	 * @generated
	 */
	EOperation getIBooking__CheckInBooking();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#checkOutBooking() <em>Check Out Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#checkOutBooking()
	 * @generated
	 */
	EOperation getIBooking__CheckOutBooking();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#checkInRoom(java.lang.String) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#checkInRoom(java.lang.String)
	 * @generated
	 */
	EOperation getIBooking__CheckInRoom__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#checkOutRoom(int) <em>Check Out Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#checkOutRoom(int)
	 * @generated
	 */
	EOperation getIBooking__CheckOutRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#getTabOfRoom(int) <em>Get Tab Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Tab Of Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#getTabOfRoom(int)
	 * @generated
	 */
	EOperation getIBooking__GetTabOfRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#getID() <em>Get ID</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get ID</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#getID()
	 * @generated
	 */
	EOperation getIBooking__GetID();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#getBookingID() <em>Get Booking ID</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking ID</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#getBookingID()
	 * @generated
	 */
	EOperation getIBooking__GetBookingID();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#getTabOfBooking() <em>Get Tab Of Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Tab Of Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#getTabOfBooking()
	 * @generated
	 */
	EOperation getIBooking__GetTabOfBooking();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#listAssignedRooms() <em>List Assigned Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Assigned Rooms</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#listAssignedRooms()
	 * @generated
	 */
	EOperation getIBooking__ListAssignedRooms();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#getState() <em>Get State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get State</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#getState()
	 * @generated
	 */
	EOperation getIBooking__GetState();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#setState(chalmers.cse.mdsd1617.group14.BookingSystem.Booking.State) <em>Set State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set State</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#setState(chalmers.cse.mdsd1617.group14.BookingSystem.Booking.State)
	 * @generated
	 */
	EOperation getIBooking__SetState__State();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#getStartDate() <em>Get Start Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Start Date</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#getStartDate()
	 * @generated
	 */
	EOperation getIBooking__GetStartDate();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#getEndDate() <em>Get End Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get End Date</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#getEndDate()
	 * @generated
	 */
	EOperation getIBooking__GetEndDate();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#getRoomTypeAmountTuples() <em>Get Room Type Amount Tuples</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Type Amount Tuples</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#getRoomTypeAmountTuples()
	 * @generated
	 */
	EOperation getIBooking__GetRoomTypeAmountTuples();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#unassignRoom(int) <em>Unassign Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unassign Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#unassignRoom(int)
	 * @generated
	 */
	EOperation getIBooking__UnassignRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#toString() <em>To String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>To String</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#toString()
	 * @generated
	 */
	EOperation getIBooking__ToString();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#assignRoom(int) <em>Assign Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Assign Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking#assignRoom(int)
	 * @generated
	 */
	EOperation getIBooking__AssignRoom__int();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider <em>IBooking Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IBooking Provider</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider
	 * @generated
	 */
	EClass getIBookingProvider();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#listCheckIns(java.lang.String) <em>List Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#listCheckIns(java.lang.String)
	 * @generated
	 */
	EOperation getIBookingProvider__ListCheckIns__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Initiate Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingProvider__InitiateBooking__String_String_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#confirmBooking(int) <em>Confirm Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#confirmBooking(int)
	 * @generated
	 */
	EOperation getIBookingProvider__ConfirmBooking__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#addRoomToBooking(java.lang.String, int) <em>Add Room To Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room To Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#addRoomToBooking(java.lang.String, int)
	 * @generated
	 */
	EOperation getIBookingProvider__AddRoomToBooking__String_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#removeRoomFromBooking(java.lang.String, int) <em>Remove Room From Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room From Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#removeRoomFromBooking(java.lang.String, int)
	 * @generated
	 */
	EOperation getIBookingProvider__RemoveRoomFromBooking__String_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#cancelBooking(int) <em>Cancel Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#cancelBooking(int)
	 * @generated
	 */
	EOperation getIBookingProvider__CancelBooking__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#checkInBooking(int) <em>Check In Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#checkInBooking(int)
	 * @generated
	 */
	EOperation getIBookingProvider__CheckInBooking__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#checkOutBooking(int) <em>Check Out Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#checkOutBooking(int)
	 * @generated
	 */
	EOperation getIBookingProvider__CheckOutBooking__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#checkInRoom(int, java.lang.String) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#checkInRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingProvider__CheckInRoom__int_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#checkOutRoom(int, int) <em>Check Out Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#checkOutRoom(int, int)
	 * @generated
	 */
	EOperation getIBookingProvider__CheckOutRoom__int_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#getTabOfRoom(int, int) <em>Get Tab Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Tab Of Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#getTabOfRoom(int, int)
	 * @generated
	 */
	EOperation getIBookingProvider__GetTabOfRoom__int_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#getTabOfBooking(int) <em>Get Tab Of Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Tab Of Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#getTabOfBooking(int)
	 * @generated
	 */
	EOperation getIBookingProvider__GetTabOfBooking__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#getOccupiedRooms(java.lang.String) <em>Get Occupied Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Occupied Rooms</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#getOccupiedRooms(java.lang.String)
	 * @generated
	 */
	EOperation getIBookingProvider__GetOccupiedRooms__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#getFreeRooms(java.lang.String, java.lang.String, int) <em>Get Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#getFreeRooms(java.lang.String, java.lang.String, int)
	 * @generated
	 */
	EOperation getIBookingProvider__GetFreeRooms__String_String_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#getFreeRoomsForRoomType(java.lang.String, java.lang.String, java.lang.String) <em>Get Free Rooms For Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms For Room Type</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#getFreeRoomsForRoomType(java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingProvider__GetFreeRoomsForRoomType__String_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#listCheckIns(java.lang.String, java.lang.String) <em>List Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#listCheckIns(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingProvider__ListCheckIns__String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#listCheckOuts(java.lang.String, java.lang.String) <em>List Check Outs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#listCheckOuts(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingProvider__ListCheckOuts__String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#updateNameOfBooking(int, java.lang.String, java.lang.String) <em>Update Name Of Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Name Of Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#updateNameOfBooking(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingProvider__UpdateNameOfBooking__int_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#updateDateOfBooking(int, java.lang.String, java.lang.String) <em>Update Date Of Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Date Of Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#updateDateOfBooking(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingProvider__UpdateDateOfBooking__int_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#isValid(int) <em>Is Valid</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#isValid(int)
	 * @generated
	 */
	EOperation getIBookingProvider__IsValid__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay During Checkout</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingProvider__PayDuringCheckout__String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#isBlockable(int) <em>Is Blockable</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Blockable</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#isBlockable(int)
	 * @generated
	 */
	EOperation getIBookingProvider__IsBlockable__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#reset() <em>Reset</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Reset</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#reset()
	 * @generated
	 */
	EOperation getIBookingProvider__Reset();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay Room During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Room During Checkout</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingProvider__PayRoomDuringCheckout__int_String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#getBooking(java.lang.String, java.lang.String, java.lang.String) <em>Get Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#getBooking(java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingProvider__GetBooking__String_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#listCheckOuts(java.lang.String) <em>List Check Outs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#listCheckOuts(java.lang.String)
	 * @generated
	 */
	EOperation getIBookingProvider__ListCheckOuts__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#listBookings() <em>List Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Bookings</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#listBookings()
	 * @generated
	 */
	EOperation getIBookingProvider__ListBookings();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#addExtraCostToRoom(int, int, java.lang.String, int) <em>Add Extra Cost To Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra Cost To Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider#addExtraCostToRoom(int, int, java.lang.String, int)
	 * @generated
	 */
	EOperation getIBookingProvider__AddExtraCostToRoom__int_int_String_int();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple <em>Two Int Tuple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Two Int Tuple</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple
	 * @generated
	 */
	EClass getTwoIntTuple();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple#getI1 <em>I1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>I1</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple#getI1()
	 * @see #getTwoIntTuple()
	 * @generated
	 */
	EAttribute getTwoIntTuple_I1();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple#getI2 <em>I2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>I2</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple#getI2()
	 * @see #getTwoIntTuple()
	 * @generated
	 */
	EAttribute getTwoIntTuple_I2();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.StringIntTuple <em>String Int Tuple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Int Tuple</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.StringIntTuple
	 * @generated
	 */
	EClass getStringIntTuple();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.StringIntTuple#getS <em>S</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>S</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.StringIntTuple#getS()
	 * @see #getStringIntTuple()
	 * @generated
	 */
	EAttribute getStringIntTuple_S();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.StringIntTuple#getI <em>I</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>I</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.StringIntTuple#getI()
	 * @see #getStringIntTuple()
	 * @generated
	 */
	EAttribute getStringIntTuple_I();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingProvider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provider</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingProvider
	 * @generated
	 */
	EClass getBookingProvider();

	/**
	 * Returns the meta object for the reference '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingProvider#getIbookingtable <em>Ibookingtable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ibookingtable</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingProvider#getIbookingtable()
	 * @see #getBookingProvider()
	 * @generated
	 */
	EReference getBookingProvider_Ibookingtable();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking <em>Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booking</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking
	 * @generated
	 */
	EClass getBooking();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getNumberOfRooms <em>Number Of Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Of Rooms</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getNumberOfRooms()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_NumberOfRooms();

	/**
	 * Returns the meta object for the attribute list '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getAssignedRooms <em>Assigned Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Assigned Rooms</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getAssignedRooms()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_AssignedRooms();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getBookingID <em>Booking ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking ID</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getBookingID()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_BookingID();

	/**
	 * Returns the meta object for the reference list '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getRoomTypeAmountTuples <em>Room Type Amount Tuples</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Room Type Amount Tuples</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getRoomTypeAmountTuples()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_RoomTypeAmountTuples();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getState <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>State</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getState()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_State();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getStartDate <em>Start Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Date</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getStartDate()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_StartDate();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getEndDate <em>End Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Date</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getEndDate()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_EndDate();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getFirstName <em>First Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First Name</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getFirstName()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_FirstName();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getLastName <em>Last Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Name</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking#getLastName()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_LastName();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable <em>IBooking Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IBooking Table</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable
	 * @generated
	 */
	EClass getIBookingTable();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#listCheckIns(java.lang.String) <em>List Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#listCheckIns(java.lang.String)
	 * @generated
	 */
	EOperation getIBookingTable__ListCheckIns__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Initiate Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingTable__InitiateBooking__String_String_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#confirmBooking(int) <em>Confirm Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#confirmBooking(int)
	 * @generated
	 */
	EOperation getIBookingTable__ConfirmBooking__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#addRoomToBooking(java.lang.String, int) <em>Add Room To Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room To Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#addRoomToBooking(java.lang.String, int)
	 * @generated
	 */
	EOperation getIBookingTable__AddRoomToBooking__String_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#removeRoomFromBooking(java.lang.String, int) <em>Remove Room From Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room From Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#removeRoomFromBooking(java.lang.String, int)
	 * @generated
	 */
	EOperation getIBookingTable__RemoveRoomFromBooking__String_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#cancelBooking(int) <em>Cancel Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#cancelBooking(int)
	 * @generated
	 */
	EOperation getIBookingTable__CancelBooking__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#checkInBooking(int) <em>Check In Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#checkInBooking(int)
	 * @generated
	 */
	EOperation getIBookingTable__CheckInBooking__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#checkOutBooking(int) <em>Check Out Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#checkOutBooking(int)
	 * @generated
	 */
	EOperation getIBookingTable__CheckOutBooking__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#checkInRoom(int, java.lang.String) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#checkInRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingTable__CheckInRoom__int_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#checkOutRoom(int, int) <em>Check Out Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#checkOutRoom(int, int)
	 * @generated
	 */
	EOperation getIBookingTable__CheckOutRoom__int_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#getTabOfRoom(int, int) <em>Get Tab Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Tab Of Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#getTabOfRoom(int, int)
	 * @generated
	 */
	EOperation getIBookingTable__GetTabOfRoom__int_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#getTabOfBooking(int) <em>Get Tab Of Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Tab Of Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#getTabOfBooking(int)
	 * @generated
	 */
	EOperation getIBookingTable__GetTabOfBooking__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#getOccupiedRooms(java.lang.String) <em>Get Occupied Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Occupied Rooms</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#getOccupiedRooms(java.lang.String)
	 * @generated
	 */
	EOperation getIBookingTable__GetOccupiedRooms__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#getFreeRooms(java.lang.String, java.lang.String, int) <em>Get Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#getFreeRooms(java.lang.String, java.lang.String, int)
	 * @generated
	 */
	EOperation getIBookingTable__GetFreeRooms__String_String_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#getFreeRoomsForRoomType(java.lang.String, java.lang.String, java.lang.String) <em>Get Free Rooms For Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms For Room Type</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#getFreeRoomsForRoomType(java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingTable__GetFreeRoomsForRoomType__String_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#listCheckIns(java.lang.String, java.lang.String) <em>List Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#listCheckIns(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingTable__ListCheckIns__String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#listCheckOuts(java.lang.String, java.lang.String) <em>List Check Outs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#listCheckOuts(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingTable__ListCheckOuts__String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#updateNameOfBooking(int, java.lang.String, java.lang.String) <em>Update Name Of Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Name Of Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#updateNameOfBooking(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingTable__UpdateNameOfBooking__int_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#updateDateOfBooking(int, java.lang.String, java.lang.String) <em>Update Date Of Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Date Of Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#updateDateOfBooking(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingTable__UpdateDateOfBooking__int_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#isValid(int) <em>Is Valid</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#isValid(int)
	 * @generated
	 */
	EOperation getIBookingTable__IsValid__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay During Checkout</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingTable__PayDuringCheckout__String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#isBlockable(int) <em>Is Blockable</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Blockable</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#isBlockable(int)
	 * @generated
	 */
	EOperation getIBookingTable__IsBlockable__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#reset() <em>Reset</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Reset</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#reset()
	 * @generated
	 */
	EOperation getIBookingTable__Reset();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay Room During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Room During Checkout</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingTable__PayRoomDuringCheckout__int_String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#getBooking(java.lang.String, java.lang.String, java.lang.String) <em>Get Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#getBooking(java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingTable__GetBooking__String_String_String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#listBookings() <em>List Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Bookings</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#listBookings()
	 * @generated
	 */
	EOperation getIBookingTable__ListBookings();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#listCheckOuts(java.lang.String) <em>List Check Outs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#listCheckOuts(java.lang.String)
	 * @generated
	 */
	EOperation getIBookingTable__ListCheckOuts__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#addExtraCostToRoom(int, int, java.lang.String, int) <em>Add Extra Cost To Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra Cost To Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable#addExtraCostToRoom(int, int, java.lang.String, int)
	 * @generated
	 */
	EOperation getIBookingTable__AddExtraCostToRoom__int_int_String_int();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingTable
	 * @generated
	 */
	EClass getBookingTable();

	/**
	 * Returns the meta object for the reference list '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingTable#getBookings <em>Bookings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Bookings</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingTable#getBookings()
	 * @see #getBookingTable()
	 * @generated
	 */
	EReference getBookingTable_Bookings();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IntDTOTuple <em>Int DTO Tuple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int DTO Tuple</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IntDTOTuple
	 * @generated
	 */
	EClass getIntDTOTuple();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IntDTOTuple#getI1 <em>I1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>I1</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IntDTOTuple#getI1()
	 * @see #getIntDTOTuple()
	 * @generated
	 */
	EAttribute getIntDTOTuple_I1();

	/**
	 * Returns the meta object for the containment reference '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IntDTOTuple#getFreeRoomTypesDTO <em>Free Room Types DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Free Room Types DTO</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IntDTOTuple#getFreeRoomTypesDTO()
	 * @see #getIntDTOTuple()
	 * @generated
	 */
	EReference getIntDTOTuple_FreeRoomTypesDTO();

	/**
	 * Returns the meta object for enum '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.State <em>State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>State</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.State
	 * @generated
	 */
	EEnum getState();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BookingFactory getBookingFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking <em>IBooking</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getIBooking()
		 * @generated
		 */
		EClass IBOOKING = eINSTANCE.getIBooking();

		/**
		 * The meta object literal for the '<em><b>Initiate Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___INITIATE_BOOKING__STRING_STRING_STRING_STRING = eINSTANCE.getIBooking__InitiateBooking__String_String_String_String();

		/**
		 * The meta object literal for the '<em><b>Confirm Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___CONFIRM_BOOKING = eINSTANCE.getIBooking__ConfirmBooking();

		/**
		 * The meta object literal for the '<em><b>Add Room To Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___ADD_ROOM_TO_BOOKING__STRING = eINSTANCE.getIBooking__AddRoomToBooking__String();

		/**
		 * The meta object literal for the '<em><b>Remove Room From Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___REMOVE_ROOM_FROM_BOOKING__STRING = eINSTANCE.getIBooking__RemoveRoomFromBooking__String();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___CANCEL_BOOKING = eINSTANCE.getIBooking__CancelBooking();

		/**
		 * The meta object literal for the '<em><b>Check In Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___CHECK_IN_BOOKING = eINSTANCE.getIBooking__CheckInBooking();

		/**
		 * The meta object literal for the '<em><b>Check Out Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___CHECK_OUT_BOOKING = eINSTANCE.getIBooking__CheckOutBooking();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___CHECK_IN_ROOM__STRING = eINSTANCE.getIBooking__CheckInRoom__String();

		/**
		 * The meta object literal for the '<em><b>Check Out Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___CHECK_OUT_ROOM__INT = eINSTANCE.getIBooking__CheckOutRoom__int();

		/**
		 * The meta object literal for the '<em><b>Get Tab Of Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___GET_TAB_OF_ROOM__INT = eINSTANCE.getIBooking__GetTabOfRoom__int();

		/**
		 * The meta object literal for the '<em><b>Get ID</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___GET_ID = eINSTANCE.getIBooking__GetID();

		/**
		 * The meta object literal for the '<em><b>Get Booking ID</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___GET_BOOKING_ID = eINSTANCE.getIBooking__GetBookingID();

		/**
		 * The meta object literal for the '<em><b>Get Tab Of Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___GET_TAB_OF_BOOKING = eINSTANCE.getIBooking__GetTabOfBooking();

		/**
		 * The meta object literal for the '<em><b>List Assigned Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___LIST_ASSIGNED_ROOMS = eINSTANCE.getIBooking__ListAssignedRooms();

		/**
		 * The meta object literal for the '<em><b>Get State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___GET_STATE = eINSTANCE.getIBooking__GetState();

		/**
		 * The meta object literal for the '<em><b>Set State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___SET_STATE__STATE = eINSTANCE.getIBooking__SetState__State();

		/**
		 * The meta object literal for the '<em><b>Get Start Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___GET_START_DATE = eINSTANCE.getIBooking__GetStartDate();

		/**
		 * The meta object literal for the '<em><b>Get End Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___GET_END_DATE = eINSTANCE.getIBooking__GetEndDate();

		/**
		 * The meta object literal for the '<em><b>Get Room Type Amount Tuples</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___GET_ROOM_TYPE_AMOUNT_TUPLES = eINSTANCE.getIBooking__GetRoomTypeAmountTuples();

		/**
		 * The meta object literal for the '<em><b>Unassign Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___UNASSIGN_ROOM__INT = eINSTANCE.getIBooking__UnassignRoom__int();

		/**
		 * The meta object literal for the '<em><b>To String</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___TO_STRING = eINSTANCE.getIBooking__ToString();

		/**
		 * The meta object literal for the '<em><b>Assign Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING___ASSIGN_ROOM__INT = eINSTANCE.getIBooking__AssignRoom__int();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider <em>IBooking Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getIBookingProvider()
		 * @generated
		 */
		EClass IBOOKING_PROVIDER = eINSTANCE.getIBookingProvider();

		/**
		 * The meta object literal for the '<em><b>List Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___LIST_CHECK_INS__STRING = eINSTANCE.getIBookingProvider__ListCheckIns__String();

		/**
		 * The meta object literal for the '<em><b>Initiate Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = eINSTANCE.getIBookingProvider__InitiateBooking__String_String_String_String();

		/**
		 * The meta object literal for the '<em><b>Confirm Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___CONFIRM_BOOKING__INT = eINSTANCE.getIBookingProvider__ConfirmBooking__int();

		/**
		 * The meta object literal for the '<em><b>Add Room To Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___ADD_ROOM_TO_BOOKING__STRING_INT = eINSTANCE.getIBookingProvider__AddRoomToBooking__String_int();

		/**
		 * The meta object literal for the '<em><b>Remove Room From Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___REMOVE_ROOM_FROM_BOOKING__STRING_INT = eINSTANCE.getIBookingProvider__RemoveRoomFromBooking__String_int();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___CANCEL_BOOKING__INT = eINSTANCE.getIBookingProvider__CancelBooking__int();

		/**
		 * The meta object literal for the '<em><b>Check In Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___CHECK_IN_BOOKING__INT = eINSTANCE.getIBookingProvider__CheckInBooking__int();

		/**
		 * The meta object literal for the '<em><b>Check Out Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___CHECK_OUT_BOOKING__INT = eINSTANCE.getIBookingProvider__CheckOutBooking__int();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___CHECK_IN_ROOM__INT_STRING = eINSTANCE.getIBookingProvider__CheckInRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Check Out Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___CHECK_OUT_ROOM__INT_INT = eINSTANCE.getIBookingProvider__CheckOutRoom__int_int();

		/**
		 * The meta object literal for the '<em><b>Get Tab Of Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___GET_TAB_OF_ROOM__INT_INT = eINSTANCE.getIBookingProvider__GetTabOfRoom__int_int();

		/**
		 * The meta object literal for the '<em><b>Get Tab Of Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___GET_TAB_OF_BOOKING__INT = eINSTANCE.getIBookingProvider__GetTabOfBooking__int();

		/**
		 * The meta object literal for the '<em><b>Get Occupied Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___GET_OCCUPIED_ROOMS__STRING = eINSTANCE.getIBookingProvider__GetOccupiedRooms__String();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___GET_FREE_ROOMS__STRING_STRING_INT = eINSTANCE.getIBookingProvider__GetFreeRooms__String_String_int();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms For Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___GET_FREE_ROOMS_FOR_ROOM_TYPE__STRING_STRING_STRING = eINSTANCE.getIBookingProvider__GetFreeRoomsForRoomType__String_String_String();

		/**
		 * The meta object literal for the '<em><b>List Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___LIST_CHECK_INS__STRING_STRING = eINSTANCE.getIBookingProvider__ListCheckIns__String_String();

		/**
		 * The meta object literal for the '<em><b>List Check Outs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___LIST_CHECK_OUTS__STRING_STRING = eINSTANCE.getIBookingProvider__ListCheckOuts__String_String();

		/**
		 * The meta object literal for the '<em><b>Update Name Of Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___UPDATE_NAME_OF_BOOKING__INT_STRING_STRING = eINSTANCE.getIBookingProvider__UpdateNameOfBooking__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Update Date Of Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___UPDATE_DATE_OF_BOOKING__INT_STRING_STRING = eINSTANCE.getIBookingProvider__UpdateDateOfBooking__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Is Valid</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___IS_VALID__INT = eINSTANCE.getIBookingProvider__IsValid__int();

		/**
		 * The meta object literal for the '<em><b>Pay During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIBookingProvider__PayDuringCheckout__String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Is Blockable</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___IS_BLOCKABLE__INT = eINSTANCE.getIBookingProvider__IsBlockable__int();

		/**
		 * The meta object literal for the '<em><b>Reset</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___RESET = eINSTANCE.getIBookingProvider__Reset();

		/**
		 * The meta object literal for the '<em><b>Pay Room During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIBookingProvider__PayRoomDuringCheckout__int_String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Get Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___GET_BOOKING__STRING_STRING_STRING = eINSTANCE.getIBookingProvider__GetBooking__String_String_String();

		/**
		 * The meta object literal for the '<em><b>List Check Outs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___LIST_CHECK_OUTS__STRING = eINSTANCE.getIBookingProvider__ListCheckOuts__String();

		/**
		 * The meta object literal for the '<em><b>List Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___LIST_BOOKINGS = eINSTANCE.getIBookingProvider__ListBookings();

		/**
		 * The meta object literal for the '<em><b>Add Extra Cost To Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_PROVIDER___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_INT = eINSTANCE.getIBookingProvider__AddExtraCostToRoom__int_int_String_int();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.TwoIntTupleImpl <em>Two Int Tuple</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.TwoIntTupleImpl
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getTwoIntTuple()
		 * @generated
		 */
		EClass TWO_INT_TUPLE = eINSTANCE.getTwoIntTuple();

		/**
		 * The meta object literal for the '<em><b>I1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TWO_INT_TUPLE__I1 = eINSTANCE.getTwoIntTuple_I1();

		/**
		 * The meta object literal for the '<em><b>I2</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TWO_INT_TUPLE__I2 = eINSTANCE.getTwoIntTuple_I2();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.StringIntTupleImpl <em>String Int Tuple</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.StringIntTupleImpl
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getStringIntTuple()
		 * @generated
		 */
		EClass STRING_INT_TUPLE = eINSTANCE.getStringIntTuple();

		/**
		 * The meta object literal for the '<em><b>S</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_INT_TUPLE__S = eINSTANCE.getStringIntTuple_S();

		/**
		 * The meta object literal for the '<em><b>I</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_INT_TUPLE__I = eINSTANCE.getStringIntTuple_I();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingProviderImpl <em>Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingProviderImpl
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getBookingProvider()
		 * @generated
		 */
		EClass BOOKING_PROVIDER = eINSTANCE.getBookingProvider();

		/**
		 * The meta object literal for the '<em><b>Ibookingtable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_PROVIDER__IBOOKINGTABLE = eINSTANCE.getBookingProvider_Ibookingtable();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingImpl <em>Booking</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingImpl
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getBooking()
		 * @generated
		 */
		EClass BOOKING = eINSTANCE.getBooking();

		/**
		 * The meta object literal for the '<em><b>Number Of Rooms</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__NUMBER_OF_ROOMS = eINSTANCE.getBooking_NumberOfRooms();

		/**
		 * The meta object literal for the '<em><b>Assigned Rooms</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__ASSIGNED_ROOMS = eINSTANCE.getBooking_AssignedRooms();

		/**
		 * The meta object literal for the '<em><b>Booking ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__BOOKING_ID = eINSTANCE.getBooking_BookingID();

		/**
		 * The meta object literal for the '<em><b>Room Type Amount Tuples</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__ROOM_TYPE_AMOUNT_TUPLES = eINSTANCE.getBooking_RoomTypeAmountTuples();

		/**
		 * The meta object literal for the '<em><b>State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__STATE = eINSTANCE.getBooking_State();

		/**
		 * The meta object literal for the '<em><b>Start Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__START_DATE = eINSTANCE.getBooking_StartDate();

		/**
		 * The meta object literal for the '<em><b>End Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__END_DATE = eINSTANCE.getBooking_EndDate();

		/**
		 * The meta object literal for the '<em><b>First Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__FIRST_NAME = eINSTANCE.getBooking_FirstName();

		/**
		 * The meta object literal for the '<em><b>Last Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__LAST_NAME = eINSTANCE.getBooking_LastName();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable <em>IBooking Table</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getIBookingTable()
		 * @generated
		 */
		EClass IBOOKING_TABLE = eINSTANCE.getIBookingTable();

		/**
		 * The meta object literal for the '<em><b>List Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___LIST_CHECK_INS__STRING = eINSTANCE.getIBookingTable__ListCheckIns__String();

		/**
		 * The meta object literal for the '<em><b>Initiate Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___INITIATE_BOOKING__STRING_STRING_STRING_STRING = eINSTANCE.getIBookingTable__InitiateBooking__String_String_String_String();

		/**
		 * The meta object literal for the '<em><b>Confirm Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___CONFIRM_BOOKING__INT = eINSTANCE.getIBookingTable__ConfirmBooking__int();

		/**
		 * The meta object literal for the '<em><b>Add Room To Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___ADD_ROOM_TO_BOOKING__STRING_INT = eINSTANCE.getIBookingTable__AddRoomToBooking__String_int();

		/**
		 * The meta object literal for the '<em><b>Remove Room From Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___REMOVE_ROOM_FROM_BOOKING__STRING_INT = eINSTANCE.getIBookingTable__RemoveRoomFromBooking__String_int();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___CANCEL_BOOKING__INT = eINSTANCE.getIBookingTable__CancelBooking__int();

		/**
		 * The meta object literal for the '<em><b>Check In Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___CHECK_IN_BOOKING__INT = eINSTANCE.getIBookingTable__CheckInBooking__int();

		/**
		 * The meta object literal for the '<em><b>Check Out Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___CHECK_OUT_BOOKING__INT = eINSTANCE.getIBookingTable__CheckOutBooking__int();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___CHECK_IN_ROOM__INT_STRING = eINSTANCE.getIBookingTable__CheckInRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Check Out Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___CHECK_OUT_ROOM__INT_INT = eINSTANCE.getIBookingTable__CheckOutRoom__int_int();

		/**
		 * The meta object literal for the '<em><b>Get Tab Of Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___GET_TAB_OF_ROOM__INT_INT = eINSTANCE.getIBookingTable__GetTabOfRoom__int_int();

		/**
		 * The meta object literal for the '<em><b>Get Tab Of Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___GET_TAB_OF_BOOKING__INT = eINSTANCE.getIBookingTable__GetTabOfBooking__int();

		/**
		 * The meta object literal for the '<em><b>Get Occupied Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___GET_OCCUPIED_ROOMS__STRING = eINSTANCE.getIBookingTable__GetOccupiedRooms__String();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___GET_FREE_ROOMS__STRING_STRING_INT = eINSTANCE.getIBookingTable__GetFreeRooms__String_String_int();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms For Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___GET_FREE_ROOMS_FOR_ROOM_TYPE__STRING_STRING_STRING = eINSTANCE.getIBookingTable__GetFreeRoomsForRoomType__String_String_String();

		/**
		 * The meta object literal for the '<em><b>List Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___LIST_CHECK_INS__STRING_STRING = eINSTANCE.getIBookingTable__ListCheckIns__String_String();

		/**
		 * The meta object literal for the '<em><b>List Check Outs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___LIST_CHECK_OUTS__STRING_STRING = eINSTANCE.getIBookingTable__ListCheckOuts__String_String();

		/**
		 * The meta object literal for the '<em><b>Update Name Of Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___UPDATE_NAME_OF_BOOKING__INT_STRING_STRING = eINSTANCE.getIBookingTable__UpdateNameOfBooking__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Update Date Of Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___UPDATE_DATE_OF_BOOKING__INT_STRING_STRING = eINSTANCE.getIBookingTable__UpdateDateOfBooking__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Is Valid</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___IS_VALID__INT = eINSTANCE.getIBookingTable__IsValid__int();

		/**
		 * The meta object literal for the '<em><b>Pay During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIBookingTable__PayDuringCheckout__String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Is Blockable</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___IS_BLOCKABLE__INT = eINSTANCE.getIBookingTable__IsBlockable__int();

		/**
		 * The meta object literal for the '<em><b>Reset</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___RESET = eINSTANCE.getIBookingTable__Reset();

		/**
		 * The meta object literal for the '<em><b>Pay Room During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIBookingTable__PayRoomDuringCheckout__int_String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Get Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___GET_BOOKING__STRING_STRING_STRING = eINSTANCE.getIBookingTable__GetBooking__String_String_String();

		/**
		 * The meta object literal for the '<em><b>List Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___LIST_BOOKINGS = eINSTANCE.getIBookingTable__ListBookings();

		/**
		 * The meta object literal for the '<em><b>List Check Outs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___LIST_CHECK_OUTS__STRING = eINSTANCE.getIBookingTable__ListCheckOuts__String();

		/**
		 * The meta object literal for the '<em><b>Add Extra Cost To Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_TABLE___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_INT = eINSTANCE.getIBookingTable__AddExtraCostToRoom__int_int_String_int();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingTableImpl <em>Table</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingTableImpl
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getBookingTable()
		 * @generated
		 */
		EClass BOOKING_TABLE = eINSTANCE.getBookingTable();

		/**
		 * The meta object literal for the '<em><b>Bookings</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_TABLE__BOOKINGS = eINSTANCE.getBookingTable_Bookings();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.IntDTOTupleImpl <em>Int DTO Tuple</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.IntDTOTupleImpl
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getIntDTOTuple()
		 * @generated
		 */
		EClass INT_DTO_TUPLE = eINSTANCE.getIntDTOTuple();

		/**
		 * The meta object literal for the '<em><b>I1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INT_DTO_TUPLE__I1 = eINSTANCE.getIntDTOTuple_I1();

		/**
		 * The meta object literal for the '<em><b>Free Room Types DTO</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INT_DTO_TUPLE__FREE_ROOM_TYPES_DTO = eINSTANCE.getIntDTOTuple_FreeRoomTypesDTO();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.State <em>State</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.State
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl#getState()
		 * @generated
		 */
		EEnum STATE = eINSTANCE.getState();

	}

} //BookingPackage
