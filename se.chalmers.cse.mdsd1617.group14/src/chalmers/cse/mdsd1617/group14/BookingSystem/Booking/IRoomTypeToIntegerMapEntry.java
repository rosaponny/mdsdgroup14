/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Booking;

import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IRoom Type To Integer Map Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IRoomTypeToIntegerMapEntry#getKey <em>Key</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IRoomTypeToIntegerMapEntry#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage#getIRoomTypeToIntegerMapEntry()
 * @model
 * @generated
 */
public interface IRoomTypeToIntegerMapEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' reference.
	 * @see #setKey(IRoomType)
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage#getIRoomTypeToIntegerMapEntry_Key()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomType getKey();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IRoomTypeToIntegerMapEntry#getKey <em>Key</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' reference.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(IRoomType value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(Integer)
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage#getIRoomTypeToIntegerMapEntry_Value()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Integer getValue();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IRoomTypeToIntegerMapEntry#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Integer value);

} // IRoomTypeToIntegerMapEntry
