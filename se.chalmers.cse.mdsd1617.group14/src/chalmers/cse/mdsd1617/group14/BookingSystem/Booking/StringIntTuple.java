/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Booking;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Int Tuple</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.StringIntTuple#getS <em>S</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.StringIntTuple#getI <em>I</em>}</li>
 * </ul>
 *
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage#getStringIntTuple()
 * @model
 * @generated
 */
public interface StringIntTuple extends EObject {
	/**
	 * Returns the value of the '<em><b>S</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>S</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>S</em>' attribute.
	 * @see #setS(String)
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage#getStringIntTuple_S()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getS();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.StringIntTuple#getS <em>S</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>S</em>' attribute.
	 * @see #getS()
	 * @generated
	 */
	void setS(String value);

	/**
	 * Returns the value of the '<em><b>I</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>I</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>I</em>' attribute.
	 * @see #setI(int)
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage#getStringIntTuple_I()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getI();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.StringIntTuple#getI <em>I</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>I</em>' attribute.
	 * @see #getI()
	 * @generated
	 */
	void setI(int value);

} // StringIntTuple
