/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Booking;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Two Int Tuple</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple#getI1 <em>I1</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple#getI2 <em>I2</em>}</li>
 * </ul>
 *
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage#getTwoIntTuple()
 * @model
 * @generated
 */
public interface TwoIntTuple extends EObject {

	/**
	 * Returns the value of the '<em><b>I1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>I1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>I1</em>' attribute.
	 * @see #setI1(int)
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage#getTwoIntTuple_I1()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getI1();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple#getI1 <em>I1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>I1</em>' attribute.
	 * @see #getI1()
	 * @generated
	 */
	void setI1(int value);

	/**
	 * Returns the value of the '<em><b>I2</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>I2</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>I2</em>' attribute.
	 * @see #setI2(int)
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage#getTwoIntTuple_I2()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getI2();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple#getI2 <em>I2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>I2</em>' attribute.
	 * @see #getI2()
	 * @generated
	 */
	void setI2(int value);
} // TwoIntTuple
