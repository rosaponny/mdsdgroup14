/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Booking.util;

import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage
 * @generated
 */
public class BookingAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static BookingPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = BookingPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BookingSwitch<Adapter> modelSwitch =
		new BookingSwitch<Adapter>() {
			@Override
			public Adapter caseIBooking(IBooking object) {
				return createIBookingAdapter();
			}
			@Override
			public Adapter caseStringIntTuple(StringIntTuple object) {
				return createStringIntTupleAdapter();
			}
			@Override
			public Adapter caseIBookingProvider(IBookingProvider object) {
				return createIBookingProviderAdapter();
			}
			@Override
			public Adapter caseTwoIntTuple(TwoIntTuple object) {
				return createTwoIntTupleAdapter();
			}
			@Override
			public Adapter caseBookingProvider(BookingProvider object) {
				return createBookingProviderAdapter();
			}
			@Override
			public Adapter caseIBookingTable(IBookingTable object) {
				return createIBookingTableAdapter();
			}
			@Override
			public Adapter caseBooking(Booking object) {
				return createBookingAdapter();
			}
			@Override
			public Adapter caseBookingTable(BookingTable object) {
				return createBookingTableAdapter();
			}
			@Override
			public Adapter caseIntDTOTuple(IntDTOTuple object) {
				return createIntDTOTupleAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking <em>IBooking</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking
	 * @generated
	 */
	public Adapter createIBookingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider <em>IBooking Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider
	 * @generated
	 */
	public Adapter createIBookingProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple <em>Two Int Tuple</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple
	 * @generated
	 */
	public Adapter createTwoIntTupleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.StringIntTuple <em>String Int Tuple</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.StringIntTuple
	 * @generated
	 */
	public Adapter createStringIntTupleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingProvider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingProvider
	 * @generated
	 */
	public Adapter createBookingProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking <em>Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking
	 * @generated
	 */
	public Adapter createBookingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable <em>IBooking Table</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable
	 * @generated
	 */
	public Adapter createIBookingTableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingTable
	 * @generated
	 */
	public Adapter createBookingTableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IntDTOTuple <em>Int DTO Tuple</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IntDTOTuple
	 * @generated
	 */
	public Adapter createIntDTOTupleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //BookingAdapterFactory
