/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.State;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.StringIntTuple;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.EconomicsFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.Tab;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomStatus;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomTable;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Booking</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingImpl#getNumberOfRooms <em>Number Of Rooms</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingImpl#getAssignedRooms <em>Assigned Rooms</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingImpl#getBookingID <em>Booking ID</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingImpl#getRoomTypeAmountTuples <em>Room Type Amount Tuples</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingImpl#getEndDate <em>End Date</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingImpl#getFirstName <em>First Name</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingImpl#getLastName <em>Last Name</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingImpl#getState <em>State</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingImpl#getStartDate <em>Start Date</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingImpl extends MinimalEObjectImpl.Container implements Booking {
	
	
	/**
	 * The default value of the '{@link #getNumberOfRooms() <em>Number Of Rooms</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfRooms()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_ROOMS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumberOfRooms() <em>Number Of Rooms</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberOfRooms()
	 * @generated
	 * @ordered
	 */
	protected int numberOfRooms = NUMBER_OF_ROOMS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAssignedRooms() <em>Assigned Rooms</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssignedRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> assignedRooms;

	/**
	 * The default value of the '{@link #getBookingID() <em>Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingID()
	 * @generated
	 * @ordered
	 */
	protected static final int BOOKING_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBookingID() <em>Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingID()
	 * @generated
	 * @ordered
	 */
	protected int bookingID = BOOKING_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRoomTypeAmountTuples() <em>Room Type Amount Tuples</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypeAmountTuples()
	 * @generated
	 * @ordered
	 */
	protected EList<StringIntTuple> roomTypeAmountTuples;

	/**
	 * The default value of the '{@link #getEndDate() <em>End Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndDate()
	 * @generated
	 * @ordered
	 */
	protected static final String END_DATE_EDEFAULT = null;
	String endDate; // end date of the booking 
	/**
	 * The default value of the '{@link #getFirstName() <em>First Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstName()
	 * @generated
	 * @ordered
	 */
	protected static final String FIRST_NAME_EDEFAULT = null;
	String firstName;  //First name of the person booking the room
	/**
	 * The default value of the '{@link #getLastName() <em>Last Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastName()
	 * @generated
	 * @ordered
	 */
	protected static final String LAST_NAME_EDEFAULT = null;
	String lastName;  // last name of the person booking the room 

	/**
	 * The default value of the '{@link #getState() <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected static final State STATE_EDEFAULT = State.UNCOMFIRMED;

	/**
	 * The cached value of the '{@link #getState() <em>State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getState()
	 * @generated
	 * @ordered
	 */
	protected State state = STATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getStartDate() <em>Start Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartDate()
	 * @generated
	 * @ordered
	 */
	protected static final String START_DATE_EDEFAULT = null;

	String startDate; // start date for the booking
	
	private final IRoomProvider iRoomProvider = RoomFactory.eINSTANCE.createRoomProvider();
	private final IRoomTypeProvider iRoomTypeProvider = RoomTypeFactory.eINSTANCE.createRoomTypeProvider();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected BookingImpl() {
		super();
		roomTypeAmountTuples = new BasicEList<StringIntTuple>();
		assignedRooms = new BasicEList<Integer>();
		state = State.UNCOMFIRMED;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.BOOKING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumberOfRooms() {
		return numberOfRooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumberOfRooms(int newNumberOfRooms) {
		int oldNumberOfRooms = numberOfRooms;
		numberOfRooms = newNumberOfRooms;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__NUMBER_OF_ROOMS, oldNumberOfRooms, numberOfRooms));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getAssignedRooms() {
		if (assignedRooms == null) {
			assignedRooms = new EDataTypeUniqueEList<Integer>(Integer.class, this, BookingPackage.BOOKING__ASSIGNED_ROOMS);
		}
		return assignedRooms;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> listAssignedRooms() {
		EList<Integer> toReturn = new BasicEList<Integer>();
		for(Integer r : assignedRooms){
			toReturn.add(r);
		}
		return toReturn;
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void unassignRoom(int roomNumber) {
		for(int i = 0; i<assignedRooms.size();i++){
			if(assignedRooms.get(i)==roomNumber){
				assignedRooms.remove(i);
				break;
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBookingID() {
		return bookingID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingID(int newBookingID) {
		int oldBookingID = bookingID;
		bookingID = newBookingID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__BOOKING_ID, oldBookingID, bookingID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StringIntTuple> getRoomTypeAmountTuples() {
		if (roomTypeAmountTuples == null) {
			roomTypeAmountTuples = new EObjectResolvingEList<StringIntTuple>(StringIntTuple.class, this, BookingPackage.BOOKING__ROOM_TYPE_AMOUNT_TUPLES);
		}
		return roomTypeAmountTuples;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public State getState() {
		return state;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setState(State newState) {
		State oldState = state;
		state = newState == null ? STATE_EDEFAULT : newState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__STATE, oldState, state));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartDate(String newStartDate) {
		String oldStartDate = startDate;
		startDate = newStartDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__START_DATE, oldStartDate, startDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndDate(String newEndDate) {
		String oldEndDate = endDate;
		endDate = newEndDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__END_DATE, oldEndDate, endDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstName(String newFirstName) {
		String oldFirstName = firstName;
		firstName = newFirstName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__FIRST_NAME, oldFirstName, firstName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastName(String newLastName) {
		String oldLastName = lastName;
		lastName = newLastName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__LAST_NAME, oldLastName, lastName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getID() {
		return this.bookingID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean assignRoom(int roomNumber) {
		RoomTable rt = (RoomTable)RoomFactory.eINSTANCE.createRoomTable();
		EList<IRoom> list = rt.getRooms();
		IRoom r = null;
		for(IRoom room:list){
			if(room.getRoomNbr()==roomNumber){
				r=room;
			}
		}
		if(r==null){
			return false;
		}
		
		if(r.getStatus()!=RoomStatus.FREE){
			return false;
		}
		((Room)r).setStatus(RoomStatus.OCCUPIED);
		return assignedRooms.add(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ITab getTabOfBooking() {
		ITab tab = EconomicsFactory.eINSTANCE.createTab();
		for(Integer room : assignedRooms){
			ITab roomTab = iRoomProvider.getTab(room); 
			EList<IBill> bills = ((Tab)roomTab).getBill();
			for(IBill bill : bills){
				tab.addBill(bill);
			}
		}
		return tab;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String lastName, String startDate, String endDate) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.startDate = startDate;
		this.endDate = endDate;
		return this.bookingID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirmBooking() {
		if(!this.assignedRooms.isEmpty()) { // should have some assigned rooms
			this.state = State.CONFIRMED;
			return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription) {
		
		boolean added = false;
		for(StringIntTuple tuple: roomTypeAmountTuples){
			
			if(tuple.getS().equals(roomTypeDescription)){
				tuple.setI(tuple.getI()+1);
				added = true;
				break;
			}
			
		}
		
		if(!added){
			StringIntTuple s = BookingFactory.eINSTANCE.createStringIntTuple();
			s.setS(roomTypeDescription);
			s.setI(1);
			roomTypeAmountTuples.add(s);
		}
		numberOfRooms++;
		return true;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoomFromBooking(String roomTypeDescription) {
		boolean removed = false; 
		int numRoomsBeforeRemove;
		for(StringIntTuple tuple : roomTypeAmountTuples) {
			if(tuple.getS().equals(roomTypeDescription)) {				
				numRoomsBeforeRemove = tuple.getI();				
				tuple.setI(tuple.getI()-1);				
				if(tuple.getI() == (numRoomsBeforeRemove-1)){
					removed = true;			
				}
				if(numRoomsBeforeRemove == 1) {					
					roomTypeAmountTuples.remove(tuple);
				}
				break;
				
			}
		}
		return removed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean cancelBooking() {
		if(state == State.CANCELLED) {
			return false; //should not be able to cancel twice
		}
		if(this.state != State.CONFIRMED) {
			return false; //Should not be able to check out in a booking that's not checked in
		}
		this.state = State.CANCELLED;
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkInBooking() {
		if(this.state == State.CHECKED_IN) {
			return false; //Should not be able to check in twice
		}
		if(this.state != State.CONFIRMED) {
			return false; //Should not be able to checkin in an unconfirmed booking
		}
		
			String rtd;
			int toAssign;
			for (StringIntTuple sit : roomTypeAmountTuples){
				rtd = sit.getS();
				toAssign = sit.getI();
				for (Integer room : assignedRooms){
					if (iRoomTypeProvider.getTypeOfRoom(room).equals(rtd)) toAssign--;
				}
				assignedRooms.addAll(iRoomTypeProvider.getFreeRoomsOfType(rtd, toAssign));
			}
			
			for(Integer room : assignedRooms) {
				iRoomProvider.occupyRoom(room);
			}
			this.state = State.CHECKED_IN;
			return true;
				
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkOutBooking() {
		
		if(this.state == State.CHECKED_OUT) {
			return false; //Should not be able to check out twice
		}
		
		if(this.state != State.CHECKED_IN) {
			return false; //Should not be able to check out in a booking that's not checked in
		}
		
		for(Integer room : assignedRooms) {
			iRoomProvider.freeRoom(room);
		}
		this.state =State.CHECKED_OUT;
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(String roomTypeDescription) {
		IRoomTypeProvider rtp = RoomTypeFactory.eINSTANCE.createRoomTypeProvider();
		if(this.state != State.CONFIRMED) {
			return -1; //Should not be able to checkin in an unconfirmed booking
		}
		boolean hasBookedThisRoomType = false;
		for(StringIntTuple sit : roomTypeAmountTuples){ //checks that this type of room was added to the booking
			if(sit.getS().equals(roomTypeDescription) && sit.getI()>0){
				int allreadyCheckedin = 0;
				for(int i : assignedRooms){
					if (iRoomTypeProvider.getTypeOfRoom(i).equals(sit.getS())) allreadyCheckedin++;
				}
				if (hasBookedThisRoomType = (allreadyCheckedin < sit.getI())) break;
			}
		}
		if(!hasBookedThisRoomType){return -1;}

		
		EList<Integer> rooms = rtp.getFreeRoomsOfType(roomTypeDescription, 1);
		if(rooms.isEmpty()){
			return -1;
		}
		int roomNumber = rooms.get(0); //takes the first room in the list
		if(!assignRoom(roomNumber)){
			return -1;
		}
		
		

		int checkedInRooms = 0;
		for(Integer room : assignedRooms){
			if(iRoomProvider.getStatus(room) == RoomStatus.OCCUPIED){
				checkedInRooms++;
			}
		}
		
		int totalRoomsOfBooking = 0;
		for(StringIntTuple sit : roomTypeAmountTuples){
			totalRoomsOfBooking += sit.getI();
		}
		
		int numRoomsLeftToCheckIn = checkedInRooms-totalRoomsOfBooking;
		if(numRoomsLeftToCheckIn==0){
			setState(State.CHECKED_IN);
		}
		
		
		//TODO: If last room was checked in, set booking state to CHECKED_IN
		
		return roomNumber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkOutRoom(int roomNumber) {
		if(this.state != State.CHECKED_IN) {
			return false; //Should not be able to check out in a booking that's not checked in
		}
		Integer r = null;
		for(Integer room : assignedRooms){
			if(room==roomNumber){
				r=room;
				break;
			}
		}
		if(r==null){
			return false;
		}
		
		
		if(assignedRooms.size()==1){
			return checkOutBooking();
		}
		
		iRoomProvider.freeRoom(roomNumber);
		
		int roomsOccupied = 0;
		for(Integer room : assignedRooms){
			if(iRoomProvider.getStatus(room) == RoomStatus.OCCUPIED){
				roomsOccupied++;
			}
		}
		if(roomsOccupied==0){ //if last room was checked out, then checkout booking
			setState(State.CHECKED_OUT); 
		}
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ITab getTabOfRoom(int roomNumber) {
		return RoomFactory.eINSTANCE.createRoomProvider().getTab(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.BOOKING__NUMBER_OF_ROOMS:
				return getNumberOfRooms();
			case BookingPackage.BOOKING__ASSIGNED_ROOMS:
				return getAssignedRooms();
			case BookingPackage.BOOKING__BOOKING_ID:
				return getBookingID();
			case BookingPackage.BOOKING__ROOM_TYPE_AMOUNT_TUPLES:
				return getRoomTypeAmountTuples();
			case BookingPackage.BOOKING__END_DATE:
				return getEndDate();
			case BookingPackage.BOOKING__FIRST_NAME:
				return getFirstName();
			case BookingPackage.BOOKING__LAST_NAME:
				return getLastName();
			case BookingPackage.BOOKING__STATE:
				return getState();
			case BookingPackage.BOOKING__START_DATE:
				return getStartDate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.BOOKING__NUMBER_OF_ROOMS:
				setNumberOfRooms((Integer)newValue);
				return;
			case BookingPackage.BOOKING__ASSIGNED_ROOMS:
				getAssignedRooms().clear();
				getAssignedRooms().addAll((Collection<? extends Integer>)newValue);
				return;
			case BookingPackage.BOOKING__BOOKING_ID:
				setBookingID((Integer)newValue);
				return;
			case BookingPackage.BOOKING__ROOM_TYPE_AMOUNT_TUPLES:
				getRoomTypeAmountTuples().clear();
				getRoomTypeAmountTuples().addAll((Collection<? extends StringIntTuple>)newValue);
				return;
			case BookingPackage.BOOKING__END_DATE:
				setEndDate((String)newValue);
				return;
			case BookingPackage.BOOKING__FIRST_NAME:
				setFirstName((String)newValue);
				return;
			case BookingPackage.BOOKING__LAST_NAME:
				setLastName((String)newValue);
				return;
			case BookingPackage.BOOKING__STATE:
				setState((State)newValue);
				return;
			case BookingPackage.BOOKING__START_DATE:
				setStartDate((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING__NUMBER_OF_ROOMS:
				setNumberOfRooms(NUMBER_OF_ROOMS_EDEFAULT);
				return;
			case BookingPackage.BOOKING__ASSIGNED_ROOMS:
				getAssignedRooms().clear();
				return;
			case BookingPackage.BOOKING__BOOKING_ID:
				setBookingID(BOOKING_ID_EDEFAULT);
				return;
			case BookingPackage.BOOKING__ROOM_TYPE_AMOUNT_TUPLES:
				getRoomTypeAmountTuples().clear();
				return;
			case BookingPackage.BOOKING__END_DATE:
				setEndDate(END_DATE_EDEFAULT);
				return;
			case BookingPackage.BOOKING__FIRST_NAME:
				setFirstName(FIRST_NAME_EDEFAULT);
				return;
			case BookingPackage.BOOKING__LAST_NAME:
				setLastName(LAST_NAME_EDEFAULT);
				return;
			case BookingPackage.BOOKING__STATE:
				setState(STATE_EDEFAULT);
				return;
			case BookingPackage.BOOKING__START_DATE:
				setStartDate(START_DATE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING__NUMBER_OF_ROOMS:
				return numberOfRooms != NUMBER_OF_ROOMS_EDEFAULT;
			case BookingPackage.BOOKING__ASSIGNED_ROOMS:
				return assignedRooms != null && !assignedRooms.isEmpty();
			case BookingPackage.BOOKING__BOOKING_ID:
				return bookingID != BOOKING_ID_EDEFAULT;
			case BookingPackage.BOOKING__ROOM_TYPE_AMOUNT_TUPLES:
				return roomTypeAmountTuples != null && !roomTypeAmountTuples.isEmpty();
			case BookingPackage.BOOKING__END_DATE:
				return END_DATE_EDEFAULT == null ? endDate != null : !END_DATE_EDEFAULT.equals(endDate);
			case BookingPackage.BOOKING__FIRST_NAME:
				return FIRST_NAME_EDEFAULT == null ? firstName != null : !FIRST_NAME_EDEFAULT.equals(firstName);
			case BookingPackage.BOOKING__LAST_NAME:
				return LAST_NAME_EDEFAULT == null ? lastName != null : !LAST_NAME_EDEFAULT.equals(lastName);
			case BookingPackage.BOOKING__STATE:
				return state != STATE_EDEFAULT;
			case BookingPackage.BOOKING__START_DATE:
				return START_DATE_EDEFAULT == null ? startDate != null : !START_DATE_EDEFAULT.equals(startDate);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackage.BOOKING___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case BookingPackage.BOOKING___CONFIRM_BOOKING:
				return confirmBooking();
			case BookingPackage.BOOKING___ADD_ROOM_TO_BOOKING__STRING:
				return addRoomToBooking((String)arguments.get(0));
			case BookingPackage.BOOKING___REMOVE_ROOM_FROM_BOOKING__STRING:
				return removeRoomFromBooking((String)arguments.get(0));
			case BookingPackage.BOOKING___CANCEL_BOOKING:
				return cancelBooking();
			case BookingPackage.BOOKING___CHECK_IN_BOOKING:
				return checkInBooking();
			case BookingPackage.BOOKING___CHECK_OUT_BOOKING:
				return checkOutBooking();
			case BookingPackage.BOOKING___CHECK_IN_ROOM__STRING:
				return checkInRoom((String)arguments.get(0));
			case BookingPackage.BOOKING___CHECK_OUT_ROOM__INT:
				return checkOutRoom((Integer)arguments.get(0));
			case BookingPackage.BOOKING___GET_TAB_OF_ROOM__INT:
				return getTabOfRoom((Integer)arguments.get(0));
			case BookingPackage.BOOKING___GET_ID:
				return getID();
			case BookingPackage.BOOKING___ASSIGN_ROOM__INT:
				return assignRoom((Integer)arguments.get(0));
			case BookingPackage.BOOKING___GET_TAB_OF_BOOKING:
				return getTabOfBooking();
			case BookingPackage.BOOKING___LIST_ASSIGNED_ROOMS:
				return listAssignedRooms();
			case BookingPackage.BOOKING___UNASSIGN_ROOM__INT:
				unassignRoom((Integer)arguments.get(0));
				return null;
			case BookingPackage.BOOKING___TO_STRING:
				return toString();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Booking Info: \nId: " + bookingID + " \tStart Date: " + startDate + " \tEnd Date: " + endDate + "\nRooms:");
		for (StringIntTuple sit : roomTypeAmountTuples) sb.append("\n\t" + sit.getI() + "x\t" + sit.getS());
		
		return sb.toString();
	}

	@Override
	public int getId() {
		return bookingID;
	}

} //BookingImpl
