/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Two Int Tuple</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.TwoIntTupleImpl#getI1 <em>I1</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.TwoIntTupleImpl#getI2 <em>I2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TwoIntTupleImpl extends MinimalEObjectImpl.Container implements TwoIntTuple {
	/**
	 * The default value of the '{@link #getI1() <em>I1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getI1()
	 * @generated
	 * @ordered
	 */
	protected static final int I1_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getI1() <em>I1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getI1()
	 * @generated
	 * @ordered
	 */
	protected int i1 = I1_EDEFAULT;
	/**
	 * The default value of the '{@link #getI2() <em>I2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getI2()
	 * @generated
	 * @ordered
	 */
	protected static final int I2_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getI2() <em>I2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getI2()
	 * @generated
	 * @ordered
	 */
	protected int i2 = I2_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TwoIntTupleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.TWO_INT_TUPLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getI1() {
		return i1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setI1(int newI1) {
		int oldI1 = i1;
		i1 = newI1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.TWO_INT_TUPLE__I1, oldI1, i1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getI2() {
		return i2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setI2(int newI2) {
		int oldI2 = i2;
		i2 = newI2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.TWO_INT_TUPLE__I2, oldI2, i2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.TWO_INT_TUPLE__I1:
				return getI1();
			case BookingPackage.TWO_INT_TUPLE__I2:
				return getI2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.TWO_INT_TUPLE__I1:
				setI1((Integer)newValue);
				return;
			case BookingPackage.TWO_INT_TUPLE__I2:
				setI2((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.TWO_INT_TUPLE__I1:
				setI1(I1_EDEFAULT);
				return;
			case BookingPackage.TWO_INT_TUPLE__I2:
				setI2(I2_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.TWO_INT_TUPLE__I1:
				return i1 != I1_EDEFAULT;
			case BookingPackage.TWO_INT_TUPLE__I2:
				return i2 != I2_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (i1: ");
		result.append(i1);
		result.append(", i2: ");
		result.append(i2);
		result.append(')');
		return result.toString();
	}

} //TwoIntTupleImpl
