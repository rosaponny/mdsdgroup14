/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Booking;

import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Int DTO Tuple</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IntDTOTuple#getI1 <em>I1</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IntDTOTuple#getFreeRoomTypesDTO <em>Free Room Types DTO</em>}</li>
 * </ul>
 *
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage#getIntDTOTuple()
 * @model
 * @generated
 */
public interface IntDTOTuple extends EObject {
	/**
	 * Returns the value of the '<em><b>I1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>I1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>I1</em>' attribute.
	 * @see #setI1(int)
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage#getIntDTOTuple_I1()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getI1();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IntDTOTuple#getI1 <em>I1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>I1</em>' attribute.
	 * @see #getI1()
	 * @generated
	 */
	void setI1(int value);

	/**
	 * Returns the value of the '<em><b>Free Room Types DTO</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Free Room Types DTO</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Free Room Types DTO</em>' containment reference.
	 * @see #setFreeRoomTypesDTO(FreeRoomTypesDTO)
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage#getIntDTOTuple_FreeRoomTypesDTO()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	FreeRoomTypesDTO getFreeRoomTypesDTO();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IntDTOTuple#getFreeRoomTypesDTO <em>Free Room Types DTO</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Free Room Types DTO</em>' containment reference.
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	void setFreeRoomTypesDTO(FreeRoomTypesDTO value);

} // IntDTOTuple
