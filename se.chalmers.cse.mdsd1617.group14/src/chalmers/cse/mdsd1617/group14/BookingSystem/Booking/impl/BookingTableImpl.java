/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingTable;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.State;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.StringIntTuple;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.Bill;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.EconomicsFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomStatus;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeProvider;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import javax.xml.soap.SOAPException;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Table</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingTableImpl#getBookings <em>Bookings</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingTableImpl extends MinimalEObjectImpl.Container implements BookingTable {
	
	int keyCount;

	RoomTypeProvider roomtypeprovider;
	RoomProvider roomprovider;
	
	/**
	 * The cached value of the '{@link #getBookings() <em>Bookings</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookings()
	 * @generated
	 * @ordered
	 */
	protected EList<IBooking> bookings;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected BookingTableImpl() {
		super();
		bookings = new BasicEList<IBooking>();
		roomprovider     = chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomFactory.eINSTANCE.createRoomProvider();
		roomtypeprovider = chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeFactory.eINSTANCE.createRoomTypeProvider();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.BOOKING_TABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IBooking> getBookings() {
		if (bookings == null) {
			bookings = new EObjectResolvingEList<IBooking>(IBooking.class, this, BookingPackage.BOOKING_TABLE__BOOKINGS);
		}
		return bookings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> listCheckIns(String day) {
		EList<Integer> checkIns = new BasicEList<Integer>();
		for(IBooking booking : bookings){
			if(booking.getState()==State.CHECKED_IN && booking.getStartDate().compareTo(day) <= 0 && booking.getEndDate().compareTo(day) >= 0){
				checkIns.add(booking.getBookingID());
			}
		}
		return checkIns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<String> listBookings() {
		EList<String> bookingToString = new BasicEList<String>(); 
		for (IBooking booking: bookings ) {
			bookingToString.add(booking.toString());
		}
		return bookingToString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> listCheckOuts(String day) {
		EList<Integer> checkOuts = new BasicEList<Integer>();
		for(IBooking booking : bookings){
			if(booking.getState()==State.CHECKED_OUT && booking.getEndDate().compareTo(day) >= 0 && booking.getEndDate().compareTo(day) <= 0){
				checkOuts.add(booking.getBookingID());
			}
		}
		return checkOuts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addExtraCostToRoom(int bookingID, int roomNumber, String costDescription, int price) {
		IBooking b = getBooking(bookingID);
		if(b==null){
			return false;
		}
		if (!roomprovider.isValid(roomNumber)) return false;
		Bill bill = EconomicsFactory.eINSTANCE.createBill();
		bill.setCost(price);
		bill.setDescription(costDescription);
		roomprovider.addBill(roomNumber, bill);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String lastName, String startDate, String endDate) {
		
		if(firstName.isEmpty() || lastName.isEmpty() || startDate.compareTo(endDate)>0){
			return -1;
		}
		
		Booking b = BookingFactoryImpl.init().createBooking();
		b.initiateBooking(firstName, lastName, startDate, endDate);
		bookings.add(b);
		b.setBookingID(keyCount++);
		b.setState(State.UNCOMFIRMED);
		return b.getBookingID();

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		IBooking b = getBooking(bookingID); 
		if(b==null){
			return false;
		}

		if (b.getState() != State.UNCOMFIRMED || b.getRoomTypeAmountTuples().isEmpty()){
			return false;
		}
		
		b.setState(State.CONFIRMED);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.BOOKING_TABLE__BOOKINGS:
				return getBookings();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.BOOKING_TABLE__BOOKINGS:
				getBookings().clear();
				getBookings().addAll((Collection<? extends IBooking>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING_TABLE__BOOKINGS:
				getBookings().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING_TABLE__BOOKINGS:
				return bookings != null && !bookings.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackage.BOOKING_TABLE___LIST_CHECK_INS__STRING:
				return listCheckIns((String)arguments.get(0));
			case BookingPackage.BOOKING_TABLE___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case BookingPackage.BOOKING_TABLE___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKING_TABLE___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKING_TABLE___REMOVE_ROOM_FROM_BOOKING__STRING_INT:
				return removeRoomFromBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKING_TABLE___CANCEL_BOOKING__INT:
				return cancelBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKING_TABLE___CHECK_IN_BOOKING__INT:
				return checkInBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKING_TABLE___CHECK_OUT_BOOKING__INT:
				return checkOutBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKING_TABLE___CHECK_IN_ROOM__INT_STRING:
				return checkInRoom((Integer)arguments.get(0), (String)arguments.get(1));
			case BookingPackage.BOOKING_TABLE___CHECK_OUT_ROOM__INT_INT:
				return checkOutRoom((Integer)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKING_TABLE___GET_TAB_OF_ROOM__INT_INT:
				return getTabOfRoom((Integer)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKING_TABLE___GET_TAB_OF_BOOKING__INT:
				return getTabOfBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKING_TABLE___GET_OCCUPIED_ROOMS__STRING:
				return getOccupiedRooms((String)arguments.get(0));
			case BookingPackage.BOOKING_TABLE___GET_FREE_ROOMS__STRING_STRING_INT:
				return getFreeRooms((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2));
			case BookingPackage.BOOKING_TABLE___GET_FREE_ROOMS_FOR_ROOM_TYPE__STRING_STRING_STRING:
				return getFreeRoomsForRoomType((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case BookingPackage.BOOKING_TABLE___LIST_CHECK_INS__STRING_STRING:
				return listCheckIns((String)arguments.get(0), (String)arguments.get(1));
			case BookingPackage.BOOKING_TABLE___LIST_CHECK_OUTS__STRING_STRING:
				return listCheckOuts((String)arguments.get(0), (String)arguments.get(1));
			case BookingPackage.BOOKING_TABLE___UPDATE_NAME_OF_BOOKING__INT_STRING_STRING:
				return updateNameOfBooking((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case BookingPackage.BOOKING_TABLE___UPDATE_DATE_OF_BOOKING__INT_STRING_STRING:
				return updateDateOfBooking((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case BookingPackage.BOOKING_TABLE___IS_VALID__INT:
				return isValid((Integer)arguments.get(0));
			case BookingPackage.BOOKING_TABLE___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case BookingPackage.BOOKING_TABLE___IS_BLOCKABLE__INT:
				return isBlockable((Integer)arguments.get(0));
			case BookingPackage.BOOKING_TABLE___RESET:
				return reset();
			case BookingPackage.BOOKING_TABLE___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case BookingPackage.BOOKING_TABLE___GET_BOOKING__STRING_STRING_STRING:
				return getBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case BookingPackage.BOOKING_TABLE___LIST_BOOKINGS:
				return listBookings();
			case BookingPackage.BOOKING_TABLE___LIST_CHECK_OUTS__STRING:
				return listCheckOuts((String)arguments.get(0));
			case BookingPackage.BOOKING_TABLE___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_INT:
				return addExtraCostToRoom((Integer)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3));
		}
		return super.eInvoke(operationID, arguments);
	}
	
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		IBooking b = getBooking(bookingID);
		if(b==null){
			return false;
		}
		if (!checkForAvaiableRooms(b, roomTypeDescription)){
			return false;
		}
		
		b.addRoomToBooking(roomTypeDescription);
		
		
		return true;
		
	
	}
	
	private boolean checkForAvaiableRooms(IBooking iBooking, String roomTypeDescription) {
		int nbrOfBookedRooms = 0;
		Booking booking = (Booking) iBooking;
		for (IBooking ib : bookings){
			Booking b = (Booking) ib;
			if(b.getStartDate().compareTo(booking.getEndDate()) <= 0 && b.getEndDate().compareTo(booking.getStartDate()) >= 0){
				for(StringIntTuple sit : b.getRoomTypeAmountTuples()){
					if ( sit.getS().equals(roomTypeDescription)){
						nbrOfBookedRooms += sit.getI();
					}
				}
			}
		}
		return nbrOfBookedRooms < roomtypeprovider.getMaximumNumberOfRooms(roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoomFromBooking(String roomTypeDescription, int bookingID) {
		for (IBooking booking : bookings)  {
			if(booking.getBookingID() == bookingID) {
				return booking.removeRoomFromBooking(roomTypeDescription);
			}
		}
		return false; 
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean cancelBooking(int bookingID) {
		for(IBooking booking : bookings ) {
			if(booking.getId() == bookingID) {
				return booking.cancelBooking();
			}
		}
		return false; 
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkInBooking(int bookingID) {
		IBooking booking = getBooking(bookingID);
		if(booking.getState()!=State.CONFIRMED && booking.getBookingID()==bookingID)return false;
		return getBooking(bookingID).checkInBooking();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkOutBooking(int bookingID) {
		IBooking booking = getBooking(bookingID);
		if (booking == null) return false;
		
		for (IBooking b : bookings){
			if (b.getState() == State.CHECKED_OUT && b.getBookingID() != bookingID) return false;
		}

		if(booking.getState()!=State.CHECKED_IN && booking.getBookingID()==bookingID)return false;
		
		int days = convertIntoDays(booking.getStartDate(), booking.getEndDate());
		for (Integer roomNumber : booking.listAssignedRooms()){
			if (roomprovider.getStatus(roomNumber) == RoomStatus.OCCUPIED){
				String roomType = roomtypeprovider.getTypeOfRoom(roomNumber);
				Bill b = EconomicsFactory.eINSTANCE.createBill();
				b.setDescription("" + days + " days. Room type: " + roomType + ". Room number: " + roomNumber);
				b.setCost(roomtypeprovider.getPrice(roomType)*days);
				roomprovider.addBill(roomNumber, b);
			}
		}
		return booking.checkOutBooking(); 
	}
	
	private int convertIntoDays(String start, String end){
		int day1   = convertSubStrings(start, 6, 8);
		int day2   = convertSubStrings(end, 6, 8);
		int month1 = convertSubStrings(start, 4, 6);
		int month2 = convertSubStrings(end, 4, 6);
		int year1  = convertSubStrings(start, 0, 4);
		int year2  = convertSubStrings(end, 0, 4);
		
		return convertYearToDays(year2) + convertMonthToDays(year2, month2) + day2
			  -convertYearToDays(year1) - convertMonthToDays(year1, month1) - day1;
	}
	
	private int convertSubStrings(String str, int start, int end){
		return Integer.parseInt(str.substring(start, end));
	}
	
	private int convertYearToDays(int year){
		return year*365 + (year / 4) - (year / 100) + (year / 400); 
	}
	
	private int convertMonthToDays(int year, int month){
		switch (month) {
		case 2:
			return isLeapYear(year) ? 28 : 29;
		case 4:
		case 6:
		case 9:
		case 11:
			return 30;
		default:
			return 31;
		}
	}
	
	private boolean isLeapYear(int year){
		 return ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(int bookingID, String roomTypeDescription) {
		IBooking booking = getBooking(bookingID);
		if(booking.getState()!=State.CONFIRMED && booking.getBookingID()==bookingID)return -1;
		
		return getBooking(bookingID).checkInRoom(roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkOutRoom(int bookingID, int roomNumber) {
		IBooking booking = getBooking(bookingID);
		if(booking.getState()!=State.CHECKED_IN && booking.getBookingID()==bookingID)return false;
		int days = convertIntoDays(booking.getStartDate(), booking.getEndDate());
		String roomType = roomtypeprovider.getTypeOfRoom(roomNumber);
		Bill b = EconomicsFactory.eINSTANCE.createBill();
		b.setDescription("Nights");
		b.setCost(roomtypeprovider.getPrice(roomType)*days);
		roomprovider.addBill(roomNumber, b);
		return getBooking(bookingID).checkOutRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ITab getTabOfRoom(int bookingID, int roomNumber) {
		return getBooking(bookingID).getTabOfRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ITab getTabOfBooking(int bookingID) {
		return getBooking(bookingID).getTabOfBooking();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<TwoIntTuple> getOccupiedRooms(String date) {
		
		EList<TwoIntTuple> tupleList = new BasicEList<TwoIntTuple>();
		for(IBooking ib:bookings){
			
			Booking b = (Booking) ib;
			if(b.getStartDate().compareTo(date) <= 0 && b.getEndDate().compareTo(date) >= 0){
				
				if (b.getState() == State.CHECKED_IN) {
					for(Integer r : b.listAssignedRooms()) {
						
						TwoIntTuple tuple = BookingFactory.eINSTANCE.createTwoIntTuple();
						tuple.setI1(b.getBookingID());
						tuple.setI2(r);
						tupleList.add(tuple);	
					}
				} else if (b.getState() == State.CONFIRMED) {
					
					String roomType;
					Integer numbersOfType;
					
					for(StringIntTuple sit : b.getRoomTypeAmountTuples()){
						
						roomType = sit.getS();
						numbersOfType = sit.getI();
						
						for (Integer room : RoomTypeFactory.eINSTANCE.createRoomTypeProvider().listRoomsOfType(roomType)) {
							
							for (TwoIntTuple t : tupleList) {
								
								if (numbersOfType <= 0) {
									break;
								}
								
								if (t.getI2() != room) {
									TwoIntTuple tup = BookingFactory.eINSTANCE.createTwoIntTuple();
									tup.setI1(b.getBookingID());
									tup.setI2(room);
									tupleList.add(tup);
									numbersOfType--;
								}
							}
						} 
					}	
				}
			}
		}
		return tupleList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<StringIntTuple> getFreeRooms(String startDate, String endDate, int numberOfBeds) {
		EList<StringIntTuple> toReturn = new BasicEList<StringIntTuple>();
		EList<String> roomTypes = roomtypeprovider.listRoomTypes();
		for (String str : roomTypes){
			if(roomtypeprovider.getNumberOfBeds(str)>=numberOfBeds){
				StringIntTuple sit = BookingFactory.eINSTANCE.createStringIntTuple();
				sit.setS(str);
				sit.setI(getFreeRoomsForRoomType(str, startDate, endDate));
				toReturn.add(sit);
			}
		}
		return toReturn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getFreeRoomsForRoomType(String roomTypeDescription, String startDate, String endDate) {
		int free = 0;
		Booking b;
		free += roomtypeprovider.getMaximumNumberOfRooms(roomTypeDescription);
		
		for (IBooking ib : bookings){

			if(ib.getState()==State.CONFIRMED || ib.getState()==State.CHECKED_IN){ //if booking is confirmed or checked in, it may overlap
				
				if(isOverlapping(startDate, endDate, ib.getStartDate(), ib.getEndDate())){ //check if overlapping
					
					if(ib.getState()==State.CHECKED_IN){ //if booking is checked in, there might be rooms checked out
						EList<Integer> rooms = ib.listAssignedRooms();
						
						for(Integer room : rooms){
							if(roomtypeprovider.getTypeOfRoom(room).equals(roomTypeDescription) && roomprovider.getStatus(room)==RoomStatus.OCCUPIED){ //if same type and rooms is occupied
								free--;
							}
						}
						
					} else {
						for (StringIntTuple sit : ib.getRoomTypeAmountTuples()){
							if (sit.getS().equals(roomTypeDescription)) free -= sit.getI(); //remove overlapping
						}
					}
					
				}
			}
		}
		return free;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> listCheckIns(String startDate, String endDate) {
		EList<Integer> list = new BasicEList<Integer>();
		
		for(IBooking ib:bookings){
			if(ib.getState()==State.CHECKED_IN && ib.getStartDate().compareTo(startDate) >= 0 && ib.getStartDate().compareTo(endDate) <= 0){
				
				list.add(new Integer(ib.getBookingID()));
				
			}
		}
		return list;	
	}


	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> listCheckOuts(String startDate, String endDate) {
		EList<Integer> list = new BasicEList<Integer>();
		
		for(IBooking ib:bookings){
			if(ib.getState()==State.CHECKED_OUT && ib.getEndDate().compareTo(startDate) >= 0 && ib.getEndDate().compareTo(endDate) <= 0){
				
				list.add(new Integer(ib.getBookingID()));
				
			}
		}
		return list;	
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateNameOfBooking(int bookingID, String firstName, String lastName) {
		Booking b = (Booking)getBooking(bookingID);
		if(b==null){
			return false;
		}
		b.setFirstName(firstName);
		b.setLastName(lastName);
		return true;	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateDateOfBooking(int bookingID, String startDate, String endDate) {
		Booking b = (Booking)getBooking(bookingID);
		if(b==null){
			return false;
		}
		
		b.setStartDate(startDate);
		b.setEndDate(endDate);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isValid(int bookingID) {
		return getBooking(bookingID) != null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		IBooking b = null;
		for(IBooking booking:bookings){
			if(booking.getState()==chalmers.cse.mdsd1617.group14.BookingSystem.Booking.State.CHECKED_OUT){
				b=booking;
				break;
			}
		}
		
		if(b==null){
			return false;
		}
		
		ITab tab = getTabOfBooking(b.getBookingID());
		double sum = tab.calculateTotal();
		
		if(pay(ccNumber, ccv, expiryMonth, expiryYear, firstName,lastName,sum)){
			b.setState(chalmers.cse.mdsd1617.group14.BookingSystem.Booking.State.PAID);
			return true;
		} else {
			return false;
		}
		

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isBlockable(int roomNumber) {
		for (IBooking ib : bookings){				
				if(ib.getState()==State.CHECKED_IN){
					EList<Integer> rooms = ib.listAssignedRooms();		
					for (Integer room: rooms){
						if (room==roomNumber)
							return false;
					}
				}
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean reset() {
		this.bookings = new BasicEList<IBooking>();
		
		return true;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		IBooking b = null;
		
		for(IBooking booking:bookings){
			
			EList<Integer> rooms = booking.listAssignedRooms();
			
			if(booking.getState()==State.CHECKED_IN || booking.getState()==State.CHECKED_OUT){
				for(Integer r:rooms){
					if(r==roomNumber && roomprovider.getStatus(r)==RoomStatus.FREE){
						b=booking;
						break;
					}
				}
			}
			
			if(b!=null){
				break;
			}
		}
		
		if(b==null){
			return false;
		}
		
		
		
		ITab tab = getTabOfRoom(b.getBookingID(), roomNumber);
		double sum = tab.calculateTotal();
		
		if(pay(ccNumber, ccv, expiryMonth, expiryYear, firstName,lastName,sum)){
			b.unassignRoom(roomNumber); //removes room from list of rooms in booking
			if(b.listAssignedRooms().isEmpty()){ //if it's the last room of the booking, set booking status to paid
				b.setState(chalmers.cse.mdsd1617.group14.BookingSystem.Booking.State.PAID);
			}
			return true;
		} else {
			return false;
		}
	}
	
	private boolean pay(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName,String lastName,double sum){
		try {
			se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires banking = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires
					.instance();

			if (banking.isCreditCardValid(ccNumber, ccv, expiryMonth, expiryYear, firstName,
					lastName)) {
				System.out.println("Valid credit card");
			} else {
				System.out.println("Invalid credit card");
				return false;
			}

			if (banking.makePayment(ccNumber, ccv, expiryMonth, expiryYear, firstName,lastName,sum)) {
				System.out.println("Payment successfully processed");
			} else {
				System.out.println("Payment failed - invalid credit card or insufficient credit");
				return false;
			}

		} catch (SOAPException e) {
			System.err.println("Error occurred while communicating with the bank");
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> getBooking(String roomTypeDescription, String startDate, String endDate) {
		EList<Integer> rooms = new BasicEList<Integer>();
		boolean roomTypeMatch = false;
		for(IBooking booking : bookings){
			for(StringIntTuple stringIntTuple : booking.getRoomTypeAmountTuples()){
				if(stringIntTuple.getS() == roomTypeDescription 
						&& booking.getStartDate().compareTo(startDate) >= 0 
						&& booking.getEndDate().compareTo(endDate) <= 0){
					rooms.add(booking.getBookingID());
				}
			}
			
		}
		return rooms;
	}

	private IBooking getBooking(int bookingID){
		for(IBooking b:bookings){
			if(b.getId()==bookingID){
				return b;
			}
		}
		return null;
	}
	
	
	/**
	 * Checks if dates are overlapping. Date format "YYYYMMDD"
	 * @param startB1 Start date of booking 1
	 * @param endB1 End date of booking 1
	 * @param startB2 Start date of booking 2
	 * @param endB2 End date of booking 2
	 * @return
	 */
	private boolean isOverlapping(String startB1, String endB1, String startB2, String endB2){
		
		if(startB1.compareTo(startB2)<0 && endB1.compareTo(startB2)>0){
			//Starts within period
			return true;
		}
		
		if(startB1.compareTo(endB2)<0 && endB1.compareTo(endB2)>0){
			//Ends within period
			return true;
		}
		
		if(startB1.compareTo(startB2)>=0 && endB1.compareTo(endB2)<=0){
			//Starts on/before, ends on/after period
			return true;
		}
		
		return false;
		
	}

} //BookingTableImpl
