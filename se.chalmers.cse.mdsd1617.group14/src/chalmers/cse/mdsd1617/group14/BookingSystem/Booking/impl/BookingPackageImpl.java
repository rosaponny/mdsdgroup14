/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.Booking;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingTable;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBooking;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IntDTOTuple;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.State;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.StringIntTuple;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.EconomicsPackage;

import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.EconomicsPackageImpl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomPackage;

import chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypePackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl;
import chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage;
import chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.types.impl.TypesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BookingPackageImpl extends EPackageImpl implements BookingPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iBookingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iBookingProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass twoIntTupleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringIntTupleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bookingProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bookingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iBookingTableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bookingTableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass intDTOTupleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum stateEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private BookingPackageImpl() {
		super(eNS_URI, BookingFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link BookingPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static BookingPackage init() {
		if (isInited) return (BookingPackage)EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI);

		// Obtain or create and register package
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new BookingPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		SystemAccessPackageImpl theSystemAccessPackage = (SystemAccessPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SystemAccessPackage.eNS_URI) instanceof SystemAccessPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SystemAccessPackage.eNS_URI) : SystemAccessPackage.eINSTANCE);
		EconomicsPackageImpl theEconomicsPackage = (EconomicsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(EconomicsPackage.eNS_URI) instanceof EconomicsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(EconomicsPackage.eNS_URI) : EconomicsPackage.eINSTANCE);
		RoomTypePackageImpl theRoomTypePackage = (RoomTypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI) instanceof RoomTypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI) : RoomTypePackage.eINSTANCE);
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) : RoomPackage.eINSTANCE);
		TypesPackageImpl theTypesPackage = (TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) : TypesPackage.eINSTANCE);

		// Create package meta-data objects
		theBookingPackage.createPackageContents();
		theSystemAccessPackage.createPackageContents();
		theEconomicsPackage.createPackageContents();
		theRoomTypePackage.createPackageContents();
		theRoomPackage.createPackageContents();
		theTypesPackage.createPackageContents();

		// Initialize created meta-data
		theBookingPackage.initializePackageContents();
		theSystemAccessPackage.initializePackageContents();
		theEconomicsPackage.initializePackageContents();
		theRoomTypePackage.initializePackageContents();
		theRoomPackage.initializePackageContents();
		theTypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theBookingPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(BookingPackage.eNS_URI, theBookingPackage);
		return theBookingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIBooking() {
		return iBookingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__InitiateBooking__String_String_String_String() {
		return iBookingEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__ConfirmBooking() {
		return iBookingEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__AddRoomToBooking__String() {
		return iBookingEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__RemoveRoomFromBooking__String() {
		return iBookingEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__CancelBooking() {
		return iBookingEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__CheckInBooking() {
		return iBookingEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__CheckOutBooking() {
		return iBookingEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__CheckInRoom__String() {
		return iBookingEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__CheckOutRoom__int() {
		return iBookingEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__GetTabOfRoom__int() {
		return iBookingEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__GetID() {
		return iBookingEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__GetBookingID() {
		return iBookingEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__GetTabOfBooking() {
		return iBookingEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__ListAssignedRooms() {
		return iBookingEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__GetState() {
		return iBookingEClass.getEOperations().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__SetState__State() {
		return iBookingEClass.getEOperations().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__GetStartDate() {
		return iBookingEClass.getEOperations().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__GetEndDate() {
		return iBookingEClass.getEOperations().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__GetRoomTypeAmountTuples() {
		return iBookingEClass.getEOperations().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__UnassignRoom__int() {
		return iBookingEClass.getEOperations().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__ToString() {
		return iBookingEClass.getEOperations().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBooking__AssignRoom__int() {
		return iBookingEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIBookingProvider() {
		return iBookingProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__ListCheckIns__String() {
		return iBookingProviderEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__InitiateBooking__String_String_String_String() {
		return iBookingProviderEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__ConfirmBooking__int() {
		return iBookingProviderEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__AddRoomToBooking__String_int() {
		return iBookingProviderEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__RemoveRoomFromBooking__String_int() {
		return iBookingProviderEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__CancelBooking__int() {
		return iBookingProviderEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__CheckInBooking__int() {
		return iBookingProviderEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__CheckOutBooking__int() {
		return iBookingProviderEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__CheckInRoom__int_String() {
		return iBookingProviderEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__CheckOutRoom__int_int() {
		return iBookingProviderEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__GetTabOfRoom__int_int() {
		return iBookingProviderEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__GetTabOfBooking__int() {
		return iBookingProviderEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__GetOccupiedRooms__String() {
		return iBookingProviderEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__GetFreeRooms__String_String_int() {
		return iBookingProviderEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__GetFreeRoomsForRoomType__String_String_String() {
		return iBookingProviderEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__ListCheckIns__String_String() {
		return iBookingProviderEClass.getEOperations().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__ListCheckOuts__String_String() {
		return iBookingProviderEClass.getEOperations().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__UpdateNameOfBooking__int_String_String() {
		return iBookingProviderEClass.getEOperations().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__UpdateDateOfBooking__int_String_String() {
		return iBookingProviderEClass.getEOperations().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__IsValid__int() {
		return iBookingProviderEClass.getEOperations().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__PayDuringCheckout__String_String_int_int_String_String() {
		return iBookingProviderEClass.getEOperations().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__IsBlockable__int() {
		return iBookingProviderEClass.getEOperations().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__Reset() {
		return iBookingProviderEClass.getEOperations().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__PayRoomDuringCheckout__int_String_String_int_int_String_String() {
		return iBookingProviderEClass.getEOperations().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__GetBooking__String_String_String() {
		return iBookingProviderEClass.getEOperations().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__ListCheckOuts__String() {
		return iBookingProviderEClass.getEOperations().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__ListBookings() {
		return iBookingProviderEClass.getEOperations().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingProvider__AddExtraCostToRoom__int_int_String_int() {
		return iBookingProviderEClass.getEOperations().get(27);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTwoIntTuple() {
		return twoIntTupleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTwoIntTuple_I1() {
		return (EAttribute)twoIntTupleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTwoIntTuple_I2() {
		return (EAttribute)twoIntTupleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringIntTuple() {
		return stringIntTupleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringIntTuple_S() {
		return (EAttribute)stringIntTupleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringIntTuple_I() {
		return (EAttribute)stringIntTupleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBookingProvider() {
		return bookingProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookingProvider_Ibookingtable() {
		return (EReference)bookingProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooking() {
		return bookingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_NumberOfRooms() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_AssignedRooms() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_BookingID() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBooking_RoomTypeAmountTuples() {
		return (EReference)bookingEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_State() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_StartDate() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_EndDate() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_FirstName() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooking_LastName() {
		return (EAttribute)bookingEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIBookingTable() {
		return iBookingTableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__ListCheckIns__String() {
		return iBookingTableEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__InitiateBooking__String_String_String_String() {
		return iBookingTableEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__ConfirmBooking__int() {
		return iBookingTableEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__AddRoomToBooking__String_int() {
		return iBookingTableEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__RemoveRoomFromBooking__String_int() {
		return iBookingTableEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__CancelBooking__int() {
		return iBookingTableEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__CheckInBooking__int() {
		return iBookingTableEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__CheckOutBooking__int() {
		return iBookingTableEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__CheckInRoom__int_String() {
		return iBookingTableEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__CheckOutRoom__int_int() {
		return iBookingTableEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__GetTabOfRoom__int_int() {
		return iBookingTableEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__GetTabOfBooking__int() {
		return iBookingTableEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__GetOccupiedRooms__String() {
		return iBookingTableEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__GetFreeRooms__String_String_int() {
		return iBookingTableEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__GetFreeRoomsForRoomType__String_String_String() {
		return iBookingTableEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__ListCheckIns__String_String() {
		return iBookingTableEClass.getEOperations().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__ListCheckOuts__String_String() {
		return iBookingTableEClass.getEOperations().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__UpdateNameOfBooking__int_String_String() {
		return iBookingTableEClass.getEOperations().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__UpdateDateOfBooking__int_String_String() {
		return iBookingTableEClass.getEOperations().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__IsValid__int() {
		return iBookingTableEClass.getEOperations().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__PayDuringCheckout__String_String_int_int_String_String() {
		return iBookingTableEClass.getEOperations().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__IsBlockable__int() {
		return iBookingTableEClass.getEOperations().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__Reset() {
		return iBookingTableEClass.getEOperations().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__PayRoomDuringCheckout__int_String_String_int_int_String_String() {
		return iBookingTableEClass.getEOperations().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__GetBooking__String_String_String() {
		return iBookingTableEClass.getEOperations().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__ListBookings() {
		return iBookingTableEClass.getEOperations().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__ListCheckOuts__String() {
		return iBookingTableEClass.getEOperations().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIBookingTable__AddExtraCostToRoom__int_int_String_int() {
		return iBookingTableEClass.getEOperations().get(27);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBookingTable() {
		return bookingTableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBookingTable_Bookings() {
		return (EReference)bookingTableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntDTOTuple() {
		return intDTOTupleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntDTOTuple_I1() {
		return (EAttribute)intDTOTupleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIntDTOTuple_FreeRoomTypesDTO() {
		return (EReference)intDTOTupleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getState() {
		return stateEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingFactory getBookingFactory() {
		return (BookingFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		iBookingEClass = createEClass(IBOOKING);
		createEOperation(iBookingEClass, IBOOKING___INITIATE_BOOKING__STRING_STRING_STRING_STRING);
		createEOperation(iBookingEClass, IBOOKING___CONFIRM_BOOKING);
		createEOperation(iBookingEClass, IBOOKING___ADD_ROOM_TO_BOOKING__STRING);
		createEOperation(iBookingEClass, IBOOKING___REMOVE_ROOM_FROM_BOOKING__STRING);
		createEOperation(iBookingEClass, IBOOKING___CANCEL_BOOKING);
		createEOperation(iBookingEClass, IBOOKING___CHECK_IN_BOOKING);
		createEOperation(iBookingEClass, IBOOKING___CHECK_OUT_BOOKING);
		createEOperation(iBookingEClass, IBOOKING___CHECK_IN_ROOM__STRING);
		createEOperation(iBookingEClass, IBOOKING___CHECK_OUT_ROOM__INT);
		createEOperation(iBookingEClass, IBOOKING___GET_TAB_OF_ROOM__INT);
		createEOperation(iBookingEClass, IBOOKING___GET_ID);
		createEOperation(iBookingEClass, IBOOKING___ASSIGN_ROOM__INT);
		createEOperation(iBookingEClass, IBOOKING___GET_BOOKING_ID);
		createEOperation(iBookingEClass, IBOOKING___GET_TAB_OF_BOOKING);
		createEOperation(iBookingEClass, IBOOKING___LIST_ASSIGNED_ROOMS);
		createEOperation(iBookingEClass, IBOOKING___GET_STATE);
		createEOperation(iBookingEClass, IBOOKING___SET_STATE__STATE);
		createEOperation(iBookingEClass, IBOOKING___GET_START_DATE);
		createEOperation(iBookingEClass, IBOOKING___GET_END_DATE);
		createEOperation(iBookingEClass, IBOOKING___GET_ROOM_TYPE_AMOUNT_TUPLES);
		createEOperation(iBookingEClass, IBOOKING___UNASSIGN_ROOM__INT);
		createEOperation(iBookingEClass, IBOOKING___TO_STRING);

		stringIntTupleEClass = createEClass(STRING_INT_TUPLE);
		createEAttribute(stringIntTupleEClass, STRING_INT_TUPLE__S);
		createEAttribute(stringIntTupleEClass, STRING_INT_TUPLE__I);

		iBookingProviderEClass = createEClass(IBOOKING_PROVIDER);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___LIST_CHECK_INS__STRING);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___INITIATE_BOOKING__STRING_STRING_STRING_STRING);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___CONFIRM_BOOKING__INT);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___ADD_ROOM_TO_BOOKING__STRING_INT);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___REMOVE_ROOM_FROM_BOOKING__STRING_INT);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___CANCEL_BOOKING__INT);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___CHECK_IN_BOOKING__INT);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___CHECK_OUT_BOOKING__INT);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___CHECK_IN_ROOM__INT_STRING);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___CHECK_OUT_ROOM__INT_INT);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___GET_TAB_OF_ROOM__INT_INT);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___GET_TAB_OF_BOOKING__INT);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___GET_OCCUPIED_ROOMS__STRING);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___GET_FREE_ROOMS__STRING_STRING_INT);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___GET_FREE_ROOMS_FOR_ROOM_TYPE__STRING_STRING_STRING);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___LIST_CHECK_INS__STRING_STRING);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___LIST_CHECK_OUTS__STRING_STRING);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___UPDATE_NAME_OF_BOOKING__INT_STRING_STRING);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___UPDATE_DATE_OF_BOOKING__INT_STRING_STRING);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___IS_VALID__INT);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___IS_BLOCKABLE__INT);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___RESET);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___GET_BOOKING__STRING_STRING_STRING);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___LIST_CHECK_OUTS__STRING);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___LIST_BOOKINGS);
		createEOperation(iBookingProviderEClass, IBOOKING_PROVIDER___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_INT);

		twoIntTupleEClass = createEClass(TWO_INT_TUPLE);
		createEAttribute(twoIntTupleEClass, TWO_INT_TUPLE__I1);
		createEAttribute(twoIntTupleEClass, TWO_INT_TUPLE__I2);

		bookingProviderEClass = createEClass(BOOKING_PROVIDER);
		createEReference(bookingProviderEClass, BOOKING_PROVIDER__IBOOKINGTABLE);

		iBookingTableEClass = createEClass(IBOOKING_TABLE);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___LIST_CHECK_INS__STRING);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___INITIATE_BOOKING__STRING_STRING_STRING_STRING);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___CONFIRM_BOOKING__INT);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___ADD_ROOM_TO_BOOKING__STRING_INT);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___REMOVE_ROOM_FROM_BOOKING__STRING_INT);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___CANCEL_BOOKING__INT);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___CHECK_IN_BOOKING__INT);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___CHECK_OUT_BOOKING__INT);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___CHECK_IN_ROOM__INT_STRING);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___CHECK_OUT_ROOM__INT_INT);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___GET_TAB_OF_ROOM__INT_INT);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___GET_TAB_OF_BOOKING__INT);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___GET_OCCUPIED_ROOMS__STRING);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___GET_FREE_ROOMS__STRING_STRING_INT);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___GET_FREE_ROOMS_FOR_ROOM_TYPE__STRING_STRING_STRING);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___LIST_CHECK_INS__STRING_STRING);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___LIST_CHECK_OUTS__STRING_STRING);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___UPDATE_NAME_OF_BOOKING__INT_STRING_STRING);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___UPDATE_DATE_OF_BOOKING__INT_STRING_STRING);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___IS_VALID__INT);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___IS_BLOCKABLE__INT);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___RESET);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___GET_BOOKING__STRING_STRING_STRING);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___LIST_BOOKINGS);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___LIST_CHECK_OUTS__STRING);
		createEOperation(iBookingTableEClass, IBOOKING_TABLE___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_INT);

		bookingEClass = createEClass(BOOKING);
		createEAttribute(bookingEClass, BOOKING__NUMBER_OF_ROOMS);
		createEAttribute(bookingEClass, BOOKING__ASSIGNED_ROOMS);
		createEAttribute(bookingEClass, BOOKING__BOOKING_ID);
		createEReference(bookingEClass, BOOKING__ROOM_TYPE_AMOUNT_TUPLES);
		createEAttribute(bookingEClass, BOOKING__END_DATE);
		createEAttribute(bookingEClass, BOOKING__FIRST_NAME);
		createEAttribute(bookingEClass, BOOKING__LAST_NAME);
		createEAttribute(bookingEClass, BOOKING__STATE);
		createEAttribute(bookingEClass, BOOKING__START_DATE);

		bookingTableEClass = createEClass(BOOKING_TABLE);
		createEReference(bookingTableEClass, BOOKING_TABLE__BOOKINGS);

		intDTOTupleEClass = createEClass(INT_DTO_TUPLE);
		createEAttribute(intDTOTupleEClass, INT_DTO_TUPLE__I1);
		createEReference(intDTOTupleEClass, INT_DTO_TUPLE__FREE_ROOM_TYPES_DTO);

		// Create enums
		stateEEnum = createEEnum(STATE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EconomicsPackage theEconomicsPackage = (EconomicsPackage)EPackage.Registry.INSTANCE.getEPackage(EconomicsPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);
		RoomTypePackage theRoomTypePackage = (RoomTypePackage)EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		bookingProviderEClass.getESuperTypes().add(this.getIBookingProvider());
		bookingEClass.getESuperTypes().add(this.getIBooking());
		bookingTableEClass.getESuperTypes().add(this.getIBookingTable());

		// Initialize classes, features, and operations; add parameters
		initEClass(iBookingEClass, IBooking.class, "IBooking", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getIBooking__InitiateBooking__String_String_String_String(), ecorePackage.getEInt(), "initiateBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__ConfirmBooking(), ecorePackage.getEBoolean(), "confirmBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBooking__AddRoomToBooking__String(), ecorePackage.getEBoolean(), "addRoomToBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBooking__RemoveRoomFromBooking__String(), ecorePackage.getEBoolean(), "removeRoomFromBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__CancelBooking(), ecorePackage.getEBoolean(), "cancelBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__CheckInBooking(), ecorePackage.getEBoolean(), "checkInBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__CheckOutBooking(), ecorePackage.getEBoolean(), "checkOutBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBooking__CheckInRoom__String(), ecorePackage.getEInt(), "checkInRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBooking__CheckOutRoom__int(), ecorePackage.getEBoolean(), "checkOutRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBooking__GetTabOfRoom__int(), theEconomicsPackage.getITab(), "getTabOfRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__GetID(), theTypesPackage.getInteger(), "getID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBooking__AssignRoom__int(), ecorePackage.getEBoolean(), "assignRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__GetBookingID(), ecorePackage.getEInt(), "getBookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__GetTabOfBooking(), theEconomicsPackage.getITab(), "getTabOfBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__ListAssignedRooms(), ecorePackage.getEInt(), "listAssignedRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__GetState(), this.getState(), "getState", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBooking__SetState__State(), null, "setState", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getState(), "state", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__GetStartDate(), ecorePackage.getEString(), "getStartDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__GetEndDate(), ecorePackage.getEString(), "getEndDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__GetRoomTypeAmountTuples(), this.getStringIntTuple(), "getRoomTypeAmountTuples", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBooking__UnassignRoom__int(), null, "unassignRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBooking__ToString(), ecorePackage.getEString(), "toString", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(stringIntTupleEClass, StringIntTuple.class, "StringIntTuple", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringIntTuple_S(), ecorePackage.getEString(), "s", null, 1, 1, StringIntTuple.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getStringIntTuple_I(), ecorePackage.getEInt(), "i", null, 1, 1, StringIntTuple.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iBookingProviderEClass, IBookingProvider.class, "IBookingProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIBookingProvider__ListCheckIns__String(), ecorePackage.getEInt(), "listCheckIns", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "day", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__InitiateBooking__String_String_String_String(), ecorePackage.getEInt(), "initiateBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__ConfirmBooking__int(), ecorePackage.getEBoolean(), "confirmBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__AddRoomToBooking__String_int(), ecorePackage.getEBoolean(), "addRoomToBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__RemoveRoomFromBooking__String_int(), ecorePackage.getEBoolean(), "removeRoomFromBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__CancelBooking__int(), ecorePackage.getEBoolean(), "cancelBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__CheckInBooking__int(), ecorePackage.getEBoolean(), "checkInBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__CheckOutBooking__int(), ecorePackage.getEBoolean(), "checkOutBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__CheckInRoom__int_String(), ecorePackage.getEInt(), "checkInRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__CheckOutRoom__int_int(), ecorePackage.getEBoolean(), "checkOutRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__GetTabOfRoom__int_int(), theEconomicsPackage.getITab(), "getTabOfRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__GetTabOfBooking__int(), theEconomicsPackage.getITab(), "getTabOfBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__GetOccupiedRooms__String(), this.getTwoIntTuple(), "getOccupiedRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "date", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__GetFreeRooms__String_String_int(), this.getStringIntTuple(), "getFreeRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "numberOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__GetFreeRoomsForRoomType__String_String_String(), ecorePackage.getEInt(), "getFreeRoomsForRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__ListCheckIns__String_String(), ecorePackage.getEInt(), "listCheckIns", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__ListCheckOuts__String_String(), ecorePackage.getEInt(), "listCheckOuts", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__UpdateNameOfBooking__int_String_String(), ecorePackage.getEBoolean(), "updateNameOfBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__UpdateDateOfBooking__int_String_String(), ecorePackage.getEBoolean(), "updateDateOfBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__IsValid__int(), ecorePackage.getEBoolean(), "isValid", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__PayDuringCheckout__String_String_int_int_String_String(), ecorePackage.getEBoolean(), "payDuringCheckout", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ccNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ccv", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "expiryMonth", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "expiryYear", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__IsBlockable__int(), ecorePackage.getEBoolean(), "isBlockable", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBookingProvider__Reset(), ecorePackage.getEBoolean(), "reset", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__PayRoomDuringCheckout__int_String_String_int_int_String_String(), ecorePackage.getEBoolean(), "payRoomDuringCheckout", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ccNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ccv", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "expiryMonth", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "expiryYear", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__GetBooking__String_String_String(), ecorePackage.getEInt(), "getBooking", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__ListCheckOuts__String(), ecorePackage.getEInt(), "listCheckOuts", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "day", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBookingProvider__ListBookings(), ecorePackage.getEString(), "listBookings", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingProvider__AddExtraCostToRoom__int_int_String_int(), ecorePackage.getEBoolean(), "addExtraCostToRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "costDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(twoIntTupleEClass, TwoIntTuple.class, "TwoIntTuple", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTwoIntTuple_I1(), ecorePackage.getEInt(), "i1", null, 1, 1, TwoIntTuple.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getTwoIntTuple_I2(), ecorePackage.getEInt(), "i2", null, 1, 1, TwoIntTuple.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(bookingProviderEClass, BookingProvider.class, "BookingProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBookingProvider_Ibookingtable(), this.getIBookingTable(), null, "ibookingtable", null, 1, 1, BookingProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iBookingTableEClass, IBookingTable.class, "IBookingTable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIBookingTable__ListCheckIns__String(), ecorePackage.getEInt(), "listCheckIns", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "day", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__InitiateBooking__String_String_String_String(), ecorePackage.getEInt(), "initiateBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__ConfirmBooking__int(), ecorePackage.getEBoolean(), "confirmBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__AddRoomToBooking__String_int(), ecorePackage.getEBoolean(), "addRoomToBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__RemoveRoomFromBooking__String_int(), ecorePackage.getEBoolean(), "removeRoomFromBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__CancelBooking__int(), ecorePackage.getEBoolean(), "cancelBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__CheckInBooking__int(), ecorePackage.getEBoolean(), "checkInBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__CheckOutBooking__int(), ecorePackage.getEBoolean(), "checkOutBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__CheckInRoom__int_String(), ecorePackage.getEInt(), "checkInRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__CheckOutRoom__int_int(), ecorePackage.getEBoolean(), "checkOutRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__GetTabOfRoom__int_int(), theEconomicsPackage.getITab(), "getTabOfRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__GetTabOfBooking__int(), theEconomicsPackage.getITab(), "getTabOfBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__GetOccupiedRooms__String(), this.getTwoIntTuple(), "getOccupiedRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "date", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__GetFreeRooms__String_String_int(), this.getStringIntTuple(), "getFreeRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "numberOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__GetFreeRoomsForRoomType__String_String_String(), ecorePackage.getEInt(), "getFreeRoomsForRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__ListCheckIns__String_String(), ecorePackage.getEInt(), "listCheckIns", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__ListCheckOuts__String_String(), ecorePackage.getEInt(), "listCheckOuts", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__UpdateNameOfBooking__int_String_String(), ecorePackage.getEBoolean(), "updateNameOfBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__UpdateDateOfBooking__int_String_String(), ecorePackage.getEBoolean(), "updateDateOfBooking", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__IsValid__int(), ecorePackage.getEBoolean(), "isValid", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__PayDuringCheckout__String_String_int_int_String_String(), ecorePackage.getEBoolean(), "payDuringCheckout", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ccNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ccv", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "expiryMonth", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "expiryYear", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__IsBlockable__int(), ecorePackage.getEBoolean(), "isBlockable", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBookingTable__Reset(), ecorePackage.getEBoolean(), "reset", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__PayRoomDuringCheckout__int_String_String_int_int_String_String(), ecorePackage.getEBoolean(), "payRoomDuringCheckout", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ccNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "ccv", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "expiryMonth", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "expiryYear", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "firstName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "lastName", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__GetBooking__String_String_String(), ecorePackage.getEInt(), "getBooking", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "startDate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "endDate", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIBookingTable__ListBookings(), ecorePackage.getEString(), "listBookings", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__ListCheckOuts__String(), ecorePackage.getEInt(), "listCheckOuts", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "day", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIBookingTable__AddExtraCostToRoom__int_int_String_int(), ecorePackage.getEBoolean(), "addExtraCostToRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "bookingID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "costDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(bookingEClass, Booking.class, "Booking", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBooking_NumberOfRooms(), ecorePackage.getEInt(), "numberOfRooms", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_AssignedRooms(), ecorePackage.getEInt(), "assignedRooms", null, 0, -1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_BookingID(), ecorePackage.getEInt(), "bookingID", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getBooking_RoomTypeAmountTuples(), this.getStringIntTuple(), null, "roomTypeAmountTuples", null, 0, -1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_EndDate(), ecorePackage.getEString(), "endDate", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_FirstName(), ecorePackage.getEString(), "firstName", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_LastName(), ecorePackage.getEString(), "lastName", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_State(), this.getState(), "state", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBooking_StartDate(), ecorePackage.getEString(), "startDate", null, 1, 1, Booking.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(bookingTableEClass, BookingTable.class, "BookingTable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBookingTable_Bookings(), this.getIBooking(), null, "bookings", null, 0, -1, BookingTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(intDTOTupleEClass, IntDTOTuple.class, "IntDTOTuple", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntDTOTuple_I1(), ecorePackage.getEInt(), "i1", null, 1, 1, IntDTOTuple.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getIntDTOTuple_FreeRoomTypesDTO(), theRoomTypePackage.getFreeRoomTypesDTO(), null, "freeRoomTypesDTO", null, 1, 1, IntDTOTuple.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(stateEEnum, State.class, "State");
		addEEnumLiteral(stateEEnum, State.UNCOMFIRMED);
		addEEnumLiteral(stateEEnum, State.CONFIRMED);
		addEEnumLiteral(stateEEnum, State.CHECKED_IN);
		addEEnumLiteral(stateEEnum, State.CHECKED_OUT);
		addEEnumLiteral(stateEEnum, State.PAID);
		addEEnumLiteral(stateEEnum, State.CANCELLED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/uml2/2.0.0/UML
		createUMLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/uml2/2.0.0/UML</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createUMLAnnotations() {
		String source = "http://www.eclipse.org/uml2/2.0.0/UML";	
		addAnnotation
		  (getIBooking__GetID(), 
		   source, 
		   new String[] {
			 "originalName", "getID()"
		   });
	}

} //BookingPackageImpl
