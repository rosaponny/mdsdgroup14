/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.EconomicsFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab;

import chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomPackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomStatus;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomImpl#getStatus <em>Status</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomImpl#getRoomNbr <em>Room Nbr</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomImpl#getTab <em>Tab</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomImpl extends MinimalEObjectImpl.Container implements Room {
	/**
	 * The default value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected static final RoomStatus STATUS_EDEFAULT = RoomStatus.FREE;

	/**
	 * The cached value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected RoomStatus status = STATUS_EDEFAULT;

	/**
	 * The default value of the '{@link #getRoomNbr() <em>Room Nbr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomNbr()
	 * @generated
	 * @ordered
	 */
	protected static final int ROOM_NBR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getRoomNbr() <em>Room Nbr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomNbr()
	 * @generated
	 * @ordered
	 */
	protected int roomNbr = ROOM_NBR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTab() <em>Tab</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTab()
	 * @generated
	 * @ordered
	 */
	protected ITab tab;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected RoomImpl() {
		super();
		
		tab = chalmers.cse.mdsd1617.group14.BookingSystem.Economics.EconomicsFactory.eINSTANCE.createTab();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomStatus getStatus() {
		return status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatus(RoomStatus newStatus) {
		RoomStatus oldStatus = status;
		status = newStatus == null ? STATUS_EDEFAULT : newStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM__STATUS, oldStatus, status));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRoomNbr() {
		return roomNbr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomNbr(int newRoomNbr) {
		int oldRoomNbr = roomNbr;
		roomNbr = newRoomNbr;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM__ROOM_NBR, oldRoomNbr, roomNbr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ITab getTab() {
		if (tab != null && tab.eIsProxy()) {
			InternalEObject oldTab = (InternalEObject)tab;
			tab = (ITab)eResolveProxy(oldTab);
			if (tab != oldTab) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RoomPackage.ROOM__TAB, oldTab, tab));
			}
		}
		return tab;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ITab basicGetTab() {
		return tab;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTab(ITab newTab) {
		ITab oldTab = tab;
		tab = newTab;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM__TAB, oldTab, tab));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean block() {
		if(status==RoomStatus.FREE){
			status = RoomStatus.BLOCKED;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean unblock() {
		if(status==RoomStatus.BLOCKED){
			status = RoomStatus.FREE;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void occupy() {
		status = RoomStatus.OCCUPIED;
		tab = EconomicsFactory.eINSTANCE.createTab();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void free() {
		status = RoomStatus.FREE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addBill(IBill bill) {
		return tab.addBill(bill);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomPackage.ROOM__STATUS:
				return getStatus();
			case RoomPackage.ROOM__ROOM_NBR:
				return getRoomNbr();
			case RoomPackage.ROOM__TAB:
				if (resolve) return getTab();
				return basicGetTab();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomPackage.ROOM__STATUS:
				setStatus((RoomStatus)newValue);
				return;
			case RoomPackage.ROOM__ROOM_NBR:
				setRoomNbr((Integer)newValue);
				return;
			case RoomPackage.ROOM__TAB:
				setTab((ITab)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM__STATUS:
				setStatus(STATUS_EDEFAULT);
				return;
			case RoomPackage.ROOM__ROOM_NBR:
				setRoomNbr(ROOM_NBR_EDEFAULT);
				return;
			case RoomPackage.ROOM__TAB:
				setTab((ITab)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM__STATUS:
				return status != STATUS_EDEFAULT;
			case RoomPackage.ROOM__ROOM_NBR:
				return roomNbr != ROOM_NBR_EDEFAULT;
			case RoomPackage.ROOM__TAB:
				return tab != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomPackage.ROOM___BLOCK:
				return block();
			case RoomPackage.ROOM___UNBLOCK:
				return unblock();
			case RoomPackage.ROOM___OCCUPY:
				occupy();
				return null;
			case RoomPackage.ROOM___FREE:
				free();
				return null;
			case RoomPackage.ROOM___ADD_BILL__IBILL:
				return addBill((IBill)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (status: ");
		result.append(status);
		result.append(", roomNbr: ");
		result.append(roomNbr);
		result.append(')');
		return result.toString();
	}

} //RoomImpl
