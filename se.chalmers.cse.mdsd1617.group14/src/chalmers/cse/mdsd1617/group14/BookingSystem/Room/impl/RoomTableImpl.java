/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomPackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomStatus;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomTable;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Table</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomTableImpl#getRooms <em>Rooms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomTableImpl extends MinimalEObjectImpl.Container implements RoomTable {
	
	/**
	 * The cached value of the '{@link #getRooms() <em>Rooms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<IRoom> rooms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected RoomTableImpl() {
		super();
		rooms = new BasicEList<IRoom>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM_TABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IRoom> getRooms() {
		if (rooms == null) {
			rooms = new EObjectResolvingEList<IRoom>(IRoom.class, this, RoomPackage.ROOM_TABLE__ROOMS);
		}
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoom(int roomNumber) {
		if(getRoom(roomNumber)!=null){
			return false;
		}
		Room r = RoomFactory.eINSTANCE.createRoom();
		r.setRoomNbr(roomNumber);
		r.setStatus(RoomStatus.FREE);
		rooms.add(r);

		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoom(int roomNumber) {
		IRoom r = getRoom(roomNumber);
		if(r==null){return false;}
		
		if(getStatus(roomNumber)==RoomStatus.OCCUPIED) {
			return false;
		}
		
		return rooms.remove(r);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> listRooms() {
		EList<Integer> list = new BasicEList<Integer>();
		for(IRoom r:rooms){
			list.add(r.getRoomNbr());
		}
		return list;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> listFreeRoom() {
		EList<Integer> freeRooms = new BasicEList<Integer>();
		for(IRoom r:rooms){
			if(r.getStatus()==RoomStatus.FREE){
				freeRooms.add(r.getRoomNbr());
			}	
		}
		return freeRooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean blockRoom(int roomNumber) {
		IRoom r = getRoom(roomNumber);
		if(r==null){return false;}
		
		if(getStatus(roomNumber)==RoomStatus.OCCUPIED) {
			return false;
		}
		return r.block();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean unblockRoom(int roomNumber) {
		IRoom r = getRoom(roomNumber);
		if(r==null){return false;}
		return r.unblock();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addBill(int roomNumber, IBill bill) {
		IRoom room = getRoom(roomNumber);
		if (room == null) return false;
		room.addBill(bill);
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean occupyRoom(int roomNumber) {
		IRoom r = getRoom(roomNumber);
		if(r!=null && r.getStatus()==RoomStatus.FREE){
			r.occupy();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean freeRoom(int roomNumber) {
		IRoom r = getRoom(roomNumber);
		if(r!=null && r.getStatus()==RoomStatus.OCCUPIED){
			r.free();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isValid(int roomNumber) {
		return getRoom(roomNumber)!=null;	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean reset() {
		this.rooms = new BasicEList<IRoom>();
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public RoomStatus getStatus(int roomNumber) {
		return getRoom(roomNumber).getStatus();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ITab getTab(int roomNumber) {
		return getRoom(roomNumber).getTab();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomPackage.ROOM_TABLE__ROOMS:
				return getRooms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomPackage.ROOM_TABLE__ROOMS:
				getRooms().clear();
				getRooms().addAll((Collection<? extends IRoom>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_TABLE__ROOMS:
				getRooms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_TABLE__ROOMS:
				return rooms != null && !rooms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomPackage.ROOM_TABLE___ADD_ROOM__INT:
				return addRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_TABLE___REMOVE_ROOM__INT:
				return removeRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_TABLE___LIST_ROOMS:
				return listRooms();
			case RoomPackage.ROOM_TABLE___LIST_FREE_ROOM:
				return listFreeRoom();
			case RoomPackage.ROOM_TABLE___BLOCK_ROOM__INT:
				return blockRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_TABLE___UNBLOCK_ROOM__INT:
				return unblockRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_TABLE___ADD_BILL__INT_IBILL:
				return addBill((Integer)arguments.get(0), (IBill)arguments.get(1));
			case RoomPackage.ROOM_TABLE___OCCUPY_ROOM__INT:
				return occupyRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_TABLE___FREE_ROOM__INT:
				return freeRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_TABLE___IS_VALID__INT:
				return isValid((Integer)arguments.get(0));
			case RoomPackage.ROOM_TABLE___RESET:
				return reset();
			case RoomPackage.ROOM_TABLE___GET_STATUS__INT:
				return getStatus((Integer)arguments.get(0));
			case RoomPackage.ROOM_TABLE___GET_TAB__INT:
				return getTab((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}
	
	private IRoom getRoom(int roomID){
		for(IRoom r : rooms){
			if(r.getRoomNbr() == roomID){
				return r;
			}
		}
		return null;
	}

} //RoomTableImpl
