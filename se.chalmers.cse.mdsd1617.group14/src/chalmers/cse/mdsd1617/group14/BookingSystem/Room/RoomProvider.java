/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Room;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomPackage#getRoomProvider()
 * @model
 * @generated
 */
public interface RoomProvider extends IRoomProvider {
} // RoomProvider
