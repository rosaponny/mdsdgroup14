/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomPackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomStatus;
import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RoomProviderImpl extends MinimalEObjectImpl.Container implements RoomProvider {
	
	IRoomTable iroomtable;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected RoomProviderImpl() {
		super();
		iroomtable = chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomFactory.eINSTANCE.createRoomTable();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM_PROVIDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> listFreeRooms() {
		return iroomtable.listFreeRoom();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> listRooms() {
		return iroomtable.listRooms();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean destroyRoom(int roomNumber) {
		if (!isValid(roomNumber)) return false;
		return iroomtable.removeRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean createRoom(int roomNumber) {
		if (isValid(roomNumber)) return false;
		return iroomtable.addRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean blockRoom(int roomNumber) {
		if (!isValid(roomNumber)) return false;
		return iroomtable.blockRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean unblockRoom(int roomNumber) {
		if (!isValid(roomNumber)) return false;
		return iroomtable.unblockRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addBill(int roomNumber, IBill bill) {
		if (!isValid(roomNumber)) return false;
		return iroomtable.addBill(roomNumber, bill);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean occupyRoom(int roomNumber) {
		if (!isValid(roomNumber)) return false;
		return iroomtable.occupyRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean freeRoom(int roomNumber) {
		if (!isValid(roomNumber)) return false;
		return iroomtable.freeRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isValid(int roomNumber) {
		return iroomtable.isValid(roomNumber);	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean reset() {
		this.iroomtable.reset();
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public RoomStatus getStatus(int roomNumber) {
		return iroomtable.getStatus(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ITab getTab(int roomNumber) {
		return iroomtable.getTab(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomPackage.ROOM_PROVIDER___LIST_FREE_ROOMS:
				return listFreeRooms();
			case RoomPackage.ROOM_PROVIDER___LIST_ROOMS:
				return listRooms();
			case RoomPackage.ROOM_PROVIDER___DESTROY_ROOM__INT:
				return destroyRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_PROVIDER___CREATE_ROOM__INT:
				return createRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_PROVIDER___BLOCK_ROOM__INT:
				return blockRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_PROVIDER___UNBLOCK_ROOM__INT:
				return unblockRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_PROVIDER___ADD_BILL__INT_IBILL:
				return addBill((Integer)arguments.get(0), (IBill)arguments.get(1));
			case RoomPackage.ROOM_PROVIDER___OCCUPY_ROOM__INT:
				return occupyRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_PROVIDER___FREE_ROOM__INT:
				return freeRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_PROVIDER___IS_VALID__INT:
				return isValid((Integer)arguments.get(0));
			case RoomPackage.ROOM_PROVIDER___RESET:
				return reset();
			case RoomPackage.ROOM_PROVIDER___GET_STATUS__INT:
				return getStatus((Integer)arguments.get(0));
			case RoomPackage.ROOM_PROVIDER___GET_TAB__INT:
				return getTab((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomProviderImpl
