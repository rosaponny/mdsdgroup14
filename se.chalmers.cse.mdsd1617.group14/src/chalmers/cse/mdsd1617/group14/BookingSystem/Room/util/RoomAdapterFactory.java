/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Room.util;

import chalmers.cse.mdsd1617.group14.BookingSystem.Room.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomPackage
 * @generated
 */
public class RoomAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static RoomPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = RoomPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomSwitch<Adapter> modelSwitch =
		new RoomSwitch<Adapter>() {
			@Override
			public Adapter caseRoomProvider(RoomProvider object) {
				return createRoomProviderAdapter();
			}
			@Override
			public Adapter caseIRoom(IRoom object) {
				return createIRoomAdapter();
			}
			@Override
			public Adapter caseRoom(Room object) {
				return createRoomAdapter();
			}
			@Override
			public Adapter caseIRoomTable(IRoomTable object) {
				return createIRoomTableAdapter();
			}
			@Override
			public Adapter caseRoomTable(RoomTable object) {
				return createRoomTableAdapter();
			}
			@Override
			public Adapter caseIRoomProvider(IRoomProvider object) {
				return createIRoomProviderAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomProvider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomProvider
	 * @generated
	 */
	public Adapter createRoomProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider <em>IRoom Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider
	 * @generated
	 */
	public Adapter createIRoomProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom <em>IRoom</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom
	 * @generated
	 */
	public Adapter createIRoomAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room
	 * @generated
	 */
	public Adapter createRoomAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable <em>IRoom Table</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable
	 * @generated
	 */
	public Adapter createIRoomTableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomTable
	 * @generated
	 */
	public Adapter createRoomTableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //RoomAdapterFactory
