/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Room;

import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room#getStatus <em>Status</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room#getRoomNbr <em>Room Nbr</em>}</li>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room#getTab <em>Tab</em>}</li>
 * </ul>
 *
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomPackage#getRoom()
 * @model
 * @generated
 */
public interface Room extends IRoom {
	/**
	 * Returns the value of the '<em><b>Status</b></em>' attribute.
	 * The literals are from the enumeration {@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomStatus}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Status</em>' attribute.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomStatus
	 * @see #setStatus(RoomStatus)
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomPackage#getRoom_Status()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomStatus getStatus();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room#getStatus <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Status</em>' attribute.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomStatus
	 * @see #getStatus()
	 * @generated
	 */
	void setStatus(RoomStatus value);

	/**
	 * Returns the value of the '<em><b>Room Nbr</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Nbr</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Nbr</em>' attribute.
	 * @see #setRoomNbr(int)
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomPackage#getRoom_RoomNbr()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getRoomNbr();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room#getRoomNbr <em>Room Nbr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Nbr</em>' attribute.
	 * @see #getRoomNbr()
	 * @generated
	 */
	void setRoomNbr(int value);

	/**
	 * Returns the value of the '<em><b>Tab</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tab</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tab</em>' reference.
	 * @see #setTab(ITab)
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomPackage#getRoom_Tab()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ITab getTab();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room#getTab <em>Tab</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tab</em>' reference.
	 * @see #getTab()
	 * @generated
	 */
	void setTab(ITab value);

} // Room
