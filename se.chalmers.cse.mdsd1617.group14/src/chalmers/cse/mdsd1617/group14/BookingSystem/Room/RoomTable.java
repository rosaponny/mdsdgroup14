/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Room;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomTable#getRooms <em>Rooms</em>}</li>
 * </ul>
 *
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomPackage#getRoomTable()
 * @model
 * @generated
 */
public interface RoomTable extends IRoomTable {
	/**
	 * Returns the value of the '<em><b>Rooms</b></em>' reference list.
	 * The list contents are of type {@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rooms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rooms</em>' reference list.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomPackage#getRoomTable_Rooms()
	 * @model ordered="false"
	 * @generated
	 */
	EList<IRoom> getRooms();

} // RoomTable
