/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.EconomicsPackage;

import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.EconomicsPackageImpl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomPackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomStatus;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomTable;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypePackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl;
import chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage;
import chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.types.impl.TypesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RoomPackageImpl extends EPackageImpl implements RoomPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomTableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomTableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum roomStatusEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RoomPackageImpl() {
		super(eNS_URI, RoomFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link RoomPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RoomPackage init() {
		if (isInited) return (RoomPackage)EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI);

		// Obtain or create and register package
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new RoomPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		SystemAccessPackageImpl theSystemAccessPackage = (SystemAccessPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SystemAccessPackage.eNS_URI) instanceof SystemAccessPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SystemAccessPackage.eNS_URI) : SystemAccessPackage.eINSTANCE);
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) : BookingPackage.eINSTANCE);
		EconomicsPackageImpl theEconomicsPackage = (EconomicsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(EconomicsPackage.eNS_URI) instanceof EconomicsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(EconomicsPackage.eNS_URI) : EconomicsPackage.eINSTANCE);
		RoomTypePackageImpl theRoomTypePackage = (RoomTypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI) instanceof RoomTypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI) : RoomTypePackage.eINSTANCE);
		TypesPackageImpl theTypesPackage = (TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) : TypesPackage.eINSTANCE);

		// Create package meta-data objects
		theRoomPackage.createPackageContents();
		theSystemAccessPackage.createPackageContents();
		theBookingPackage.createPackageContents();
		theEconomicsPackage.createPackageContents();
		theRoomTypePackage.createPackageContents();
		theTypesPackage.createPackageContents();

		// Initialize created meta-data
		theRoomPackage.initializePackageContents();
		theSystemAccessPackage.initializePackageContents();
		theBookingPackage.initializePackageContents();
		theEconomicsPackage.initializePackageContents();
		theRoomTypePackage.initializePackageContents();
		theTypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRoomPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RoomPackage.eNS_URI, theRoomPackage);
		return theRoomPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomProvider() {
		return roomProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomProvider() {
		return iRoomProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomProvider__ListFreeRooms() {
		return iRoomProviderEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomProvider__ListRooms() {
		return iRoomProviderEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomProvider__DestroyRoom__int() {
		return iRoomProviderEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomProvider__CreateRoom__int() {
		return iRoomProviderEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomProvider__BlockRoom__int() {
		return iRoomProviderEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomProvider__UnblockRoom__int() {
		return iRoomProviderEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomProvider__AddBill__int_IBill() {
		return iRoomProviderEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomProvider__OccupyRoom__int() {
		return iRoomProviderEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomProvider__FreeRoom__int() {
		return iRoomProviderEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomProvider__IsValid__int() {
		return iRoomProviderEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomProvider__Reset() {
		return iRoomProviderEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomProvider__GetStatus__int() {
		return iRoomProviderEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomProvider__GetTab__int() {
		return iRoomProviderEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoom() {
		return iRoomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoom__Block() {
		return iRoomEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoom__Unblock() {
		return iRoomEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoom__Occupy() {
		return iRoomEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoom__Free() {
		return iRoomEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoom__GetRoomNbr() {
		return iRoomEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoom__GetStatus() {
		return iRoomEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoom__GetTab() {
		return iRoomEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoom__AddBill__IBill() {
		return iRoomEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoom() {
		return roomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoom_Status() {
		return (EAttribute)roomEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoom_RoomNbr() {
		return (EAttribute)roomEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoom_Tab() {
		return (EReference)roomEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomTable() {
		return iRoomTableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTable__AddRoom__int() {
		return iRoomTableEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTable__RemoveRoom__int() {
		return iRoomTableEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTable__ListRooms() {
		return iRoomTableEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTable__ListFreeRoom() {
		return iRoomTableEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTable__BlockRoom__int() {
		return iRoomTableEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTable__UnblockRoom__int() {
		return iRoomTableEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTable__AddBill__int_IBill() {
		return iRoomTableEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTable__OccupyRoom__int() {
		return iRoomTableEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTable__FreeRoom__int() {
		return iRoomTableEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTable__IsValid__int() {
		return iRoomTableEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTable__Reset() {
		return iRoomTableEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTable__GetStatus__int() {
		return iRoomTableEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTable__GetTab__int() {
		return iRoomTableEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomTable() {
		return roomTableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomTable_Rooms() {
		return (EReference)roomTableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRoomStatus() {
		return roomStatusEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomFactory getRoomFactory() {
		return (RoomFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		roomProviderEClass = createEClass(ROOM_PROVIDER);

		iRoomEClass = createEClass(IROOM);
		createEOperation(iRoomEClass, IROOM___BLOCK);
		createEOperation(iRoomEClass, IROOM___UNBLOCK);
		createEOperation(iRoomEClass, IROOM___OCCUPY);
		createEOperation(iRoomEClass, IROOM___FREE);
		createEOperation(iRoomEClass, IROOM___GET_ROOM_NBR);
		createEOperation(iRoomEClass, IROOM___GET_STATUS);
		createEOperation(iRoomEClass, IROOM___GET_TAB);
		createEOperation(iRoomEClass, IROOM___ADD_BILL__IBILL);

		roomEClass = createEClass(ROOM);
		createEAttribute(roomEClass, ROOM__STATUS);
		createEAttribute(roomEClass, ROOM__ROOM_NBR);
		createEReference(roomEClass, ROOM__TAB);

		iRoomTableEClass = createEClass(IROOM_TABLE);
		createEOperation(iRoomTableEClass, IROOM_TABLE___ADD_ROOM__INT);
		createEOperation(iRoomTableEClass, IROOM_TABLE___REMOVE_ROOM__INT);
		createEOperation(iRoomTableEClass, IROOM_TABLE___LIST_ROOMS);
		createEOperation(iRoomTableEClass, IROOM_TABLE___LIST_FREE_ROOM);
		createEOperation(iRoomTableEClass, IROOM_TABLE___BLOCK_ROOM__INT);
		createEOperation(iRoomTableEClass, IROOM_TABLE___UNBLOCK_ROOM__INT);
		createEOperation(iRoomTableEClass, IROOM_TABLE___ADD_BILL__INT_IBILL);
		createEOperation(iRoomTableEClass, IROOM_TABLE___OCCUPY_ROOM__INT);
		createEOperation(iRoomTableEClass, IROOM_TABLE___FREE_ROOM__INT);
		createEOperation(iRoomTableEClass, IROOM_TABLE___IS_VALID__INT);
		createEOperation(iRoomTableEClass, IROOM_TABLE___RESET);
		createEOperation(iRoomTableEClass, IROOM_TABLE___GET_STATUS__INT);
		createEOperation(iRoomTableEClass, IROOM_TABLE___GET_TAB__INT);

		roomTableEClass = createEClass(ROOM_TABLE);
		createEReference(roomTableEClass, ROOM_TABLE__ROOMS);

		iRoomProviderEClass = createEClass(IROOM_PROVIDER);
		createEOperation(iRoomProviderEClass, IROOM_PROVIDER___LIST_FREE_ROOMS);
		createEOperation(iRoomProviderEClass, IROOM_PROVIDER___LIST_ROOMS);
		createEOperation(iRoomProviderEClass, IROOM_PROVIDER___DESTROY_ROOM__INT);
		createEOperation(iRoomProviderEClass, IROOM_PROVIDER___CREATE_ROOM__INT);
		createEOperation(iRoomProviderEClass, IROOM_PROVIDER___BLOCK_ROOM__INT);
		createEOperation(iRoomProviderEClass, IROOM_PROVIDER___UNBLOCK_ROOM__INT);
		createEOperation(iRoomProviderEClass, IROOM_PROVIDER___ADD_BILL__INT_IBILL);
		createEOperation(iRoomProviderEClass, IROOM_PROVIDER___OCCUPY_ROOM__INT);
		createEOperation(iRoomProviderEClass, IROOM_PROVIDER___FREE_ROOM__INT);
		createEOperation(iRoomProviderEClass, IROOM_PROVIDER___IS_VALID__INT);
		createEOperation(iRoomProviderEClass, IROOM_PROVIDER___RESET);
		createEOperation(iRoomProviderEClass, IROOM_PROVIDER___GET_STATUS__INT);
		createEOperation(iRoomProviderEClass, IROOM_PROVIDER___GET_TAB__INT);

		// Create enums
		roomStatusEEnum = createEEnum(ROOM_STATUS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EconomicsPackage theEconomicsPackage = (EconomicsPackage)EPackage.Registry.INSTANCE.getEPackage(EconomicsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		roomProviderEClass.getESuperTypes().add(this.getIRoomProvider());
		roomEClass.getESuperTypes().add(this.getIRoom());
		roomTableEClass.getESuperTypes().add(this.getIRoomTable());

		// Initialize classes, features, and operations; add parameters
		initEClass(roomProviderEClass, RoomProvider.class, "RoomProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(iRoomEClass, IRoom.class, "IRoom", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getIRoom__Block(), ecorePackage.getEBoolean(), "block", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoom__Unblock(), ecorePackage.getEBoolean(), "unblock", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoom__Occupy(), null, "occupy", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoom__Free(), null, "free", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoom__GetRoomNbr(), ecorePackage.getEInt(), "getRoomNbr", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoom__GetStatus(), this.getRoomStatus(), "getStatus", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoom__GetTab(), theEconomicsPackage.getITab(), "getTab", 1, 1, IS_UNIQUE, !IS_ORDERED);

		EOperation op = initEOperation(getIRoom__AddBill__IBill(), ecorePackage.getEBoolean(), "addBill", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theEconomicsPackage.getIBill(), "bill", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(roomEClass, Room.class, "Room", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRoom_Status(), this.getRoomStatus(), "status", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoom_RoomNbr(), ecorePackage.getEInt(), "roomNbr", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRoom_Tab(), theEconomicsPackage.getITab(), null, "tab", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iRoomTableEClass, IRoomTable.class, "IRoomTable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIRoomTable__AddRoom__int(), ecorePackage.getEBoolean(), "addRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTable__RemoveRoom__int(), ecorePackage.getEBoolean(), "removeRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomTable__ListRooms(), ecorePackage.getEInt(), "listRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomTable__ListFreeRoom(), ecorePackage.getEInt(), "listFreeRoom", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTable__BlockRoom__int(), ecorePackage.getEBoolean(), "blockRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTable__UnblockRoom__int(), ecorePackage.getEBoolean(), "unblockRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTable__AddBill__int_IBill(), ecorePackage.getEBoolean(), "addBill", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theEconomicsPackage.getIBill(), "bill", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTable__OccupyRoom__int(), ecorePackage.getEBoolean(), "occupyRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTable__FreeRoom__int(), ecorePackage.getEBoolean(), "freeRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTable__IsValid__int(), ecorePackage.getEBoolean(), "isValid", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomTable__Reset(), ecorePackage.getEBoolean(), "reset", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTable__GetStatus__int(), this.getRoomStatus(), "getStatus", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTable__GetTab__int(), theEconomicsPackage.getITab(), "getTab", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(roomTableEClass, RoomTable.class, "RoomTable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoomTable_Rooms(), this.getIRoom(), null, "rooms", null, 0, -1, RoomTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iRoomProviderEClass, IRoomProvider.class, "IRoomProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getIRoomProvider__ListFreeRooms(), ecorePackage.getEInt(), "listFreeRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomProvider__ListRooms(), ecorePackage.getEInt(), "listRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomProvider__DestroyRoom__int(), ecorePackage.getEBoolean(), "destroyRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomProvider__CreateRoom__int(), ecorePackage.getEBoolean(), "createRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomProvider__BlockRoom__int(), ecorePackage.getEBoolean(), "blockRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomProvider__UnblockRoom__int(), ecorePackage.getEBoolean(), "unblockRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomProvider__AddBill__int_IBill(), ecorePackage.getEBoolean(), "addBill", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theEconomicsPackage.getIBill(), "bill", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomProvider__OccupyRoom__int(), ecorePackage.getEBoolean(), "occupyRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomProvider__FreeRoom__int(), ecorePackage.getEBoolean(), "freeRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomProvider__IsValid__int(), ecorePackage.getEBoolean(), "isValid", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomProvider__Reset(), ecorePackage.getEBoolean(), "reset", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomProvider__GetStatus__int(), this.getRoomStatus(), "getStatus", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomProvider__GetTab__int(), theEconomicsPackage.getITab(), "getTab", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(roomStatusEEnum, RoomStatus.class, "RoomStatus");
		addEEnumLiteral(roomStatusEEnum, RoomStatus.FREE);
		addEEnumLiteral(roomStatusEEnum, RoomStatus.OCCUPIED);
		addEEnumLiteral(roomStatusEEnum, RoomStatus.BLOCKED);

		// Create resource
		createResource(eNS_URI);
	}

} //RoomPackageImpl
