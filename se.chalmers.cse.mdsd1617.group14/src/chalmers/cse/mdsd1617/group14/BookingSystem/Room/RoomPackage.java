/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Room;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomFactory
 * @model kind="package"
 * @generated
 */
public interface RoomPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Room";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///chalmers/cse/mdsd1617/group14/BookingSystem/Room.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "chalmers.cse.mdsd1617.group14.BookingSystem.Room";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RoomPackage eINSTANCE = chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl.init();

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomProviderImpl <em>Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomProviderImpl
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl#getRoomProvider()
	 * @generated
	 */
	int ROOM_PROVIDER = 0;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider <em>IRoom Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl#getIRoomProvider()
	 * @generated
	 */
	int IROOM_PROVIDER = 5;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom <em>IRoom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl#getIRoom()
	 * @generated
	 */
	int IROOM = 1;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomImpl <em>Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomImpl
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 2;

	/**
	 * The number of structural features of the '<em>IRoom Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_PROVIDER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>List Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_PROVIDER___LIST_FREE_ROOMS = 0;

	/**
	 * The operation id for the '<em>List Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_PROVIDER___LIST_ROOMS = 1;

	/**
	 * The operation id for the '<em>Destroy Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_PROVIDER___DESTROY_ROOM__INT = 2;

	/**
	 * The operation id for the '<em>Create Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_PROVIDER___CREATE_ROOM__INT = 3;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_PROVIDER___BLOCK_ROOM__INT = 4;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_PROVIDER___UNBLOCK_ROOM__INT = 5;

	/**
	 * The operation id for the '<em>Add Bill</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_PROVIDER___ADD_BILL__INT_IBILL = 6;

	/**
	 * The operation id for the '<em>Occupy Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_PROVIDER___OCCUPY_ROOM__INT = 7;

	/**
	 * The operation id for the '<em>Free Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_PROVIDER___FREE_ROOM__INT = 8;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_PROVIDER___IS_VALID__INT = 9;

	/**
	 * The operation id for the '<em>Reset</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_PROVIDER___RESET = 10;

	/**
	 * The operation id for the '<em>Get Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_PROVIDER___GET_STATUS__INT = 11;

	/**
	 * The operation id for the '<em>Get Tab</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_PROVIDER___GET_TAB__INT = 12;

	/**
	 * The number of operations of the '<em>IRoom Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_PROVIDER_OPERATION_COUNT = 13;

	/**
	 * The number of structural features of the '<em>Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_PROVIDER_FEATURE_COUNT = IROOM_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>List Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_PROVIDER___LIST_FREE_ROOMS = IROOM_PROVIDER___LIST_FREE_ROOMS;

	/**
	 * The operation id for the '<em>List Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_PROVIDER___LIST_ROOMS = IROOM_PROVIDER___LIST_ROOMS;

	/**
	 * The operation id for the '<em>Destroy Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_PROVIDER___DESTROY_ROOM__INT = IROOM_PROVIDER___DESTROY_ROOM__INT;

	/**
	 * The operation id for the '<em>Create Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_PROVIDER___CREATE_ROOM__INT = IROOM_PROVIDER___CREATE_ROOM__INT;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_PROVIDER___BLOCK_ROOM__INT = IROOM_PROVIDER___BLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_PROVIDER___UNBLOCK_ROOM__INT = IROOM_PROVIDER___UNBLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Add Bill</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_PROVIDER___ADD_BILL__INT_IBILL = IROOM_PROVIDER___ADD_BILL__INT_IBILL;

	/**
	 * The operation id for the '<em>Occupy Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_PROVIDER___OCCUPY_ROOM__INT = IROOM_PROVIDER___OCCUPY_ROOM__INT;

	/**
	 * The operation id for the '<em>Free Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_PROVIDER___FREE_ROOM__INT = IROOM_PROVIDER___FREE_ROOM__INT;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_PROVIDER___IS_VALID__INT = IROOM_PROVIDER___IS_VALID__INT;

	/**
	 * The operation id for the '<em>Reset</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_PROVIDER___RESET = IROOM_PROVIDER___RESET;

	/**
	 * The operation id for the '<em>Get Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_PROVIDER___GET_STATUS__INT = IROOM_PROVIDER___GET_STATUS__INT;

	/**
	 * The operation id for the '<em>Get Tab</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_PROVIDER___GET_TAB__INT = IROOM_PROVIDER___GET_TAB__INT;

	/**
	 * The number of operations of the '<em>Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_PROVIDER_OPERATION_COUNT = IROOM_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IRoom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Block</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___BLOCK = 0;

	/**
	 * The operation id for the '<em>Unblock</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___UNBLOCK = 1;

	/**
	 * The operation id for the '<em>Occupy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___OCCUPY = 2;

	/**
	 * The operation id for the '<em>Free</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___FREE = 3;

	/**
	 * The operation id for the '<em>Get Room Nbr</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___GET_ROOM_NBR = 4;

	/**
	 * The operation id for the '<em>Get Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___GET_STATUS = 5;

	/**
	 * The operation id for the '<em>Get Tab</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___GET_TAB = 6;

	/**
	 * The operation id for the '<em>Add Bill</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___ADD_BILL__IBILL = 7;

	/**
	 * The number of operations of the '<em>IRoom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_OPERATION_COUNT = 8;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__STATUS = IROOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Room Nbr</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ROOM_NBR = IROOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Tab</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__TAB = IROOM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_FEATURE_COUNT = IROOM_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Block</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___BLOCK = IROOM___BLOCK;

	/**
	 * The operation id for the '<em>Unblock</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___UNBLOCK = IROOM___UNBLOCK;

	/**
	 * The operation id for the '<em>Occupy</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___OCCUPY = IROOM___OCCUPY;

	/**
	 * The operation id for the '<em>Free</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___FREE = IROOM___FREE;

	/**
	 * The operation id for the '<em>Get Room Nbr</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___GET_ROOM_NBR = IROOM___GET_ROOM_NBR;

	/**
	 * The operation id for the '<em>Get Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___GET_STATUS = IROOM___GET_STATUS;

	/**
	 * The operation id for the '<em>Get Tab</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___GET_TAB = IROOM___GET_TAB;

	/**
	 * The operation id for the '<em>Add Bill</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___ADD_BILL__IBILL = IROOM___ADD_BILL__IBILL;

	/**
	 * The number of operations of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_OPERATION_COUNT = IROOM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable <em>IRoom Table</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl#getIRoomTable()
	 * @generated
	 */
	int IROOM_TABLE = 3;

	/**
	 * The number of structural features of the '<em>IRoom Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TABLE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TABLE___ADD_ROOM__INT = 0;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TABLE___REMOVE_ROOM__INT = 1;

	/**
	 * The operation id for the '<em>List Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TABLE___LIST_ROOMS = 2;

	/**
	 * The operation id for the '<em>List Free Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TABLE___LIST_FREE_ROOM = 3;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TABLE___BLOCK_ROOM__INT = 4;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TABLE___UNBLOCK_ROOM__INT = 5;

	/**
	 * The operation id for the '<em>Add Bill</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TABLE___ADD_BILL__INT_IBILL = 6;

	/**
	 * The operation id for the '<em>Occupy Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TABLE___OCCUPY_ROOM__INT = 7;

	/**
	 * The operation id for the '<em>Free Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TABLE___FREE_ROOM__INT = 8;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TABLE___IS_VALID__INT = 9;

	/**
	 * The operation id for the '<em>Reset</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TABLE___RESET = 10;

	/**
	 * The operation id for the '<em>Get Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TABLE___GET_STATUS__INT = 11;

	/**
	 * The operation id for the '<em>Get Tab</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TABLE___GET_TAB__INT = 12;

	/**
	 * The number of operations of the '<em>IRoom Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TABLE_OPERATION_COUNT = 13;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomTableImpl <em>Table</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomTableImpl
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl#getRoomTable()
	 * @generated
	 */
	int ROOM_TABLE = 4;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TABLE__ROOMS = IROOM_TABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TABLE_FEATURE_COUNT = IROOM_TABLE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TABLE___ADD_ROOM__INT = IROOM_TABLE___ADD_ROOM__INT;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TABLE___REMOVE_ROOM__INT = IROOM_TABLE___REMOVE_ROOM__INT;

	/**
	 * The operation id for the '<em>List Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TABLE___LIST_ROOMS = IROOM_TABLE___LIST_ROOMS;

	/**
	 * The operation id for the '<em>List Free Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TABLE___LIST_FREE_ROOM = IROOM_TABLE___LIST_FREE_ROOM;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TABLE___BLOCK_ROOM__INT = IROOM_TABLE___BLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TABLE___UNBLOCK_ROOM__INT = IROOM_TABLE___UNBLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Add Bill</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TABLE___ADD_BILL__INT_IBILL = IROOM_TABLE___ADD_BILL__INT_IBILL;

	/**
	 * The operation id for the '<em>Occupy Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TABLE___OCCUPY_ROOM__INT = IROOM_TABLE___OCCUPY_ROOM__INT;

	/**
	 * The operation id for the '<em>Free Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TABLE___FREE_ROOM__INT = IROOM_TABLE___FREE_ROOM__INT;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TABLE___IS_VALID__INT = IROOM_TABLE___IS_VALID__INT;

	/**
	 * The operation id for the '<em>Reset</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TABLE___RESET = IROOM_TABLE___RESET;

	/**
	 * The operation id for the '<em>Get Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TABLE___GET_STATUS__INT = IROOM_TABLE___GET_STATUS__INT;

	/**
	 * The operation id for the '<em>Get Tab</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TABLE___GET_TAB__INT = IROOM_TABLE___GET_TAB__INT;

	/**
	 * The number of operations of the '<em>Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TABLE_OPERATION_COUNT = IROOM_TABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomStatus <em>Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomStatus
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl#getRoomStatus()
	 * @generated
	 */
	int ROOM_STATUS = 6;


	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomProvider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provider</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomProvider
	 * @generated
	 */
	EClass getRoomProvider();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider <em>IRoom Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Provider</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider
	 * @generated
	 */
	EClass getIRoomProvider();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#listFreeRooms() <em>List Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Free Rooms</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#listFreeRooms()
	 * @generated
	 */
	EOperation getIRoomProvider__ListFreeRooms();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#listRooms() <em>List Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Rooms</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#listRooms()
	 * @generated
	 */
	EOperation getIRoomProvider__ListRooms();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#destroyRoom(int) <em>Destroy Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Destroy Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#destroyRoom(int)
	 * @generated
	 */
	EOperation getIRoomProvider__DestroyRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#createRoom(int) <em>Create Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#createRoom(int)
	 * @generated
	 */
	EOperation getIRoomProvider__CreateRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#blockRoom(int) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#blockRoom(int)
	 * @generated
	 */
	EOperation getIRoomProvider__BlockRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#unblockRoom(int) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#unblockRoom(int)
	 * @generated
	 */
	EOperation getIRoomProvider__UnblockRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#addBill(int, chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill) <em>Add Bill</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Bill</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#addBill(int, chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill)
	 * @generated
	 */
	EOperation getIRoomProvider__AddBill__int_IBill();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#occupyRoom(int) <em>Occupy Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Occupy Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#occupyRoom(int)
	 * @generated
	 */
	EOperation getIRoomProvider__OccupyRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#freeRoom(int) <em>Free Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Free Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#freeRoom(int)
	 * @generated
	 */
	EOperation getIRoomProvider__FreeRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#isValid(int) <em>Is Valid</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#isValid(int)
	 * @generated
	 */
	EOperation getIRoomProvider__IsValid__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#reset() <em>Reset</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Reset</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#reset()
	 * @generated
	 */
	EOperation getIRoomProvider__Reset();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#getStatus(int) <em>Get Status</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Status</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#getStatus(int)
	 * @generated
	 */
	EOperation getIRoomProvider__GetStatus__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#getTab(int) <em>Get Tab</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Tab</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider#getTab(int)
	 * @generated
	 */
	EOperation getIRoomProvider__GetTab__int();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom <em>IRoom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom
	 * @generated
	 */
	EClass getIRoom();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom#block() <em>Block</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom#block()
	 * @generated
	 */
	EOperation getIRoom__Block();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom#unblock() <em>Unblock</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom#unblock()
	 * @generated
	 */
	EOperation getIRoom__Unblock();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom#occupy() <em>Occupy</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Occupy</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom#occupy()
	 * @generated
	 */
	EOperation getIRoom__Occupy();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom#free() <em>Free</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Free</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom#free()
	 * @generated
	 */
	EOperation getIRoom__Free();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom#getRoomNbr() <em>Get Room Nbr</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Nbr</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom#getRoomNbr()
	 * @generated
	 */
	EOperation getIRoom__GetRoomNbr();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom#getStatus() <em>Get Status</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Status</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom#getStatus()
	 * @generated
	 */
	EOperation getIRoom__GetStatus();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom#getTab() <em>Get Tab</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Tab</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom#getTab()
	 * @generated
	 */
	EOperation getIRoom__GetTab();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom#addBill(chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill) <em>Add Bill</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Bill</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom#addBill(chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill)
	 * @generated
	 */
	EOperation getIRoom__AddBill__IBill();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room
	 * @generated
	 */
	EClass getRoom();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room#getStatus()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_Status();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room#getRoomNbr <em>Room Nbr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Nbr</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room#getRoomNbr()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_RoomNbr();

	/**
	 * Returns the meta object for the reference '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room#getTab <em>Tab</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tab</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.Room#getTab()
	 * @see #getRoom()
	 * @generated
	 */
	EReference getRoom_Tab();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable <em>IRoom Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Table</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable
	 * @generated
	 */
	EClass getIRoomTable();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#addRoom(int) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#addRoom(int)
	 * @generated
	 */
	EOperation getIRoomTable__AddRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#removeRoom(int)
	 * @generated
	 */
	EOperation getIRoomTable__RemoveRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#listRooms() <em>List Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Rooms</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#listRooms()
	 * @generated
	 */
	EOperation getIRoomTable__ListRooms();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#listFreeRoom() <em>List Free Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Free Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#listFreeRoom()
	 * @generated
	 */
	EOperation getIRoomTable__ListFreeRoom();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#blockRoom(int) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#blockRoom(int)
	 * @generated
	 */
	EOperation getIRoomTable__BlockRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#unblockRoom(int) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#unblockRoom(int)
	 * @generated
	 */
	EOperation getIRoomTable__UnblockRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#addBill(int, chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill) <em>Add Bill</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Bill</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#addBill(int, chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill)
	 * @generated
	 */
	EOperation getIRoomTable__AddBill__int_IBill();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#occupyRoom(int) <em>Occupy Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Occupy Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#occupyRoom(int)
	 * @generated
	 */
	EOperation getIRoomTable__OccupyRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#freeRoom(int) <em>Free Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Free Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#freeRoom(int)
	 * @generated
	 */
	EOperation getIRoomTable__FreeRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#isValid(int) <em>Is Valid</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#isValid(int)
	 * @generated
	 */
	EOperation getIRoomTable__IsValid__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#reset() <em>Reset</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Reset</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#reset()
	 * @generated
	 */
	EOperation getIRoomTable__Reset();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#getStatus(int) <em>Get Status</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Status</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#getStatus(int)
	 * @generated
	 */
	EOperation getIRoomTable__GetStatus__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#getTab(int) <em>Get Tab</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Tab</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable#getTab(int)
	 * @generated
	 */
	EOperation getIRoomTable__GetTab__int();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomTable
	 * @generated
	 */
	EClass getRoomTable();

	/**
	 * Returns the meta object for the reference list '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomTable#getRooms <em>Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Rooms</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomTable#getRooms()
	 * @see #getRoomTable()
	 * @generated
	 */
	EReference getRoomTable_Rooms();

	/**
	 * Returns the meta object for enum '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Status</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomStatus
	 * @generated
	 */
	EEnum getRoomStatus();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RoomFactory getRoomFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomProviderImpl <em>Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomProviderImpl
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl#getRoomProvider()
		 * @generated
		 */
		EClass ROOM_PROVIDER = eINSTANCE.getRoomProvider();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider <em>IRoom Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl#getIRoomProvider()
		 * @generated
		 */
		EClass IROOM_PROVIDER = eINSTANCE.getIRoomProvider();

		/**
		 * The meta object literal for the '<em><b>List Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_PROVIDER___LIST_FREE_ROOMS = eINSTANCE.getIRoomProvider__ListFreeRooms();

		/**
		 * The meta object literal for the '<em><b>List Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_PROVIDER___LIST_ROOMS = eINSTANCE.getIRoomProvider__ListRooms();

		/**
		 * The meta object literal for the '<em><b>Destroy Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_PROVIDER___DESTROY_ROOM__INT = eINSTANCE.getIRoomProvider__DestroyRoom__int();

		/**
		 * The meta object literal for the '<em><b>Create Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_PROVIDER___CREATE_ROOM__INT = eINSTANCE.getIRoomProvider__CreateRoom__int();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_PROVIDER___BLOCK_ROOM__INT = eINSTANCE.getIRoomProvider__BlockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_PROVIDER___UNBLOCK_ROOM__INT = eINSTANCE.getIRoomProvider__UnblockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Add Bill</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_PROVIDER___ADD_BILL__INT_IBILL = eINSTANCE.getIRoomProvider__AddBill__int_IBill();

		/**
		 * The meta object literal for the '<em><b>Occupy Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_PROVIDER___OCCUPY_ROOM__INT = eINSTANCE.getIRoomProvider__OccupyRoom__int();

		/**
		 * The meta object literal for the '<em><b>Free Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_PROVIDER___FREE_ROOM__INT = eINSTANCE.getIRoomProvider__FreeRoom__int();

		/**
		 * The meta object literal for the '<em><b>Is Valid</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_PROVIDER___IS_VALID__INT = eINSTANCE.getIRoomProvider__IsValid__int();

		/**
		 * The meta object literal for the '<em><b>Reset</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_PROVIDER___RESET = eINSTANCE.getIRoomProvider__Reset();

		/**
		 * The meta object literal for the '<em><b>Get Status</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_PROVIDER___GET_STATUS__INT = eINSTANCE.getIRoomProvider__GetStatus__int();

		/**
		 * The meta object literal for the '<em><b>Get Tab</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_PROVIDER___GET_TAB__INT = eINSTANCE.getIRoomProvider__GetTab__int();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom <em>IRoom</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoom
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl#getIRoom()
		 * @generated
		 */
		EClass IROOM = eINSTANCE.getIRoom();

		/**
		 * The meta object literal for the '<em><b>Block</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___BLOCK = eINSTANCE.getIRoom__Block();

		/**
		 * The meta object literal for the '<em><b>Unblock</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___UNBLOCK = eINSTANCE.getIRoom__Unblock();

		/**
		 * The meta object literal for the '<em><b>Occupy</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___OCCUPY = eINSTANCE.getIRoom__Occupy();

		/**
		 * The meta object literal for the '<em><b>Free</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___FREE = eINSTANCE.getIRoom__Free();

		/**
		 * The meta object literal for the '<em><b>Get Room Nbr</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___GET_ROOM_NBR = eINSTANCE.getIRoom__GetRoomNbr();

		/**
		 * The meta object literal for the '<em><b>Get Status</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___GET_STATUS = eINSTANCE.getIRoom__GetStatus();

		/**
		 * The meta object literal for the '<em><b>Get Tab</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___GET_TAB = eINSTANCE.getIRoom__GetTab();

		/**
		 * The meta object literal for the '<em><b>Add Bill</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___ADD_BILL__IBILL = eINSTANCE.getIRoom__AddBill__IBill();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomImpl <em>Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomImpl
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl#getRoom()
		 * @generated
		 */
		EClass ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__STATUS = eINSTANCE.getRoom_Status();

		/**
		 * The meta object literal for the '<em><b>Room Nbr</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__ROOM_NBR = eINSTANCE.getRoom_RoomNbr();

		/**
		 * The meta object literal for the '<em><b>Tab</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM__TAB = eINSTANCE.getRoom_Tab();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable <em>IRoom Table</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomTable
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl#getIRoomTable()
		 * @generated
		 */
		EClass IROOM_TABLE = eINSTANCE.getIRoomTable();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TABLE___ADD_ROOM__INT = eINSTANCE.getIRoomTable__AddRoom__int();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TABLE___REMOVE_ROOM__INT = eINSTANCE.getIRoomTable__RemoveRoom__int();

		/**
		 * The meta object literal for the '<em><b>List Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TABLE___LIST_ROOMS = eINSTANCE.getIRoomTable__ListRooms();

		/**
		 * The meta object literal for the '<em><b>List Free Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TABLE___LIST_FREE_ROOM = eINSTANCE.getIRoomTable__ListFreeRoom();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TABLE___BLOCK_ROOM__INT = eINSTANCE.getIRoomTable__BlockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TABLE___UNBLOCK_ROOM__INT = eINSTANCE.getIRoomTable__UnblockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Add Bill</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TABLE___ADD_BILL__INT_IBILL = eINSTANCE.getIRoomTable__AddBill__int_IBill();

		/**
		 * The meta object literal for the '<em><b>Occupy Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TABLE___OCCUPY_ROOM__INT = eINSTANCE.getIRoomTable__OccupyRoom__int();

		/**
		 * The meta object literal for the '<em><b>Free Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TABLE___FREE_ROOM__INT = eINSTANCE.getIRoomTable__FreeRoom__int();

		/**
		 * The meta object literal for the '<em><b>Is Valid</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TABLE___IS_VALID__INT = eINSTANCE.getIRoomTable__IsValid__int();

		/**
		 * The meta object literal for the '<em><b>Reset</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TABLE___RESET = eINSTANCE.getIRoomTable__Reset();

		/**
		 * The meta object literal for the '<em><b>Get Status</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TABLE___GET_STATUS__INT = eINSTANCE.getIRoomTable__GetStatus__int();

		/**
		 * The meta object literal for the '<em><b>Get Tab</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TABLE___GET_TAB__INT = eINSTANCE.getIRoomTable__GetTab__int();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomTableImpl <em>Table</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomTableImpl
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl#getRoomTable()
		 * @generated
		 */
		EClass ROOM_TABLE = eINSTANCE.getRoomTable();

		/**
		 * The meta object literal for the '<em><b>Rooms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_TABLE__ROOMS = eINSTANCE.getRoomTable_Rooms();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomStatus <em>Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomStatus
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl#getRoomStatus()
		 * @generated
		 */
		EEnum ROOM_STATUS = eINSTANCE.getRoomStatus();

	}

} //RoomPackage
