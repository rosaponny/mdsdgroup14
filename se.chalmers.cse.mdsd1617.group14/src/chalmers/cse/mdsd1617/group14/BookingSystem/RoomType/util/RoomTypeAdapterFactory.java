/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.util;

import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypePackage
 * @generated
 */
public class RoomTypeAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static RoomTypePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = RoomTypePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomTypeSwitch<Adapter> modelSwitch =
		new RoomTypeSwitch<Adapter>() {
			@Override
			public Adapter caseIRoomTypeProvider(IRoomTypeProvider object) {
				return createIRoomTypeProviderAdapter();
			}
			@Override
			public Adapter caseFeature(Feature object) {
				return createFeatureAdapter();
			}
			@Override
			public Adapter caseRoomType(RoomType object) {
				return createRoomTypeAdapter();
			}
			@Override
			public Adapter caseIRoomType(IRoomType object) {
				return createIRoomTypeAdapter();
			}
			@Override
			public Adapter caseIRoomTypeTable(IRoomTypeTable object) {
				return createIRoomTypeTableAdapter();
			}
			@Override
			public Adapter caseRoomTypeTable(RoomTypeTable object) {
				return createRoomTypeTableAdapter();
			}
			@Override
			public Adapter caseRoomTypeProvider(RoomTypeProvider object) {
				return createRoomTypeProviderAdapter();
			}
			@Override
			public Adapter caseRoomTypeComparator(RoomTypeComparator object) {
				return createRoomTypeComparatorAdapter();
			}
			@Override
			public Adapter caseFreeRoomTypesDTO(FreeRoomTypesDTO object) {
				return createFreeRoomTypesDTOAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider <em>IRoom Type Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider
	 * @generated
	 */
	public Adapter createIRoomTypeProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.Feature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.Feature
	 * @generated
	 */
	public Adapter createFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomType
	 * @generated
	 */
	public Adapter createRoomTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable <em>IRoom Type Table</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable
	 * @generated
	 */
	public Adapter createIRoomTypeTableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeTable
	 * @generated
	 */
	public Adapter createRoomTypeTableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeProvider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeProvider
	 * @generated
	 */
	public Adapter createRoomTypeProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeComparator <em>Comparator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeComparator
	 * @generated
	 */
	public Adapter createRoomTypeComparatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO <em>Free Room Types DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO
	 * @generated
	 */
	public Adapter createFreeRoomTypesDTOAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType <em>IRoom Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType
	 * @generated
	 */
	public Adapter createIRoomTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //RoomTypeAdapterFactory
