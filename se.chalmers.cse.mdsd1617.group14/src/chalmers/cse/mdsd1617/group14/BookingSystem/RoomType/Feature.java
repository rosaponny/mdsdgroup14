/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.RoomType;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypePackage#getFeature()
 * @model
 * @generated
 */
public interface Feature extends EObject {
} // Feature
