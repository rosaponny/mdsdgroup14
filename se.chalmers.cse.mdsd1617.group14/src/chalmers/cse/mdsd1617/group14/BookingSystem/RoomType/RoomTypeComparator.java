/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.RoomType;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comparator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypePackage#getRoomTypeComparator()
 * @model
 * @generated
 */
public interface RoomTypeComparator extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" room1Required="true" room1Ordered="false" room2Required="true" room2Ordered="false"
	 * @generated
	 */
	int compare(IRoomType room1, IRoomType room2);

} // RoomTypeComparator
