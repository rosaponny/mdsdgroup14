/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.Feature;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypePackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeTable;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Provider</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeProviderImpl#getRoomTypeTable <em>Room Type Table</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomTypeProviderImpl extends MinimalEObjectImpl.Container implements RoomTypeProvider {
	/**
	 * The cached value of the '{@link #getRoomTypeTable() <em>Room Type Table</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypeTable()
	 * @generated
	 * @ordered
	 */
	protected RoomTypeTable roomTypeTable;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected RoomTypeProviderImpl() {
		super();
		roomTypeTable = chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeFactory.eINSTANCE.createRoomTypeTable();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomTypePackage.Literals.ROOM_TYPE_PROVIDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeTable getRoomTypeTable() {
		if (roomTypeTable != null && roomTypeTable.eIsProxy()) {
			InternalEObject oldRoomTypeTable = (InternalEObject)roomTypeTable;
			roomTypeTable = (RoomTypeTable)eResolveProxy(oldRoomTypeTable);
			if (roomTypeTable != oldRoomTypeTable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RoomTypePackage.ROOM_TYPE_PROVIDER__ROOM_TYPE_TABLE, oldRoomTypeTable, roomTypeTable));
			}
		}
		return roomTypeTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeTable basicGetRoomTypeTable() {
		return roomTypeTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomTypeTable(RoomTypeTable newRoomTypeTable) {
		RoomTypeTable oldRoomTypeTable = roomTypeTable;
		roomTypeTable = newRoomTypeTable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomTypePackage.ROOM_TYPE_PROVIDER__ROOM_TYPE_TABLE, oldRoomTypeTable, roomTypeTable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean createRoomType(String name, int price, int numberOfBeds, EList<Feature> feature) {
		if (price < 0 || numberOfBeds < 0) return false;
		return roomTypeTable.createRoomType(name, price, numberOfBeds, feature);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateRoomType(String toUpdate, String name, int price, int numberOfBeds, EList<Feature> feature) {
		return roomTypeTable.updateRoomType(toUpdate, name, price, numberOfBeds, feature);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoomType(String toRemove) {
		if (!isValid(toRemove)) return false;
		return roomTypeTable.removeRoomType(toRemove);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateTypeOfRoom(String change, String newType, int roomNumber) {		
		return roomTypeTable.updateTypeOfRoom(change, newType, roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> getFreeRoomsOfType(String roomType, int desiredNumber) {
		return roomTypeTable.getFreeRoomsOfType(roomType,desiredNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getMaximumNumberOfRooms(String roomTypeDescription) {
		return roomTypeTable.getMaximumNumberOfRooms(roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean setTypeOfRoom(String roomTypeDescription, int roomNumber) {
		if (!isValid(roomTypeDescription)) return false;
		return roomTypeTable.setTypeOfRoom(roomTypeDescription, roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isValid(String roomTypeDescription) {
		return roomTypeTable.isValid(roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getNumberOfFreeRooms(String roomTypeDescription) {
		return roomTypeTable.getNumberOfFreeRooms(roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getNumberOfBeds(String roomTypeDescription) {
		return roomTypeTable.getNumberOfBeds(roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getPrice(String roomTypeDescription) {
		return roomTypeTable.getPrice(roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean reset() {
		this.roomTypeTable.reset();
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getTypeOfRoom(int roomNumber) {
		return roomTypeTable.getTypeOfRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<String> listRoomTypes() {
		return roomTypeTable.listRoomTypes();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> listRoomsOfType(String roomTypeDescription) {
		return roomTypeTable.listRoomsOfType(roomTypeDescription);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoom(int roomNumber) {
		return roomTypeTable.removeRoom(roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomTypePackage.ROOM_TYPE_PROVIDER__ROOM_TYPE_TABLE:
				if (resolve) return getRoomTypeTable();
				return basicGetRoomTypeTable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomTypePackage.ROOM_TYPE_PROVIDER__ROOM_TYPE_TABLE:
				setRoomTypeTable((RoomTypeTable)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomTypePackage.ROOM_TYPE_PROVIDER__ROOM_TYPE_TABLE:
				setRoomTypeTable((RoomTypeTable)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomTypePackage.ROOM_TYPE_PROVIDER__ROOM_TYPE_TABLE:
				return roomTypeTable != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomTypePackage.ROOM_TYPE_PROVIDER___CREATE_ROOM_TYPE__STRING_INT_INT_ELIST:
				return createRoomType((String)arguments.get(0), (Integer)arguments.get(1), (Integer)arguments.get(2), (EList<Feature>)arguments.get(3));
			case RoomTypePackage.ROOM_TYPE_PROVIDER___UPDATE_ROOM_TYPE__STRING_STRING_INT_INT_ELIST:
				return updateRoomType((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (EList<Feature>)arguments.get(4));
			case RoomTypePackage.ROOM_TYPE_PROVIDER___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_PROVIDER___UPDATE_TYPE_OF_ROOM__STRING_STRING_INT:
				return updateTypeOfRoom((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2));
			case RoomTypePackage.ROOM_TYPE_PROVIDER___GET_FREE_ROOMS_OF_TYPE__STRING_INT:
				return getFreeRoomsOfType((String)arguments.get(0), (Integer)arguments.get(1));
			case RoomTypePackage.ROOM_TYPE_PROVIDER___GET_MAXIMUM_NUMBER_OF_ROOMS__STRING:
				return getMaximumNumberOfRooms((String)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_PROVIDER___SET_TYPE_OF_ROOM__STRING_INT:
				return setTypeOfRoom((String)arguments.get(0), (Integer)arguments.get(1));
			case RoomTypePackage.ROOM_TYPE_PROVIDER___IS_VALID__STRING:
				return isValid((String)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_PROVIDER___GET_NUMBER_OF_FREE_ROOMS__STRING:
				return getNumberOfFreeRooms((String)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_PROVIDER___GET_NUMBER_OF_BEDS__STRING:
				return getNumberOfBeds((String)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_PROVIDER___GET_PRICE__STRING:
				return getPrice((String)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_PROVIDER___RESET:
				return reset();
			case RoomTypePackage.ROOM_TYPE_PROVIDER___GET_TYPE_OF_ROOM__INT:
				return getTypeOfRoom((Integer)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_PROVIDER___LIST_ROOM_TYPES:
				return listRoomTypes();
			case RoomTypePackage.ROOM_TYPE_PROVIDER___LIST_ROOMS_OF_TYPE__STRING:
				return listRoomsOfType((String)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_PROVIDER___REMOVE_ROOM__INT:
				return removeRoom((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomTypeProviderImpl
