/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.RoomType;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeFactory
 * @model kind="package"
 * @generated
 */
public interface RoomTypePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "RoomType";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///chalmers/cse/mdsd1617/group14/BookingSystem/RoomType.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "chalmers.cse.mdsd1617.group14.BookingSystem.RoomType";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RoomTypePackage eINSTANCE = chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl.init();

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider <em>IRoom Type Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getIRoomTypeProvider()
	 * @generated
	 */
	int IROOM_TYPE_PROVIDER = 0;

	/**
	 * The number of structural features of the '<em>IRoom Type Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Create Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER___CREATE_ROOM_TYPE__STRING_INT_INT_ELIST = 0;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER___UPDATE_ROOM_TYPE__STRING_STRING_INT_INT_ELIST = 1;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER___REMOVE_ROOM_TYPE__STRING = 2;

	/**
	 * The operation id for the '<em>Update Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER___UPDATE_TYPE_OF_ROOM__STRING_STRING_INT = 3;

	/**
	 * The operation id for the '<em>Get Free Rooms Of Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER___GET_FREE_ROOMS_OF_TYPE__STRING_INT = 4;

	/**
	 * The operation id for the '<em>Get Maximum Number Of Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER___GET_MAXIMUM_NUMBER_OF_ROOMS__STRING = 5;

	/**
	 * The operation id for the '<em>Set Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER___SET_TYPE_OF_ROOM__STRING_INT = 6;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER___IS_VALID__STRING = 7;

	/**
	 * The operation id for the '<em>Get Number Of Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER___GET_NUMBER_OF_FREE_ROOMS__STRING = 8;

	/**
	 * The operation id for the '<em>Get Number Of Beds</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER___GET_NUMBER_OF_BEDS__STRING = 9;

	/**
	 * The operation id for the '<em>Get Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER___GET_PRICE__STRING = 10;

	/**
	 * The operation id for the '<em>Reset</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER___RESET = 11;

	/**
	 * The operation id for the '<em>Get Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER___GET_TYPE_OF_ROOM__INT = 12;

	/**
	 * The operation id for the '<em>List Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER___LIST_ROOM_TYPES = 13;

	/**
	 * The operation id for the '<em>List Rooms Of Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER___LIST_ROOMS_OF_TYPE__STRING = 14;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER___REMOVE_ROOM__INT = 15;

	/**
	 * The number of operations of the '<em>IRoom Type Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_PROVIDER_OPERATION_COUNT = 16;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.FeatureImpl <em>Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.FeatureImpl
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getFeature()
	 * @generated
	 */
	int FEATURE = 1;

	/**
	 * The number of structural features of the '<em>Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeImpl <em>Room Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeImpl
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getRoomType()
	 * @generated
	 */
	int ROOM_TYPE = 2;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable <em>IRoom Type Table</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getIRoomTypeTable()
	 * @generated
	 */
	int IROOM_TYPE_TABLE = 4;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeTableImpl <em>Table</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeTableImpl
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getRoomTypeTable()
	 * @generated
	 */
	int ROOM_TYPE_TABLE = 5;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeProviderImpl <em>Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeProviderImpl
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getRoomTypeProvider()
	 * @generated
	 */
	int ROOM_TYPE_PROVIDER = 6;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeComparatorImpl <em>Comparator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeComparatorImpl
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getRoomTypeComparator()
	 * @generated
	 */
	int ROOM_TYPE_COMPARATOR = 7;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType <em>IRoom Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getIRoomType()
	 * @generated
	 */
	int IROOM_TYPE = 3;

	/**
	 * The number of structural features of the '<em>IRoom Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___UPDATE_ROOM_TYPE__STRING_INT_INT_ELIST = 0;

	/**
	 * The operation id for the '<em>Get Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___GET_DESCRIPTION = 1;

	/**
	 * The operation id for the '<em>Get Associated Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___GET_ASSOCIATED_ROOMS = 2;

	/**
	 * The operation id for the '<em>Get Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___GET_PRICE = 3;

	/**
	 * The operation id for the '<em>Get Number Of Beds</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___GET_NUMBER_OF_BEDS = 4;

	/**
	 * The operation id for the '<em>Maximum Number Of Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___MAXIMUM_NUMBER_OF_ROOMS = 5;

	/**
	 * The number of operations of the '<em>IRoom Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_OPERATION_COUNT = 6;

	/**
	 * The feature id for the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__PRICE = IROOM_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Number Of Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__NUMBER_OF_BEDS = IROOM_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Features</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__FEATURES = IROOM_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Associated Rooms</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__ASSOCIATED_ROOMS = IROOM_TYPE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__DESCRIPTION = IROOM_TYPE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_FEATURE_COUNT = IROOM_TYPE_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___UPDATE_ROOM_TYPE__STRING_INT_INT_ELIST = IROOM_TYPE___UPDATE_ROOM_TYPE__STRING_INT_INT_ELIST;

	/**
	 * The operation id for the '<em>Get Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___GET_DESCRIPTION = IROOM_TYPE___GET_DESCRIPTION;

	/**
	 * The operation id for the '<em>Get Associated Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___GET_ASSOCIATED_ROOMS = IROOM_TYPE___GET_ASSOCIATED_ROOMS;

	/**
	 * The operation id for the '<em>Get Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___GET_PRICE = IROOM_TYPE___GET_PRICE;

	/**
	 * The operation id for the '<em>Get Number Of Beds</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___GET_NUMBER_OF_BEDS = IROOM_TYPE___GET_NUMBER_OF_BEDS;

	/**
	 * The operation id for the '<em>Maximum Number Of Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___MAXIMUM_NUMBER_OF_ROOMS = IROOM_TYPE___MAXIMUM_NUMBER_OF_ROOMS;

	/**
	 * The number of operations of the '<em>Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_OPERATION_COUNT = IROOM_TYPE_OPERATION_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IRoom Type Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE___ADD_ROOM_TYPE__STRING = 0;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE___REMOVE_ROOM_TYPE__STRING = 1;

	/**
	 * The operation id for the '<em>List Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE___LIST_ROOM_TYPES = 2;

	/**
	 * The operation id for the '<em>Update Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE___UPDATE_TYPE_OF_ROOM__STRING_STRING_INT = 3;

	/**
	 * The operation id for the '<em>Get Free Rooms Of Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE___GET_FREE_ROOMS_OF_TYPE__STRING_INT = 4;

	/**
	 * The operation id for the '<em>Set Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE___SET_TYPE_OF_ROOM__STRING_INT = 5;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE___IS_VALID__STRING = 6;

	/**
	 * The operation id for the '<em>Create Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE___CREATE_ROOM_TYPE__STRING_INT_INT_ELIST = 7;

	/**
	 * The operation id for the '<em>Get Maximum Number Of Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE___GET_MAXIMUM_NUMBER_OF_ROOMS__STRING = 8;

	/**
	 * The operation id for the '<em>Get Number Of Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE___GET_NUMBER_OF_FREE_ROOMS__STRING = 9;

	/**
	 * The operation id for the '<em>Get Number Of Beds</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE___GET_NUMBER_OF_BEDS__STRING = 10;

	/**
	 * The operation id for the '<em>Get Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE___GET_PRICE__STRING = 11;

	/**
	 * The operation id for the '<em>Reset</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE___RESET = 12;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE___UPDATE_ROOM_TYPE__STRING_STRING_INT_INT_ELIST = 13;

	/**
	 * The operation id for the '<em>Get Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE___GET_TYPE_OF_ROOM__INT = 14;

	/**
	 * The operation id for the '<em>List Rooms Of Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE___LIST_ROOMS_OF_TYPE__STRING = 15;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE___REMOVE_ROOM__INT = 16;

	/**
	 * The number of operations of the '<em>IRoom Type Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_TABLE_OPERATION_COUNT = 17;

	/**
	 * The feature id for the '<em><b>Room Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE__ROOM_TYPES = IROOM_TYPE_TABLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE_FEATURE_COUNT = IROOM_TYPE_TABLE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE___ADD_ROOM_TYPE__STRING = IROOM_TYPE_TABLE___ADD_ROOM_TYPE__STRING;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE___REMOVE_ROOM_TYPE__STRING = IROOM_TYPE_TABLE___REMOVE_ROOM_TYPE__STRING;

	/**
	 * The operation id for the '<em>List Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE___LIST_ROOM_TYPES = IROOM_TYPE_TABLE___LIST_ROOM_TYPES;

	/**
	 * The operation id for the '<em>Update Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE___UPDATE_TYPE_OF_ROOM__STRING_STRING_INT = IROOM_TYPE_TABLE___UPDATE_TYPE_OF_ROOM__STRING_STRING_INT;

	/**
	 * The operation id for the '<em>Get Free Rooms Of Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE___GET_FREE_ROOMS_OF_TYPE__STRING_INT = IROOM_TYPE_TABLE___GET_FREE_ROOMS_OF_TYPE__STRING_INT;

	/**
	 * The operation id for the '<em>Set Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE___SET_TYPE_OF_ROOM__STRING_INT = IROOM_TYPE_TABLE___SET_TYPE_OF_ROOM__STRING_INT;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE___IS_VALID__STRING = IROOM_TYPE_TABLE___IS_VALID__STRING;

	/**
	 * The operation id for the '<em>Create Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE___CREATE_ROOM_TYPE__STRING_INT_INT_ELIST = IROOM_TYPE_TABLE___CREATE_ROOM_TYPE__STRING_INT_INT_ELIST;

	/**
	 * The operation id for the '<em>Get Maximum Number Of Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE___GET_MAXIMUM_NUMBER_OF_ROOMS__STRING = IROOM_TYPE_TABLE___GET_MAXIMUM_NUMBER_OF_ROOMS__STRING;

	/**
	 * The operation id for the '<em>Get Number Of Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE___GET_NUMBER_OF_FREE_ROOMS__STRING = IROOM_TYPE_TABLE___GET_NUMBER_OF_FREE_ROOMS__STRING;

	/**
	 * The operation id for the '<em>Get Number Of Beds</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE___GET_NUMBER_OF_BEDS__STRING = IROOM_TYPE_TABLE___GET_NUMBER_OF_BEDS__STRING;

	/**
	 * The operation id for the '<em>Get Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE___GET_PRICE__STRING = IROOM_TYPE_TABLE___GET_PRICE__STRING;

	/**
	 * The operation id for the '<em>Reset</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE___RESET = IROOM_TYPE_TABLE___RESET;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE___UPDATE_ROOM_TYPE__STRING_STRING_INT_INT_ELIST = IROOM_TYPE_TABLE___UPDATE_ROOM_TYPE__STRING_STRING_INT_INT_ELIST;

	/**
	 * The operation id for the '<em>Get Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE___GET_TYPE_OF_ROOM__INT = IROOM_TYPE_TABLE___GET_TYPE_OF_ROOM__INT;

	/**
	 * The operation id for the '<em>List Rooms Of Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE___LIST_ROOMS_OF_TYPE__STRING = IROOM_TYPE_TABLE___LIST_ROOMS_OF_TYPE__STRING;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE___REMOVE_ROOM__INT = IROOM_TYPE_TABLE___REMOVE_ROOM__INT;

	/**
	 * The number of operations of the '<em>Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_TABLE_OPERATION_COUNT = IROOM_TYPE_TABLE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Room Type Table</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER__ROOM_TYPE_TABLE = IROOM_TYPE_PROVIDER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER_FEATURE_COUNT = IROOM_TYPE_PROVIDER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Create Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER___CREATE_ROOM_TYPE__STRING_INT_INT_ELIST = IROOM_TYPE_PROVIDER___CREATE_ROOM_TYPE__STRING_INT_INT_ELIST;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER___UPDATE_ROOM_TYPE__STRING_STRING_INT_INT_ELIST = IROOM_TYPE_PROVIDER___UPDATE_ROOM_TYPE__STRING_STRING_INT_INT_ELIST;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER___REMOVE_ROOM_TYPE__STRING = IROOM_TYPE_PROVIDER___REMOVE_ROOM_TYPE__STRING;

	/**
	 * The operation id for the '<em>Update Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER___UPDATE_TYPE_OF_ROOM__STRING_STRING_INT = IROOM_TYPE_PROVIDER___UPDATE_TYPE_OF_ROOM__STRING_STRING_INT;

	/**
	 * The operation id for the '<em>Get Free Rooms Of Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER___GET_FREE_ROOMS_OF_TYPE__STRING_INT = IROOM_TYPE_PROVIDER___GET_FREE_ROOMS_OF_TYPE__STRING_INT;

	/**
	 * The operation id for the '<em>Get Maximum Number Of Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER___GET_MAXIMUM_NUMBER_OF_ROOMS__STRING = IROOM_TYPE_PROVIDER___GET_MAXIMUM_NUMBER_OF_ROOMS__STRING;

	/**
	 * The operation id for the '<em>Set Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER___SET_TYPE_OF_ROOM__STRING_INT = IROOM_TYPE_PROVIDER___SET_TYPE_OF_ROOM__STRING_INT;

	/**
	 * The operation id for the '<em>Is Valid</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER___IS_VALID__STRING = IROOM_TYPE_PROVIDER___IS_VALID__STRING;

	/**
	 * The operation id for the '<em>Get Number Of Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER___GET_NUMBER_OF_FREE_ROOMS__STRING = IROOM_TYPE_PROVIDER___GET_NUMBER_OF_FREE_ROOMS__STRING;

	/**
	 * The operation id for the '<em>Get Number Of Beds</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER___GET_NUMBER_OF_BEDS__STRING = IROOM_TYPE_PROVIDER___GET_NUMBER_OF_BEDS__STRING;

	/**
	 * The operation id for the '<em>Get Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER___GET_PRICE__STRING = IROOM_TYPE_PROVIDER___GET_PRICE__STRING;

	/**
	 * The operation id for the '<em>Reset</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER___RESET = IROOM_TYPE_PROVIDER___RESET;

	/**
	 * The operation id for the '<em>Get Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER___GET_TYPE_OF_ROOM__INT = IROOM_TYPE_PROVIDER___GET_TYPE_OF_ROOM__INT;

	/**
	 * The operation id for the '<em>List Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER___LIST_ROOM_TYPES = IROOM_TYPE_PROVIDER___LIST_ROOM_TYPES;

	/**
	 * The operation id for the '<em>List Rooms Of Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER___LIST_ROOMS_OF_TYPE__STRING = IROOM_TYPE_PROVIDER___LIST_ROOMS_OF_TYPE__STRING;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER___REMOVE_ROOM__INT = IROOM_TYPE_PROVIDER___REMOVE_ROOM__INT;

	/**
	 * The number of operations of the '<em>Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_PROVIDER_OPERATION_COUNT = IROOM_TYPE_PROVIDER_OPERATION_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Comparator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_COMPARATOR_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Compare</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_COMPARATOR___COMPARE__IROOMTYPE_IROOMTYPE = 0;

	/**
	 * The number of operations of the '<em>Comparator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_COMPARATOR_OPERATION_COUNT = 1;


	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.FreeRoomTypesDTOImpl <em>Free Room Types DTO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.FreeRoomTypesDTOImpl
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getFreeRoomTypesDTO()
	 * @generated
	 */
	int FREE_ROOM_TYPES_DTO = 8;

	/**
	 * The feature id for the '<em><b>Room Type Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__ROOM_TYPE_DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Num Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__NUM_BEDS = 1;

	/**
	 * The feature id for the '<em><b>Price Per Night</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__PRICE_PER_NIGHT = 2;

	/**
	 * The feature id for the '<em><b>Num Free Rooms</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__NUM_FREE_ROOMS = 3;

	/**
	 * The number of structural features of the '<em>Free Room Types DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO_FEATURE_COUNT = 4;

	/**
	 * The operation id for the '<em>Init</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO___INIT__STRING = 0;

	/**
	 * The number of operations of the '<em>Free Room Types DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO_OPERATION_COUNT = 1;


	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider <em>IRoom Type Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Type Provider</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider
	 * @generated
	 */
	EClass getIRoomTypeProvider();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#createRoomType(java.lang.String, int, int, org.eclipse.emf.common.util.EList) <em>Create Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Room Type</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#createRoomType(java.lang.String, int, int, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getIRoomTypeProvider__CreateRoomType__String_int_int_EList();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#updateRoomType(java.lang.String, java.lang.String, int, int, org.eclipse.emf.common.util.EList) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#updateRoomType(java.lang.String, java.lang.String, int, int, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getIRoomTypeProvider__UpdateRoomType__String_String_int_int_EList();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeProvider__RemoveRoomType__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#updateTypeOfRoom(java.lang.String, java.lang.String, int) <em>Update Type Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Type Of Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#updateTypeOfRoom(java.lang.String, java.lang.String, int)
	 * @generated
	 */
	EOperation getIRoomTypeProvider__UpdateTypeOfRoom__String_String_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#getFreeRoomsOfType(java.lang.String, int) <em>Get Free Rooms Of Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms Of Type</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#getFreeRoomsOfType(java.lang.String, int)
	 * @generated
	 */
	EOperation getIRoomTypeProvider__GetFreeRoomsOfType__String_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#getMaximumNumberOfRooms(java.lang.String) <em>Get Maximum Number Of Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Maximum Number Of Rooms</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#getMaximumNumberOfRooms(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeProvider__GetMaximumNumberOfRooms__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#setTypeOfRoom(java.lang.String, int) <em>Set Type Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Type Of Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#setTypeOfRoom(java.lang.String, int)
	 * @generated
	 */
	EOperation getIRoomTypeProvider__SetTypeOfRoom__String_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#isValid(java.lang.String) <em>Is Valid</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#isValid(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeProvider__IsValid__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#getNumberOfFreeRooms(java.lang.String) <em>Get Number Of Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Number Of Free Rooms</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#getNumberOfFreeRooms(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeProvider__GetNumberOfFreeRooms__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#getNumberOfBeds(java.lang.String) <em>Get Number Of Beds</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Number Of Beds</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#getNumberOfBeds(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeProvider__GetNumberOfBeds__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#getPrice(java.lang.String) <em>Get Price</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Price</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#getPrice(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeProvider__GetPrice__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#reset() <em>Reset</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Reset</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#reset()
	 * @generated
	 */
	EOperation getIRoomTypeProvider__Reset();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#getTypeOfRoom(int) <em>Get Type Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Type Of Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#getTypeOfRoom(int)
	 * @generated
	 */
	EOperation getIRoomTypeProvider__GetTypeOfRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#listRoomTypes() <em>List Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Room Types</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#listRoomTypes()
	 * @generated
	 */
	EOperation getIRoomTypeProvider__ListRoomTypes();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#listRoomsOfType(java.lang.String) <em>List Rooms Of Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Rooms Of Type</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#listRoomsOfType(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeProvider__ListRoomsOfType__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider#removeRoom(int)
	 * @generated
	 */
	EOperation getIRoomTypeProvider__RemoveRoom__int();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.Feature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.Feature
	 * @generated
	 */
	EClass getFeature();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Type</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomType
	 * @generated
	 */
	EClass getRoomType();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomType#getPrice <em>Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomType#getPrice()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Price();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomType#getNumberOfBeds <em>Number Of Beds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Of Beds</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomType#getNumberOfBeds()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_NumberOfBeds();

	/**
	 * Returns the meta object for the reference list '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomType#getFeatures <em>Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Features</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomType#getFeatures()
	 * @see #getRoomType()
	 * @generated
	 */
	EReference getRoomType_Features();

	/**
	 * Returns the meta object for the attribute list '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomType#getAssociatedRooms <em>Associated Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Associated Rooms</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomType#getAssociatedRooms()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_AssociatedRooms();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomType#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomType#getDescription()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Description();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable <em>IRoom Type Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Type Table</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable
	 * @generated
	 */
	EClass getIRoomTypeTable();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#addRoomType(java.lang.String) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#addRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeTable__AddRoomType__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeTable__RemoveRoomType__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#listRoomTypes() <em>List Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Room Types</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#listRoomTypes()
	 * @generated
	 */
	EOperation getIRoomTypeTable__ListRoomTypes();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#updateTypeOfRoom(java.lang.String, java.lang.String, int) <em>Update Type Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Type Of Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#updateTypeOfRoom(java.lang.String, java.lang.String, int)
	 * @generated
	 */
	EOperation getIRoomTypeTable__UpdateTypeOfRoom__String_String_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#getFreeRoomsOfType(java.lang.String, int) <em>Get Free Rooms Of Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms Of Type</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#getFreeRoomsOfType(java.lang.String, int)
	 * @generated
	 */
	EOperation getIRoomTypeTable__GetFreeRoomsOfType__String_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#setTypeOfRoom(java.lang.String, int) <em>Set Type Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Type Of Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#setTypeOfRoom(java.lang.String, int)
	 * @generated
	 */
	EOperation getIRoomTypeTable__SetTypeOfRoom__String_int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#isValid(java.lang.String) <em>Is Valid</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Valid</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#isValid(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeTable__IsValid__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#createRoomType(java.lang.String, int, int, org.eclipse.emf.common.util.EList) <em>Create Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Room Type</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#createRoomType(java.lang.String, int, int, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getIRoomTypeTable__CreateRoomType__String_int_int_EList();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#getMaximumNumberOfRooms(java.lang.String) <em>Get Maximum Number Of Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Maximum Number Of Rooms</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#getMaximumNumberOfRooms(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeTable__GetMaximumNumberOfRooms__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#getNumberOfFreeRooms(java.lang.String) <em>Get Number Of Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Number Of Free Rooms</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#getNumberOfFreeRooms(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeTable__GetNumberOfFreeRooms__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#getNumberOfBeds(java.lang.String) <em>Get Number Of Beds</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Number Of Beds</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#getNumberOfBeds(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeTable__GetNumberOfBeds__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#getPrice(java.lang.String) <em>Get Price</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Price</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#getPrice(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeTable__GetPrice__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#reset() <em>Reset</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Reset</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#reset()
	 * @generated
	 */
	EOperation getIRoomTypeTable__Reset();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#updateRoomType(java.lang.String, java.lang.String, int, int, org.eclipse.emf.common.util.EList) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#updateRoomType(java.lang.String, java.lang.String, int, int, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getIRoomTypeTable__UpdateRoomType__String_String_int_int_EList();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#getTypeOfRoom(int) <em>Get Type Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Type Of Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#getTypeOfRoom(int)
	 * @generated
	 */
	EOperation getIRoomTypeTable__GetTypeOfRoom__int();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#listRoomsOfType(java.lang.String) <em>List Rooms Of Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Rooms Of Type</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#listRoomsOfType(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeTable__ListRoomsOfType__String();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable#removeRoom(int)
	 * @generated
	 */
	EOperation getIRoomTypeTable__RemoveRoom__int();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeTable
	 * @generated
	 */
	EClass getRoomTypeTable();

	/**
	 * Returns the meta object for the reference list '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeTable#getRoomTypes <em>Room Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Room Types</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeTable#getRoomTypes()
	 * @see #getRoomTypeTable()
	 * @generated
	 */
	EReference getRoomTypeTable_RoomTypes();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeProvider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provider</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeProvider
	 * @generated
	 */
	EClass getRoomTypeProvider();

	/**
	 * Returns the meta object for the reference '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeProvider#getRoomTypeTable <em>Room Type Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Type Table</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeProvider#getRoomTypeTable()
	 * @see #getRoomTypeProvider()
	 * @generated
	 */
	EReference getRoomTypeProvider_RoomTypeTable();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeComparator <em>Comparator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Comparator</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeComparator
	 * @generated
	 */
	EClass getRoomTypeComparator();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeComparator#compare(chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType, chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType) <em>Compare</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Compare</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeComparator#compare(chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType, chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType)
	 * @generated
	 */
	EOperation getRoomTypeComparator__Compare__IRoomType_IRoomType();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO <em>Free Room Types DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Free Room Types DTO</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO
	 * @generated
	 */
	EClass getFreeRoomTypesDTO();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO#getRoomTypeDescription <em>Room Type Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Type Description</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO#getRoomTypeDescription()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_RoomTypeDescription();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO#getNumBeds <em>Num Beds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Beds</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO#getNumBeds()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_NumBeds();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO#getPricePerNight <em>Price Per Night</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price Per Night</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO#getPricePerNight()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_PricePerNight();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO#getNumFreeRooms <em>Num Free Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Free Rooms</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO#getNumFreeRooms()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_NumFreeRooms();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO#init(java.lang.String) <em>Init</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Init</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO#init(java.lang.String)
	 * @generated
	 */
	EOperation getFreeRoomTypesDTO__Init__String();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType <em>IRoom Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Type</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType
	 * @generated
	 */
	EClass getIRoomType();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType#updateRoomType(java.lang.String, int, int, org.eclipse.emf.common.util.EList) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType#updateRoomType(java.lang.String, int, int, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getIRoomType__UpdateRoomType__String_int_int_EList();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType#getDescription() <em>Get Description</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Description</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType#getDescription()
	 * @generated
	 */
	EOperation getIRoomType__GetDescription();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType#getAssociatedRooms() <em>Get Associated Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Associated Rooms</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType#getAssociatedRooms()
	 * @generated
	 */
	EOperation getIRoomType__GetAssociatedRooms();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType#getPrice() <em>Get Price</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Price</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType#getPrice()
	 * @generated
	 */
	EOperation getIRoomType__GetPrice();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType#getNumberOfBeds() <em>Get Number Of Beds</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Number Of Beds</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType#getNumberOfBeds()
	 * @generated
	 */
	EOperation getIRoomType__GetNumberOfBeds();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType#maximumNumberOfRooms() <em>Maximum Number Of Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Maximum Number Of Rooms</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType#maximumNumberOfRooms()
	 * @generated
	 */
	EOperation getIRoomType__MaximumNumberOfRooms();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RoomTypeFactory getRoomTypeFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider <em>IRoom Type Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getIRoomTypeProvider()
		 * @generated
		 */
		EClass IROOM_TYPE_PROVIDER = eINSTANCE.getIRoomTypeProvider();

		/**
		 * The meta object literal for the '<em><b>Create Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_PROVIDER___CREATE_ROOM_TYPE__STRING_INT_INT_ELIST = eINSTANCE.getIRoomTypeProvider__CreateRoomType__String_int_int_EList();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_PROVIDER___UPDATE_ROOM_TYPE__STRING_STRING_INT_INT_ELIST = eINSTANCE.getIRoomTypeProvider__UpdateRoomType__String_String_int_int_EList();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_PROVIDER___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getIRoomTypeProvider__RemoveRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Update Type Of Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_PROVIDER___UPDATE_TYPE_OF_ROOM__STRING_STRING_INT = eINSTANCE.getIRoomTypeProvider__UpdateTypeOfRoom__String_String_int();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms Of Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_PROVIDER___GET_FREE_ROOMS_OF_TYPE__STRING_INT = eINSTANCE.getIRoomTypeProvider__GetFreeRoomsOfType__String_int();

		/**
		 * The meta object literal for the '<em><b>Get Maximum Number Of Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_PROVIDER___GET_MAXIMUM_NUMBER_OF_ROOMS__STRING = eINSTANCE.getIRoomTypeProvider__GetMaximumNumberOfRooms__String();

		/**
		 * The meta object literal for the '<em><b>Set Type Of Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_PROVIDER___SET_TYPE_OF_ROOM__STRING_INT = eINSTANCE.getIRoomTypeProvider__SetTypeOfRoom__String_int();

		/**
		 * The meta object literal for the '<em><b>Is Valid</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_PROVIDER___IS_VALID__STRING = eINSTANCE.getIRoomTypeProvider__IsValid__String();

		/**
		 * The meta object literal for the '<em><b>Get Number Of Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_PROVIDER___GET_NUMBER_OF_FREE_ROOMS__STRING = eINSTANCE.getIRoomTypeProvider__GetNumberOfFreeRooms__String();

		/**
		 * The meta object literal for the '<em><b>Get Number Of Beds</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_PROVIDER___GET_NUMBER_OF_BEDS__STRING = eINSTANCE.getIRoomTypeProvider__GetNumberOfBeds__String();

		/**
		 * The meta object literal for the '<em><b>Get Price</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_PROVIDER___GET_PRICE__STRING = eINSTANCE.getIRoomTypeProvider__GetPrice__String();

		/**
		 * The meta object literal for the '<em><b>Reset</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_PROVIDER___RESET = eINSTANCE.getIRoomTypeProvider__Reset();

		/**
		 * The meta object literal for the '<em><b>Get Type Of Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_PROVIDER___GET_TYPE_OF_ROOM__INT = eINSTANCE.getIRoomTypeProvider__GetTypeOfRoom__int();

		/**
		 * The meta object literal for the '<em><b>List Room Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_PROVIDER___LIST_ROOM_TYPES = eINSTANCE.getIRoomTypeProvider__ListRoomTypes();

		/**
		 * The meta object literal for the '<em><b>List Rooms Of Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_PROVIDER___LIST_ROOMS_OF_TYPE__STRING = eINSTANCE.getIRoomTypeProvider__ListRoomsOfType__String();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_PROVIDER___REMOVE_ROOM__INT = eINSTANCE.getIRoomTypeProvider__RemoveRoom__int();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.FeatureImpl <em>Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.FeatureImpl
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getFeature()
		 * @generated
		 */
		EClass FEATURE = eINSTANCE.getFeature();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeImpl <em>Room Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeImpl
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getRoomType()
		 * @generated
		 */
		EClass ROOM_TYPE = eINSTANCE.getRoomType();

		/**
		 * The meta object literal for the '<em><b>Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__PRICE = eINSTANCE.getRoomType_Price();

		/**
		 * The meta object literal for the '<em><b>Number Of Beds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__NUMBER_OF_BEDS = eINSTANCE.getRoomType_NumberOfBeds();

		/**
		 * The meta object literal for the '<em><b>Features</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_TYPE__FEATURES = eINSTANCE.getRoomType_Features();

		/**
		 * The meta object literal for the '<em><b>Associated Rooms</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__ASSOCIATED_ROOMS = eINSTANCE.getRoomType_AssociatedRooms();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__DESCRIPTION = eINSTANCE.getRoomType_Description();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable <em>IRoom Type Table</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getIRoomTypeTable()
		 * @generated
		 */
		EClass IROOM_TYPE_TABLE = eINSTANCE.getIRoomTypeTable();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_TABLE___ADD_ROOM_TYPE__STRING = eINSTANCE.getIRoomTypeTable__AddRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_TABLE___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getIRoomTypeTable__RemoveRoomType__String();

		/**
		 * The meta object literal for the '<em><b>List Room Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_TABLE___LIST_ROOM_TYPES = eINSTANCE.getIRoomTypeTable__ListRoomTypes();

		/**
		 * The meta object literal for the '<em><b>Update Type Of Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_TABLE___UPDATE_TYPE_OF_ROOM__STRING_STRING_INT = eINSTANCE.getIRoomTypeTable__UpdateTypeOfRoom__String_String_int();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms Of Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_TABLE___GET_FREE_ROOMS_OF_TYPE__STRING_INT = eINSTANCE.getIRoomTypeTable__GetFreeRoomsOfType__String_int();

		/**
		 * The meta object literal for the '<em><b>Set Type Of Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_TABLE___SET_TYPE_OF_ROOM__STRING_INT = eINSTANCE.getIRoomTypeTable__SetTypeOfRoom__String_int();

		/**
		 * The meta object literal for the '<em><b>Is Valid</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_TABLE___IS_VALID__STRING = eINSTANCE.getIRoomTypeTable__IsValid__String();

		/**
		 * The meta object literal for the '<em><b>Create Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_TABLE___CREATE_ROOM_TYPE__STRING_INT_INT_ELIST = eINSTANCE.getIRoomTypeTable__CreateRoomType__String_int_int_EList();

		/**
		 * The meta object literal for the '<em><b>Get Maximum Number Of Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_TABLE___GET_MAXIMUM_NUMBER_OF_ROOMS__STRING = eINSTANCE.getIRoomTypeTable__GetMaximumNumberOfRooms__String();

		/**
		 * The meta object literal for the '<em><b>Get Number Of Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_TABLE___GET_NUMBER_OF_FREE_ROOMS__STRING = eINSTANCE.getIRoomTypeTable__GetNumberOfFreeRooms__String();

		/**
		 * The meta object literal for the '<em><b>Get Number Of Beds</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_TABLE___GET_NUMBER_OF_BEDS__STRING = eINSTANCE.getIRoomTypeTable__GetNumberOfBeds__String();

		/**
		 * The meta object literal for the '<em><b>Get Price</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_TABLE___GET_PRICE__STRING = eINSTANCE.getIRoomTypeTable__GetPrice__String();

		/**
		 * The meta object literal for the '<em><b>Reset</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_TABLE___RESET = eINSTANCE.getIRoomTypeTable__Reset();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_TABLE___UPDATE_ROOM_TYPE__STRING_STRING_INT_INT_ELIST = eINSTANCE.getIRoomTypeTable__UpdateRoomType__String_String_int_int_EList();

		/**
		 * The meta object literal for the '<em><b>Get Type Of Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_TABLE___GET_TYPE_OF_ROOM__INT = eINSTANCE.getIRoomTypeTable__GetTypeOfRoom__int();

		/**
		 * The meta object literal for the '<em><b>List Rooms Of Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_TABLE___LIST_ROOMS_OF_TYPE__STRING = eINSTANCE.getIRoomTypeTable__ListRoomsOfType__String();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_TABLE___REMOVE_ROOM__INT = eINSTANCE.getIRoomTypeTable__RemoveRoom__int();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeTableImpl <em>Table</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeTableImpl
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getRoomTypeTable()
		 * @generated
		 */
		EClass ROOM_TYPE_TABLE = eINSTANCE.getRoomTypeTable();

		/**
		 * The meta object literal for the '<em><b>Room Types</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_TYPE_TABLE__ROOM_TYPES = eINSTANCE.getRoomTypeTable_RoomTypes();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeProviderImpl <em>Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeProviderImpl
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getRoomTypeProvider()
		 * @generated
		 */
		EClass ROOM_TYPE_PROVIDER = eINSTANCE.getRoomTypeProvider();

		/**
		 * The meta object literal for the '<em><b>Room Type Table</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_TYPE_PROVIDER__ROOM_TYPE_TABLE = eINSTANCE.getRoomTypeProvider_RoomTypeTable();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeComparatorImpl <em>Comparator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeComparatorImpl
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getRoomTypeComparator()
		 * @generated
		 */
		EClass ROOM_TYPE_COMPARATOR = eINSTANCE.getRoomTypeComparator();

		/**
		 * The meta object literal for the '<em><b>Compare</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM_TYPE_COMPARATOR___COMPARE__IROOMTYPE_IROOMTYPE = eINSTANCE.getRoomTypeComparator__Compare__IRoomType_IRoomType();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.FreeRoomTypesDTOImpl <em>Free Room Types DTO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.FreeRoomTypesDTOImpl
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getFreeRoomTypesDTO()
		 * @generated
		 */
		EClass FREE_ROOM_TYPES_DTO = eINSTANCE.getFreeRoomTypesDTO();

		/**
		 * The meta object literal for the '<em><b>Room Type Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__ROOM_TYPE_DESCRIPTION = eINSTANCE.getFreeRoomTypesDTO_RoomTypeDescription();

		/**
		 * The meta object literal for the '<em><b>Num Beds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__NUM_BEDS = eINSTANCE.getFreeRoomTypesDTO_NumBeds();

		/**
		 * The meta object literal for the '<em><b>Price Per Night</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__PRICE_PER_NIGHT = eINSTANCE.getFreeRoomTypesDTO_PricePerNight();

		/**
		 * The meta object literal for the '<em><b>Num Free Rooms</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__NUM_FREE_ROOMS = eINSTANCE.getFreeRoomTypesDTO_NumFreeRooms();

		/**
		 * The meta object literal for the '<em><b>Init</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FREE_ROOM_TYPES_DTO___INIT__STRING = eINSTANCE.getFreeRoomTypesDTO__Init__String();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType <em>IRoom Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl#getIRoomType()
		 * @generated
		 */
		EClass IROOM_TYPE = eINSTANCE.getIRoomType();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___UPDATE_ROOM_TYPE__STRING_INT_INT_ELIST = eINSTANCE.getIRoomType__UpdateRoomType__String_int_int_EList();

		/**
		 * The meta object literal for the '<em><b>Get Description</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___GET_DESCRIPTION = eINSTANCE.getIRoomType__GetDescription();

		/**
		 * The meta object literal for the '<em><b>Get Associated Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___GET_ASSOCIATED_ROOMS = eINSTANCE.getIRoomType__GetAssociatedRooms();

		/**
		 * The meta object literal for the '<em><b>Get Price</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___GET_PRICE = eINSTANCE.getIRoomType__GetPrice();

		/**
		 * The meta object literal for the '<em><b>Get Number Of Beds</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___GET_NUMBER_OF_BEDS = eINSTANCE.getIRoomType__GetNumberOfBeds();

		/**
		 * The meta object literal for the '<em><b>Maximum Number Of Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___MAXIMUM_NUMBER_OF_ROOMS = eINSTANCE.getIRoomType__MaximumNumberOfRooms();

	}

} //RoomTypePackage
