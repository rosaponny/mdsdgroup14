/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.Feature;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypePackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FeatureImpl extends MinimalEObjectImpl.Container implements Feature {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomTypePackage.Literals.FEATURE;
	}

} //FeatureImpl
