/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RoomTypeFactoryImpl extends EFactoryImpl implements RoomTypeFactory {
	
	RoomTypeProvider roomTypeProvider;
	RoomTypeTable roomTypeTable;
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RoomTypeFactory init() {
		try {
			RoomTypeFactory theRoomTypeFactory = (RoomTypeFactory)EPackage.Registry.INSTANCE.getEFactory(RoomTypePackage.eNS_URI);
			if (theRoomTypeFactory != null) {
				return theRoomTypeFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new RoomTypeFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case RoomTypePackage.FEATURE: return createFeature();
			case RoomTypePackage.ROOM_TYPE: return createRoomType();
			case RoomTypePackage.ROOM_TYPE_TABLE: return createRoomTypeTable();
			case RoomTypePackage.ROOM_TYPE_PROVIDER: return createRoomTypeProvider();
			case RoomTypePackage.ROOM_TYPE_COMPARATOR: return createRoomTypeComparator();
			case RoomTypePackage.FREE_ROOM_TYPES_DTO: return createFreeRoomTypesDTO();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature createFeature() {
		FeatureImpl feature = new FeatureImpl();
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomType createRoomType() {
		RoomTypeImpl roomType = new RoomTypeImpl();
		return roomType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public RoomTypeTable createRoomTypeTable() {
		if (roomTypeTable == null){
			roomTypeTable = new RoomTypeTableImpl();
		}
		return roomTypeTable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public RoomTypeProvider createRoomTypeProvider() {
		if(roomTypeProvider == null){
			roomTypeProvider = new RoomTypeProviderImpl();
		}
		return roomTypeProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeComparator createRoomTypeComparator() {
		RoomTypeComparatorImpl roomTypeComparator = new RoomTypeComparatorImpl();
		return roomTypeComparator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FreeRoomTypesDTO createFreeRoomTypesDTO() {
		FreeRoomTypesDTOImpl freeRoomTypesDTO = new FreeRoomTypesDTOImpl();
		return freeRoomTypesDTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypePackage getRoomTypePackage() {
		return (RoomTypePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static RoomTypePackage getPackage() {
		return RoomTypePackage.eINSTANCE;
	}

} //RoomTypeFactoryImpl
