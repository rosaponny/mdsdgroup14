/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.RoomType;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeTable#getRoomTypes <em>Room Types</em>}</li>
 * </ul>
 *
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypePackage#getRoomTypeTable()
 * @model
 * @generated
 */
public interface RoomTypeTable extends IRoomTypeTable {
	/**
	 * Returns the value of the '<em><b>Room Types</b></em>' reference list.
	 * The list contents are of type {@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Types</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Types</em>' reference list.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypePackage#getRoomTypeTable_RoomTypes()
	 * @model ordered="false"
	 * @generated
	 */
	EList<IRoomType> getRoomTypes();

} // RoomTypeTable
