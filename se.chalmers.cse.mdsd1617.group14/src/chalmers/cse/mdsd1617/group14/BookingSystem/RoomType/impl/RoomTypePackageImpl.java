/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.EconomicsPackage;

import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.EconomicsPackageImpl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomPackage;

import chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl;

import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.Feature;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.FreeRoomTypesDTO;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeTable;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomType;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeComparator;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypePackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeTable;
import chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage;
import chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.types.impl.TypesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RoomTypePackageImpl extends EPackageImpl implements RoomTypePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomTypeProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomTypeTableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomTypeTableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomTypeProviderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomTypeComparatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass freeRoomTypesDTOEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomTypeEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RoomTypePackageImpl() {
		super(eNS_URI, RoomTypeFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link RoomTypePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RoomTypePackage init() {
		if (isInited) return (RoomTypePackage)EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI);

		// Obtain or create and register package
		RoomTypePackageImpl theRoomTypePackage = (RoomTypePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof RoomTypePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new RoomTypePackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		SystemAccessPackageImpl theSystemAccessPackage = (SystemAccessPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SystemAccessPackage.eNS_URI) instanceof SystemAccessPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SystemAccessPackage.eNS_URI) : SystemAccessPackage.eINSTANCE);
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) : BookingPackage.eINSTANCE);
		EconomicsPackageImpl theEconomicsPackage = (EconomicsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(EconomicsPackage.eNS_URI) instanceof EconomicsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(EconomicsPackage.eNS_URI) : EconomicsPackage.eINSTANCE);
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) : RoomPackage.eINSTANCE);
		TypesPackageImpl theTypesPackage = (TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) : TypesPackage.eINSTANCE);

		// Create package meta-data objects
		theRoomTypePackage.createPackageContents();
		theSystemAccessPackage.createPackageContents();
		theBookingPackage.createPackageContents();
		theEconomicsPackage.createPackageContents();
		theRoomPackage.createPackageContents();
		theTypesPackage.createPackageContents();

		// Initialize created meta-data
		theRoomTypePackage.initializePackageContents();
		theSystemAccessPackage.initializePackageContents();
		theBookingPackage.initializePackageContents();
		theEconomicsPackage.initializePackageContents();
		theRoomPackage.initializePackageContents();
		theTypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRoomTypePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RoomTypePackage.eNS_URI, theRoomTypePackage);
		return theRoomTypePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomTypeProvider() {
		return iRoomTypeProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeProvider__CreateRoomType__String_int_int_EList() {
		return iRoomTypeProviderEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeProvider__UpdateRoomType__String_String_int_int_EList() {
		return iRoomTypeProviderEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeProvider__RemoveRoomType__String() {
		return iRoomTypeProviderEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeProvider__UpdateTypeOfRoom__String_String_int() {
		return iRoomTypeProviderEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeProvider__GetFreeRoomsOfType__String_int() {
		return iRoomTypeProviderEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeProvider__GetMaximumNumberOfRooms__String() {
		return iRoomTypeProviderEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeProvider__SetTypeOfRoom__String_int() {
		return iRoomTypeProviderEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeProvider__IsValid__String() {
		return iRoomTypeProviderEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeProvider__GetNumberOfFreeRooms__String() {
		return iRoomTypeProviderEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeProvider__GetNumberOfBeds__String() {
		return iRoomTypeProviderEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeProvider__GetPrice__String() {
		return iRoomTypeProviderEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeProvider__Reset() {
		return iRoomTypeProviderEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeProvider__GetTypeOfRoom__int() {
		return iRoomTypeProviderEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeProvider__ListRoomTypes() {
		return iRoomTypeProviderEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeProvider__ListRoomsOfType__String() {
		return iRoomTypeProviderEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeProvider__RemoveRoom__int() {
		return iRoomTypeProviderEClass.getEOperations().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeature() {
		return featureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomType() {
		return roomTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_Price() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_NumberOfBeds() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomType_Features() {
		return (EReference)roomTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_AssociatedRooms() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_Description() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomTypeTable() {
		return iRoomTypeTableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeTable__AddRoomType__String() {
		return iRoomTypeTableEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeTable__RemoveRoomType__String() {
		return iRoomTypeTableEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeTable__ListRoomTypes() {
		return iRoomTypeTableEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeTable__UpdateTypeOfRoom__String_String_int() {
		return iRoomTypeTableEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeTable__GetFreeRoomsOfType__String_int() {
		return iRoomTypeTableEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeTable__SetTypeOfRoom__String_int() {
		return iRoomTypeTableEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeTable__IsValid__String() {
		return iRoomTypeTableEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeTable__CreateRoomType__String_int_int_EList() {
		return iRoomTypeTableEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeTable__GetMaximumNumberOfRooms__String() {
		return iRoomTypeTableEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeTable__GetNumberOfFreeRooms__String() {
		return iRoomTypeTableEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeTable__GetNumberOfBeds__String() {
		return iRoomTypeTableEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeTable__GetPrice__String() {
		return iRoomTypeTableEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeTable__Reset() {
		return iRoomTypeTableEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeTable__UpdateRoomType__String_String_int_int_EList() {
		return iRoomTypeTableEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeTable__GetTypeOfRoom__int() {
		return iRoomTypeTableEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeTable__ListRoomsOfType__String() {
		return iRoomTypeTableEClass.getEOperations().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeTable__RemoveRoom__int() {
		return iRoomTypeTableEClass.getEOperations().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomTypeTable() {
		return roomTypeTableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomTypeTable_RoomTypes() {
		return (EReference)roomTypeTableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomTypeProvider() {
		return roomTypeProviderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomTypeProvider_RoomTypeTable() {
		return (EReference)roomTypeProviderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomTypeComparator() {
		return roomTypeComparatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoomTypeComparator__Compare__IRoomType_IRoomType() {
		return roomTypeComparatorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFreeRoomTypesDTO() {
		return freeRoomTypesDTOEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFreeRoomTypesDTO_RoomTypeDescription() {
		return (EAttribute)freeRoomTypesDTOEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFreeRoomTypesDTO_NumBeds() {
		return (EAttribute)freeRoomTypesDTOEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFreeRoomTypesDTO_PricePerNight() {
		return (EAttribute)freeRoomTypesDTOEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFreeRoomTypesDTO_NumFreeRooms() {
		return (EAttribute)freeRoomTypesDTOEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getFreeRoomTypesDTO__Init__String() {
		return freeRoomTypesDTOEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomType() {
		return iRoomTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__UpdateRoomType__String_int_int_EList() {
		return iRoomTypeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__GetDescription() {
		return iRoomTypeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__GetAssociatedRooms() {
		return iRoomTypeEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__GetPrice() {
		return iRoomTypeEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__GetNumberOfBeds() {
		return iRoomTypeEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__MaximumNumberOfRooms() {
		return iRoomTypeEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeFactory getRoomTypeFactory() {
		return (RoomTypeFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		iRoomTypeProviderEClass = createEClass(IROOM_TYPE_PROVIDER);
		createEOperation(iRoomTypeProviderEClass, IROOM_TYPE_PROVIDER___CREATE_ROOM_TYPE__STRING_INT_INT_ELIST);
		createEOperation(iRoomTypeProviderEClass, IROOM_TYPE_PROVIDER___UPDATE_ROOM_TYPE__STRING_STRING_INT_INT_ELIST);
		createEOperation(iRoomTypeProviderEClass, IROOM_TYPE_PROVIDER___REMOVE_ROOM_TYPE__STRING);
		createEOperation(iRoomTypeProviderEClass, IROOM_TYPE_PROVIDER___UPDATE_TYPE_OF_ROOM__STRING_STRING_INT);
		createEOperation(iRoomTypeProviderEClass, IROOM_TYPE_PROVIDER___GET_FREE_ROOMS_OF_TYPE__STRING_INT);
		createEOperation(iRoomTypeProviderEClass, IROOM_TYPE_PROVIDER___GET_MAXIMUM_NUMBER_OF_ROOMS__STRING);
		createEOperation(iRoomTypeProviderEClass, IROOM_TYPE_PROVIDER___SET_TYPE_OF_ROOM__STRING_INT);
		createEOperation(iRoomTypeProviderEClass, IROOM_TYPE_PROVIDER___IS_VALID__STRING);
		createEOperation(iRoomTypeProviderEClass, IROOM_TYPE_PROVIDER___GET_NUMBER_OF_FREE_ROOMS__STRING);
		createEOperation(iRoomTypeProviderEClass, IROOM_TYPE_PROVIDER___GET_NUMBER_OF_BEDS__STRING);
		createEOperation(iRoomTypeProviderEClass, IROOM_TYPE_PROVIDER___GET_PRICE__STRING);
		createEOperation(iRoomTypeProviderEClass, IROOM_TYPE_PROVIDER___RESET);
		createEOperation(iRoomTypeProviderEClass, IROOM_TYPE_PROVIDER___GET_TYPE_OF_ROOM__INT);
		createEOperation(iRoomTypeProviderEClass, IROOM_TYPE_PROVIDER___LIST_ROOM_TYPES);
		createEOperation(iRoomTypeProviderEClass, IROOM_TYPE_PROVIDER___LIST_ROOMS_OF_TYPE__STRING);
		createEOperation(iRoomTypeProviderEClass, IROOM_TYPE_PROVIDER___REMOVE_ROOM__INT);

		featureEClass = createEClass(FEATURE);

		roomTypeEClass = createEClass(ROOM_TYPE);
		createEAttribute(roomTypeEClass, ROOM_TYPE__PRICE);
		createEAttribute(roomTypeEClass, ROOM_TYPE__NUMBER_OF_BEDS);
		createEReference(roomTypeEClass, ROOM_TYPE__FEATURES);
		createEAttribute(roomTypeEClass, ROOM_TYPE__ASSOCIATED_ROOMS);
		createEAttribute(roomTypeEClass, ROOM_TYPE__DESCRIPTION);

		iRoomTypeEClass = createEClass(IROOM_TYPE);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___UPDATE_ROOM_TYPE__STRING_INT_INT_ELIST);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___GET_DESCRIPTION);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___GET_ASSOCIATED_ROOMS);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___GET_PRICE);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___GET_NUMBER_OF_BEDS);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___MAXIMUM_NUMBER_OF_ROOMS);

		iRoomTypeTableEClass = createEClass(IROOM_TYPE_TABLE);
		createEOperation(iRoomTypeTableEClass, IROOM_TYPE_TABLE___ADD_ROOM_TYPE__STRING);
		createEOperation(iRoomTypeTableEClass, IROOM_TYPE_TABLE___REMOVE_ROOM_TYPE__STRING);
		createEOperation(iRoomTypeTableEClass, IROOM_TYPE_TABLE___LIST_ROOM_TYPES);
		createEOperation(iRoomTypeTableEClass, IROOM_TYPE_TABLE___UPDATE_TYPE_OF_ROOM__STRING_STRING_INT);
		createEOperation(iRoomTypeTableEClass, IROOM_TYPE_TABLE___GET_FREE_ROOMS_OF_TYPE__STRING_INT);
		createEOperation(iRoomTypeTableEClass, IROOM_TYPE_TABLE___SET_TYPE_OF_ROOM__STRING_INT);
		createEOperation(iRoomTypeTableEClass, IROOM_TYPE_TABLE___IS_VALID__STRING);
		createEOperation(iRoomTypeTableEClass, IROOM_TYPE_TABLE___CREATE_ROOM_TYPE__STRING_INT_INT_ELIST);
		createEOperation(iRoomTypeTableEClass, IROOM_TYPE_TABLE___GET_MAXIMUM_NUMBER_OF_ROOMS__STRING);
		createEOperation(iRoomTypeTableEClass, IROOM_TYPE_TABLE___GET_NUMBER_OF_FREE_ROOMS__STRING);
		createEOperation(iRoomTypeTableEClass, IROOM_TYPE_TABLE___GET_NUMBER_OF_BEDS__STRING);
		createEOperation(iRoomTypeTableEClass, IROOM_TYPE_TABLE___GET_PRICE__STRING);
		createEOperation(iRoomTypeTableEClass, IROOM_TYPE_TABLE___RESET);
		createEOperation(iRoomTypeTableEClass, IROOM_TYPE_TABLE___UPDATE_ROOM_TYPE__STRING_STRING_INT_INT_ELIST);
		createEOperation(iRoomTypeTableEClass, IROOM_TYPE_TABLE___GET_TYPE_OF_ROOM__INT);
		createEOperation(iRoomTypeTableEClass, IROOM_TYPE_TABLE___LIST_ROOMS_OF_TYPE__STRING);
		createEOperation(iRoomTypeTableEClass, IROOM_TYPE_TABLE___REMOVE_ROOM__INT);

		roomTypeTableEClass = createEClass(ROOM_TYPE_TABLE);
		createEReference(roomTypeTableEClass, ROOM_TYPE_TABLE__ROOM_TYPES);

		roomTypeProviderEClass = createEClass(ROOM_TYPE_PROVIDER);
		createEReference(roomTypeProviderEClass, ROOM_TYPE_PROVIDER__ROOM_TYPE_TABLE);

		roomTypeComparatorEClass = createEClass(ROOM_TYPE_COMPARATOR);
		createEOperation(roomTypeComparatorEClass, ROOM_TYPE_COMPARATOR___COMPARE__IROOMTYPE_IROOMTYPE);

		freeRoomTypesDTOEClass = createEClass(FREE_ROOM_TYPES_DTO);
		createEAttribute(freeRoomTypesDTOEClass, FREE_ROOM_TYPES_DTO__ROOM_TYPE_DESCRIPTION);
		createEAttribute(freeRoomTypesDTOEClass, FREE_ROOM_TYPES_DTO__NUM_BEDS);
		createEAttribute(freeRoomTypesDTOEClass, FREE_ROOM_TYPES_DTO__PRICE_PER_NIGHT);
		createEAttribute(freeRoomTypesDTOEClass, FREE_ROOM_TYPES_DTO__NUM_FREE_ROOMS);
		createEOperation(freeRoomTypesDTOEClass, FREE_ROOM_TYPES_DTO___INIT__STRING);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		roomTypeEClass.getESuperTypes().add(this.getIRoomType());
		roomTypeTableEClass.getESuperTypes().add(this.getIRoomTypeTable());
		roomTypeProviderEClass.getESuperTypes().add(this.getIRoomTypeProvider());

		// Initialize classes, features, and operations; add parameters
		initEClass(iRoomTypeProviderEClass, IRoomTypeProvider.class, "IRoomTypeProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getIRoomTypeProvider__CreateRoomType__String_int_int_EList(), ecorePackage.getEBoolean(), "createRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "numberOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getFeature(), "feature", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeProvider__UpdateRoomType__String_String_int_int_EList(), ecorePackage.getEBoolean(), "updateRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "toUpdate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "numberOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getFeature(), "feature", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeProvider__RemoveRoomType__String(), ecorePackage.getEBoolean(), "removeRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "toRemove", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeProvider__UpdateTypeOfRoom__String_String_int(), ecorePackage.getEBoolean(), "updateTypeOfRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "change", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "newType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeProvider__GetFreeRoomsOfType__String_int(), ecorePackage.getEInt(), "getFreeRoomsOfType", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "desiredNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeProvider__GetMaximumNumberOfRooms__String(), ecorePackage.getEInt(), "getMaximumNumberOfRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeProvider__SetTypeOfRoom__String_int(), ecorePackage.getEBoolean(), "setTypeOfRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeProvider__IsValid__String(), ecorePackage.getEBoolean(), "isValid", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeProvider__GetNumberOfFreeRooms__String(), ecorePackage.getEInt(), "getNumberOfFreeRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeProvider__GetNumberOfBeds__String(), ecorePackage.getEInt(), "getNumberOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeProvider__GetPrice__String(), ecorePackage.getEInt(), "getPrice", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomTypeProvider__Reset(), ecorePackage.getEBoolean(), "reset", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeProvider__GetTypeOfRoom__int(), ecorePackage.getEString(), "getTypeOfRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomTypeProvider__ListRoomTypes(), ecorePackage.getEString(), "listRoomTypes", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeProvider__ListRoomsOfType__String(), ecorePackage.getEInt(), "listRoomsOfType", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeProvider__RemoveRoom__int(), ecorePackage.getEBoolean(), "removeRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(featureEClass, Feature.class, "Feature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(roomTypeEClass, RoomType.class, "RoomType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRoomType_Price(), ecorePackage.getEInt(), "price", null, 1, 1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomType_NumberOfBeds(), ecorePackage.getEInt(), "numberOfBeds", null, 1, 1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRoomType_Features(), this.getFeature(), null, "features", null, 0, -1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomType_AssociatedRooms(), ecorePackage.getEInt(), "associatedRooms", null, 0, -1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomType_Description(), ecorePackage.getEString(), "description", null, 1, 1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iRoomTypeEClass, IRoomType.class, "IRoomType", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIRoomType__UpdateRoomType__String_int_int_EList(), null, "updateRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "numberOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getFeature(), "features", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomType__GetDescription(), ecorePackage.getEString(), "getDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomType__GetAssociatedRooms(), ecorePackage.getEInt(), "getAssociatedRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomType__GetPrice(), ecorePackage.getEInt(), "getPrice", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomType__GetNumberOfBeds(), ecorePackage.getEInt(), "getNumberOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomType__MaximumNumberOfRooms(), ecorePackage.getEInt(), "maximumNumberOfRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iRoomTypeTableEClass, IRoomTypeTable.class, "IRoomTypeTable", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIRoomTypeTable__AddRoomType__String(), ecorePackage.getEBoolean(), "addRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeTable__RemoveRoomType__String(), ecorePackage.getEBoolean(), "removeRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomTypeTable__ListRoomTypes(), ecorePackage.getEString(), "listRoomTypes", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeTable__UpdateTypeOfRoom__String_String_int(), ecorePackage.getEBoolean(), "updateTypeOfRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "change", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "newType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeTable__GetFreeRoomsOfType__String_int(), ecorePackage.getEInt(), "getFreeRoomsOfType", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "desiredNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeTable__SetTypeOfRoom__String_int(), ecorePackage.getEBoolean(), "setTypeOfRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeTable__IsValid__String(), ecorePackage.getEBoolean(), "isValid", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeTable__CreateRoomType__String_int_int_EList(), ecorePackage.getEBoolean(), "createRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "numberOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getFeature(), "features", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeTable__GetMaximumNumberOfRooms__String(), ecorePackage.getEInt(), "getMaximumNumberOfRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeTable__GetNumberOfFreeRooms__String(), ecorePackage.getEInt(), "getNumberOfFreeRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeTable__GetNumberOfBeds__String(), ecorePackage.getEInt(), "getNumberOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeTable__GetPrice__String(), ecorePackage.getEInt(), "getPrice", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomTypeTable__Reset(), ecorePackage.getEBoolean(), "reset", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeTable__UpdateRoomType__String_String_int_int_EList(), ecorePackage.getEBoolean(), "updateRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "toUpdate", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "numberOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getFeature(), "feature", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeTable__GetTypeOfRoom__int(), ecorePackage.getEString(), "getTypeOfRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeTable__ListRoomsOfType__String(), ecorePackage.getEInt(), "listRoomsOfType", 0, -1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeTable__RemoveRoom__int(), ecorePackage.getEBoolean(), "removeRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNumber", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(roomTypeTableEClass, RoomTypeTable.class, "RoomTypeTable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoomTypeTable_RoomTypes(), this.getIRoomType(), null, "roomTypes", null, 0, -1, RoomTypeTable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(roomTypeProviderEClass, RoomTypeProvider.class, "RoomTypeProvider", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoomTypeProvider_RoomTypeTable(), this.getRoomTypeTable(), null, "roomTypeTable", null, 1, 1, RoomTypeProvider.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(roomTypeComparatorEClass, RoomTypeComparator.class, "RoomTypeComparator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getRoomTypeComparator__Compare__IRoomType_IRoomType(), ecorePackage.getEInt(), "compare", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getIRoomType(), "room1", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getIRoomType(), "room2", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(freeRoomTypesDTOEClass, FreeRoomTypesDTO.class, "FreeRoomTypesDTO", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFreeRoomTypesDTO_RoomTypeDescription(), ecorePackage.getEString(), "roomTypeDescription", null, 1, 1, FreeRoomTypesDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getFreeRoomTypesDTO_NumBeds(), ecorePackage.getEInt(), "numBeds", null, 1, 1, FreeRoomTypesDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getFreeRoomTypesDTO_PricePerNight(), ecorePackage.getEDouble(), "pricePerNight", null, 1, 1, FreeRoomTypesDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getFreeRoomTypesDTO_NumFreeRooms(), ecorePackage.getEInt(), "numFreeRooms", null, 1, 1, FreeRoomTypesDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		op = initEOperation(getFreeRoomTypesDTO__Init__String(), ecorePackage.getEBoolean(), "init", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "roomTypeDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //RoomTypePackageImpl
