/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.RoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeProvider#getRoomTypeTable <em>Room Type Table</em>}</li>
 * </ul>
 *
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypePackage#getRoomTypeProvider()
 * @model
 * @generated
 */
public interface RoomTypeProvider extends IRoomTypeProvider {

	/**
	 * Returns the value of the '<em><b>Room Type Table</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type Table</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type Table</em>' reference.
	 * @see #setRoomTypeTable(RoomTypeTable)
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypePackage#getRoomTypeProvider_RoomTypeTable()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomTypeTable getRoomTypeTable();

	/**
	 * Sets the value of the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeProvider#getRoomTypeTable <em>Room Type Table</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type Table</em>' reference.
	 * @see #getRoomTypeTable()
	 * @generated
	 */
	void setRoomTypeTable(RoomTypeTable value);
} // RoomTypeProvider
