/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomStatus;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.Feature;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomType;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypePackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeTable;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Table</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypeTableImpl#getRoomTypes <em>Room Types</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomTypeTableImpl extends MinimalEObjectImpl.Container implements RoomTypeTable {
	/**
	 * The cached value of the '{@link #getRoomTypes() <em>Room Types</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<IRoomType> roomTypes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected RoomTypeTableImpl() {
		super();
		roomTypes = new BasicEList<IRoomType>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomTypePackage.Literals.ROOM_TYPE_TABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IRoomType> getRoomTypes() {
		if (roomTypes == null) {
			roomTypes = new EObjectResolvingEList<IRoomType>(IRoomType.class, this, RoomTypePackage.ROOM_TYPE_TABLE__ROOM_TYPES);
		}
		return roomTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return 
	 * @generated NOT
	 */
	public boolean addRoomType(String roomType) {
		RoomType rt = chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeFactory.eINSTANCE.createRoomType();
		rt.setDescription(roomType);

		return roomTypes.add(rt);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoomType(String roomType) {
		IRoomType rt = getRoomType(roomType);
		if(rt==null){
			return false;
		}
		if(((RoomType) rt).getAssociatedRooms().isEmpty()){
				return roomTypes.remove(rt);
		}

		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<String> listRoomTypes() {
		EList<String> list = new BasicEList<String>();
		for(IRoomType rt:roomTypes){
			list.add(rt.getDescription());
		}
		return list;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateTypeOfRoom(String change, String newType, int roomNumber) {
		IRoomType rt1 = getRoomType(change);
		IRoomType rt2 = getRoomType(newType);
		if(rt1!=null && rt2!=null){
			EList<Integer> ar1 = rt1.getAssociatedRooms();
			
			if(ar1.contains(new Integer(roomNumber))){
				ar1.remove(new Integer(roomNumber));
				rt2.getAssociatedRooms().add(new Integer(roomNumber));
				return true;
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> getFreeRoomsOfType(String roomTypeDescription, int desiredNumber) {
		IRoomType rt = getRoomType(roomTypeDescription);
		EList<Integer> rooms = rt.getAssociatedRooms();
		RoomProvider rp = RoomFactory.eINSTANCE.createRoomProvider();
		EList<Integer> freeRooms = new BasicEList<Integer>();
		int added = 0;
		for(Integer i:rooms){
			if(rp.getStatus(i)==RoomStatus.FREE){
				freeRooms.add(i);
				added++;
			}
			if(added==desiredNumber){
				break;
			}
		}
		
		return freeRooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getMaximumNumberOfRooms(String roomTypeDescription) {
		IRoomType rt = getRoomType(roomTypeDescription);
		if (rt == null){
			return 0;
		}
		return rt.maximumNumberOfRooms();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getNumberOfFreeRooms(String roomTypeDescription) {
		IRoomType rt = getRoomType(roomTypeDescription);
		EList<Integer> rooms = rt.getAssociatedRooms();
		RoomProvider rp = RoomFactory.eINSTANCE.createRoomProvider();
		
		int freeRooms = 0;
		for(Integer i:rooms){
			if(rp.getStatus(i)==RoomStatus.FREE){
				freeRooms++;
			}
		}
		
		return freeRooms;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getNumberOfBeds(String roomTypeDescription) {
		return getRoomType(roomTypeDescription).getNumberOfBeds();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getPrice(String roomTypeDescription) {
		return getRoomType(roomTypeDescription).getPrice();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean reset() {
		this.roomTypes = new BasicEList<IRoomType>();
				
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean updateRoomType(String toUpdate, String name, int price, int numberOfBeds, EList<Feature> feature) {
		IRoomType rt = getRoomType(toUpdate);
		if(rt!=null){
			rt.updateRoomType(name, price, numberOfBeds, feature);
			return true;
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getTypeOfRoom(int roomNumber) {
		for (IRoomType irt : roomTypes){
			if (irt.getAssociatedRooms().contains(roomNumber)) return irt.getDescription();
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Integer> listRoomsOfType(String roomTypeDescription) {
		IRoomType rt = getRoomType(roomTypeDescription);
		return rt.getAssociatedRooms();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeRoom(int roomNumber) {		
		IRoomProvider rp = RoomFactory.eINSTANCE.createRoomProvider();
		boolean removed = rp.destroyRoom(roomNumber);
		
		if(!removed){
			return false;
		}
		
		for(IRoomType roomType : roomTypes){
			roomType.getAssociatedRooms().remove(new Integer(roomNumber));
		}
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean setTypeOfRoom(String roomTypeDescription, int roomNumber) {
		IRoomType rt = getRoomType(roomTypeDescription);
		if(rt == null){
			return false;
		}
		
		Integer rn = new Integer(roomNumber);
		EList<Integer> ar = rt.getAssociatedRooms();
		
		if(ar.contains(rn)){
			return false;
		}
		
		for(IRoomType rt2: roomTypes){
			EList<Integer> ar2 = rt2.getAssociatedRooms();
			if(ar2.contains(rn)){
				ar2.remove(rn);
			}
		}
		
		ar.add(new Integer(roomNumber));
		
		return true;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isValid(String roomTypeDescription) {
		return getRoomType(roomTypeDescription)!=null;	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */

	public boolean createRoomType(String name, int price, int numberOfBeds, EList<Feature> features) {
		RoomType rt = RoomTypeFactory.eINSTANCE.createRoomType();
		rt.setDescription(name);
		rt.setPrice(price);
		rt.setNumberOfBeds(numberOfBeds);
		EList<Feature> l = rt.getFeatures();
		l.addAll(features);
		
		roomTypes.add(rt);
		
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomTypePackage.ROOM_TYPE_TABLE__ROOM_TYPES:
				return getRoomTypes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomTypePackage.ROOM_TYPE_TABLE__ROOM_TYPES:
				getRoomTypes().clear();
				getRoomTypes().addAll((Collection<? extends IRoomType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomTypePackage.ROOM_TYPE_TABLE__ROOM_TYPES:
				getRoomTypes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomTypePackage.ROOM_TYPE_TABLE__ROOM_TYPES:
				return roomTypes != null && !roomTypes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomTypePackage.ROOM_TYPE_TABLE___ADD_ROOM_TYPE__STRING:
				return addRoomType((String)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_TABLE___REMOVE_ROOM_TYPE__STRING:
				return removeRoomType((String)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_TABLE___LIST_ROOM_TYPES:
				return listRoomTypes();
			case RoomTypePackage.ROOM_TYPE_TABLE___UPDATE_TYPE_OF_ROOM__STRING_STRING_INT:
				return updateTypeOfRoom((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2));
			case RoomTypePackage.ROOM_TYPE_TABLE___GET_FREE_ROOMS_OF_TYPE__STRING_INT:
				return getFreeRoomsOfType((String)arguments.get(0), (Integer)arguments.get(1));
			case RoomTypePackage.ROOM_TYPE_TABLE___SET_TYPE_OF_ROOM__STRING_INT:
				return setTypeOfRoom((String)arguments.get(0), (Integer)arguments.get(1));
			case RoomTypePackage.ROOM_TYPE_TABLE___IS_VALID__STRING:
				return isValid((String)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_TABLE___CREATE_ROOM_TYPE__STRING_INT_INT_ELIST:
				return createRoomType((String)arguments.get(0), (Integer)arguments.get(1), (Integer)arguments.get(2), (EList<Feature>)arguments.get(3));
			case RoomTypePackage.ROOM_TYPE_TABLE___GET_MAXIMUM_NUMBER_OF_ROOMS__STRING:
				return getMaximumNumberOfRooms((String)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_TABLE___GET_NUMBER_OF_FREE_ROOMS__STRING:
				return getNumberOfFreeRooms((String)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_TABLE___GET_NUMBER_OF_BEDS__STRING:
				return getNumberOfBeds((String)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_TABLE___GET_PRICE__STRING:
				return getPrice((String)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_TABLE___RESET:
				return reset();
			case RoomTypePackage.ROOM_TYPE_TABLE___UPDATE_ROOM_TYPE__STRING_STRING_INT_INT_ELIST:
				return updateRoomType((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (EList<Feature>)arguments.get(4));
			case RoomTypePackage.ROOM_TYPE_TABLE___GET_TYPE_OF_ROOM__INT:
				return getTypeOfRoom((Integer)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_TABLE___LIST_ROOMS_OF_TYPE__STRING:
				return listRoomsOfType((String)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_TABLE___REMOVE_ROOM__INT:
				return removeRoom((Integer)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}
	
	private IRoomType getRoomType(String roomTypeDescription){
		for(IRoomType r : roomTypes){
			if(r.getDescription().equals(roomTypeDescription)){
				return r;
			}
		}
		return null;
	}

} //RoomTypeTableImpl
