/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomType;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeComparator;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypePackage;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Comparator</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RoomTypeComparatorImpl extends MinimalEObjectImpl.Container implements RoomTypeComparator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomTypeComparatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomTypePackage.Literals.ROOM_TYPE_COMPARATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int compare(IRoomType room1, IRoomType room2) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomTypePackage.ROOM_TYPE_COMPARATOR___COMPARE__IROOMTYPE_IROOMTYPE:
				return compare((IRoomType)arguments.get(0), (IRoomType)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomTypeComparatorImpl
