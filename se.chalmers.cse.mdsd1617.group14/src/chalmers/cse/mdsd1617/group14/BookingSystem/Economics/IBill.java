/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Economics;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IBill</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.EconomicsPackage#getIBill()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IBill extends EObject {

} // IBill
