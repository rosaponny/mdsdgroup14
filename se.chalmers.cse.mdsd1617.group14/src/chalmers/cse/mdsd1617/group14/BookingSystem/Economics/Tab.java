/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Economics;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tab</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.Tab#getBill <em>Bill</em>}</li>
 * </ul>
 *
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.EconomicsPackage#getTab()
 * @model
 * @generated
 */
public interface Tab extends ITab {
	/**
	 * Returns the value of the '<em><b>Bill</b></em>' reference list.
	 * The list contents are of type {@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bill</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bill</em>' reference list.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.EconomicsPackage#getTab_Bill()
	 * @model ordered="false"
	 * @generated
	 */
	EList<IBill> getBill();

} // Tab
