/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingPackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.impl.BookingPackageImpl;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.Bill;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.EconomicsFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.EconomicsPackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.Tab;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomPackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.impl.RoomPackageImpl;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypePackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.impl.RoomTypePackageImpl;
import chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessPackage;
import chalmers.cse.mdsd1617.group14.SystemAccess.impl.SystemAccessPackageImpl;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.types.impl.TypesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EconomicsPackageImpl extends EPackageImpl implements EconomicsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tabEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass billEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iTabEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iBillEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.EconomicsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private EconomicsPackageImpl() {
		super(eNS_URI, EconomicsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link EconomicsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static EconomicsPackage init() {
		if (isInited) return (EconomicsPackage)EPackage.Registry.INSTANCE.getEPackage(EconomicsPackage.eNS_URI);

		// Obtain or create and register package
		EconomicsPackageImpl theEconomicsPackage = (EconomicsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof EconomicsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new EconomicsPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		SystemAccessPackageImpl theSystemAccessPackage = (SystemAccessPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SystemAccessPackage.eNS_URI) instanceof SystemAccessPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SystemAccessPackage.eNS_URI) : SystemAccessPackage.eINSTANCE);
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) : BookingPackage.eINSTANCE);
		RoomTypePackageImpl theRoomTypePackage = (RoomTypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI) instanceof RoomTypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI) : RoomTypePackage.eINSTANCE);
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) : RoomPackage.eINSTANCE);
		TypesPackageImpl theTypesPackage = (TypesPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) instanceof TypesPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI) : TypesPackage.eINSTANCE);

		// Create package meta-data objects
		theEconomicsPackage.createPackageContents();
		theSystemAccessPackage.createPackageContents();
		theBookingPackage.createPackageContents();
		theRoomTypePackage.createPackageContents();
		theRoomPackage.createPackageContents();
		theTypesPackage.createPackageContents();

		// Initialize created meta-data
		theEconomicsPackage.initializePackageContents();
		theSystemAccessPackage.initializePackageContents();
		theBookingPackage.initializePackageContents();
		theRoomTypePackage.initializePackageContents();
		theRoomPackage.initializePackageContents();
		theTypesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theEconomicsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(EconomicsPackage.eNS_URI, theEconomicsPackage);
		return theEconomicsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTab() {
		return tabEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTab_Bill() {
		return (EReference)tabEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBill() {
		return billEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBill_Description() {
		return (EAttribute)billEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBill_Cost() {
		return (EAttribute)billEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getITab() {
		return iTabEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getITab__AddBill__IBill() {
		return iTabEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getITab__Present() {
		return iTabEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getITab__CalculateTotal() {
		return iTabEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getITab__RemoveBill__IBill() {
		return iTabEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIBill() {
		return iBillEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EconomicsFactory getEconomicsFactory() {
		return (EconomicsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		tabEClass = createEClass(TAB);
		createEReference(tabEClass, TAB__BILL);

		iBillEClass = createEClass(IBILL);

		billEClass = createEClass(BILL);
		createEAttribute(billEClass, BILL__DESCRIPTION);
		createEAttribute(billEClass, BILL__COST);

		iTabEClass = createEClass(ITAB);
		createEOperation(iTabEClass, ITAB___ADD_BILL__IBILL);
		createEOperation(iTabEClass, ITAB___PRESENT);
		createEOperation(iTabEClass, ITAB___CALCULATE_TOTAL);
		createEOperation(iTabEClass, ITAB___REMOVE_BILL__IBILL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		tabEClass.getESuperTypes().add(this.getITab());
		billEClass.getESuperTypes().add(this.getIBill());

		// Initialize classes, features, and operations; add parameters
		initEClass(tabEClass, Tab.class, "Tab", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTab_Bill(), this.getIBill(), null, "bill", null, 0, -1, Tab.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iBillEClass, IBill.class, "IBill", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(billEClass, Bill.class, "Bill", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBill_Description(), theTypesPackage.getString(), "description", null, 1, 1, Bill.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getBill_Cost(), ecorePackage.getEInt(), "cost", null, 1, 1, Bill.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iTabEClass, ITab.class, "ITab", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getITab__AddBill__IBill(), ecorePackage.getEBoolean(), "addBill", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getIBill(), "bill", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getITab__Present(), null, "present", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getITab__CalculateTotal(), ecorePackage.getEInt(), "calculateTotal", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getITab__RemoveBill__IBill(), ecorePackage.getEBoolean(), "removeBill", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getIBill(), "bill", 1, 1, IS_UNIQUE, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //EconomicsPackageImpl
