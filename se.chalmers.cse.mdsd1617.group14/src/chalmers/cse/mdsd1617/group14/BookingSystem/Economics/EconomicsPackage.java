/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Economics;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.EconomicsFactory
 * @model kind="package"
 * @generated
 */
public interface EconomicsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Economics";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///chalmers/cse/mdsd1617/group14/BookingSystem/Economics.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "chalmers.cse.mdsd1617.group14.BookingSystem.Economics";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	EconomicsPackage eINSTANCE = chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.EconomicsPackageImpl.init();

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.TabImpl <em>Tab</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.TabImpl
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.EconomicsPackageImpl#getTab()
	 * @generated
	 */
	int TAB = 0;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.BillImpl <em>Bill</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.BillImpl
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.EconomicsPackageImpl#getBill()
	 * @generated
	 */
	int BILL = 2;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab <em>ITab</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.EconomicsPackageImpl#getITab()
	 * @generated
	 */
	int ITAB = 3;

	/**
	 * The number of structural features of the '<em>ITab</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITAB_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Bill</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITAB___ADD_BILL__IBILL = 0;

	/**
	 * The operation id for the '<em>Present</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITAB___PRESENT = 1;

	/**
	 * The operation id for the '<em>Calculate Total</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITAB___CALCULATE_TOTAL = 2;

	/**
	 * The operation id for the '<em>Remove Bill</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITAB___REMOVE_BILL__IBILL = 3;

	/**
	 * The number of operations of the '<em>ITab</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITAB_OPERATION_COUNT = 4;

	/**
	 * The feature id for the '<em><b>Bill</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAB__BILL = ITAB_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Tab</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAB_FEATURE_COUNT = ITAB_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Add Bill</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAB___ADD_BILL__IBILL = ITAB___ADD_BILL__IBILL;

	/**
	 * The operation id for the '<em>Present</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAB___PRESENT = ITAB___PRESENT;

	/**
	 * The operation id for the '<em>Calculate Total</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAB___CALCULATE_TOTAL = ITAB___CALCULATE_TOTAL;

	/**
	 * The operation id for the '<em>Remove Bill</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAB___REMOVE_BILL__IBILL = ITAB___REMOVE_BILL__IBILL;

	/**
	 * The number of operations of the '<em>Tab</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TAB_OPERATION_COUNT = ITAB_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill <em>IBill</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.EconomicsPackageImpl#getIBill()
	 * @generated
	 */
	int IBILL = 1;

	/**
	 * The number of structural features of the '<em>IBill</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBILL_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>IBill</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBILL_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BILL__DESCRIPTION = IBILL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BILL__COST = IBILL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Bill</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BILL_FEATURE_COUNT = IBILL_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Bill</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BILL_OPERATION_COUNT = IBILL_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.Tab <em>Tab</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tab</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.Tab
	 * @generated
	 */
	EClass getTab();

	/**
	 * Returns the meta object for the reference list '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.Tab#getBill <em>Bill</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Bill</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.Tab#getBill()
	 * @see #getTab()
	 * @generated
	 */
	EReference getTab_Bill();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.Bill <em>Bill</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bill</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.Bill
	 * @generated
	 */
	EClass getBill();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.Bill#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.Bill#getDescription()
	 * @see #getBill()
	 * @generated
	 */
	EAttribute getBill_Description();

	/**
	 * Returns the meta object for the attribute '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.Bill#getCost <em>Cost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cost</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.Bill#getCost()
	 * @see #getBill()
	 * @generated
	 */
	EAttribute getBill_Cost();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab <em>ITab</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ITab</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab
	 * @generated
	 */
	EClass getITab();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab#addBill(chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill) <em>Add Bill</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Bill</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab#addBill(chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill)
	 * @generated
	 */
	EOperation getITab__AddBill__IBill();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab#present() <em>Present</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Present</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab#present()
	 * @generated
	 */
	EOperation getITab__Present();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab#calculateTotal() <em>Calculate Total</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Calculate Total</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab#calculateTotal()
	 * @generated
	 */
	EOperation getITab__CalculateTotal();

	/**
	 * Returns the meta object for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab#removeBill(chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill) <em>Remove Bill</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Bill</em>' operation.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab#removeBill(chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill)
	 * @generated
	 */
	EOperation getITab__RemoveBill__IBill();

	/**
	 * Returns the meta object for class '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill <em>IBill</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IBill</em>'.
	 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill
	 * @generated
	 */
	EClass getIBill();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	EconomicsFactory getEconomicsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.TabImpl <em>Tab</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.TabImpl
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.EconomicsPackageImpl#getTab()
		 * @generated
		 */
		EClass TAB = eINSTANCE.getTab();

		/**
		 * The meta object literal for the '<em><b>Bill</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TAB__BILL = eINSTANCE.getTab_Bill();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.BillImpl <em>Bill</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.BillImpl
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.EconomicsPackageImpl#getBill()
		 * @generated
		 */
		EClass BILL = eINSTANCE.getBill();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BILL__DESCRIPTION = eINSTANCE.getBill_Description();

		/**
		 * The meta object literal for the '<em><b>Cost</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BILL__COST = eINSTANCE.getBill_Cost();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab <em>ITab</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.ITab
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.EconomicsPackageImpl#getITab()
		 * @generated
		 */
		EClass ITAB = eINSTANCE.getITab();

		/**
		 * The meta object literal for the '<em><b>Add Bill</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ITAB___ADD_BILL__IBILL = eINSTANCE.getITab__AddBill__IBill();

		/**
		 * The meta object literal for the '<em><b>Present</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ITAB___PRESENT = eINSTANCE.getITab__Present();

		/**
		 * The meta object literal for the '<em><b>Calculate Total</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ITAB___CALCULATE_TOTAL = eINSTANCE.getITab__CalculateTotal();

		/**
		 * The meta object literal for the '<em><b>Remove Bill</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ITAB___REMOVE_BILL__IBILL = eINSTANCE.getITab__RemoveBill__IBill();

		/**
		 * The meta object literal for the '{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill <em>IBill</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill
		 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.EconomicsPackageImpl#getIBill()
		 * @generated
		 */
		EClass IBILL = eINSTANCE.getIBill();

	}

} //EconomicsPackage
