/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl;

import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.EconomicsPackage;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.IBill;
import chalmers.cse.mdsd1617.group14.BookingSystem.Economics.Tab;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tab</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link chalmers.cse.mdsd1617.group14.BookingSystem.Economics.impl.TabImpl#getBill <em>Bill</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TabImpl extends MinimalEObjectImpl.Container implements Tab {
	/**
	 * The cached value of the '{@link #getBill() <em>Bill</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBill()
	 * @generated
	 * @ordered
	 */
	protected EList<IBill> bill;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT 
	 */
	protected TabImpl() {
		super();
		this.bill = new BasicEList<IBill>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EconomicsPackage.Literals.TAB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IBill> getBill() {
		if (bill == null) {
			bill = new EObjectResolvingEList<IBill>(IBill.class, this, EconomicsPackage.TAB__BILL);
		}
		return bill;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addBill(IBill bill) {
		this.bill.add(bill);
		if(!this.bill.contains(bill)) {
			return false;
		}
		return true; 
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void present() {
		String calculatedCost = "Bills: ";
		for (IBill b:bill) {
			calculatedCost = calculatedCost + ", " + b.toString();
		}
		
		System.out.println(calculatedCost);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int calculateTotal() {
		float cost = 0; 
		for (IBill b: bill) {
			cost += ((BillImpl) b).getCost();
		}
		return (int)cost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean removeBill(IBill bill) {
		this.bill.remove(bill);
		if(this.bill.contains(bill)) {
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EconomicsPackage.TAB__BILL:
				return getBill();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EconomicsPackage.TAB__BILL:
				getBill().clear();
				getBill().addAll((Collection<? extends IBill>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EconomicsPackage.TAB__BILL:
				getBill().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EconomicsPackage.TAB__BILL:
				return bill != null && !bill.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case EconomicsPackage.TAB___ADD_BILL__IBILL:
				return addBill((IBill)arguments.get(0));
			case EconomicsPackage.TAB___PRESENT:
				present();
				return null;
			case EconomicsPackage.TAB___CALCULATE_TOTAL:
				return calculateTotal();
			case EconomicsPackage.TAB___REMOVE_BILL__IBILL:
				return removeBill((IBill)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //TabImpl
