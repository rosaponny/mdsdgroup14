/**
 */
package chalmers.cse.mdsd1617.group14.BookingSystem.Economics;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ITab</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see chalmers.cse.mdsd1617.group14.BookingSystem.Economics.EconomicsPackage#getITab()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ITab extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" billRequired="true" billOrdered="false"
	 * @generated
	 */
	boolean addBill(IBill bill);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void present();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int calculateTotal();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" billRequired="true" billOrdered="false"
	 * @generated
	 */
	boolean removeBill(IBill bill);

} // ITab
