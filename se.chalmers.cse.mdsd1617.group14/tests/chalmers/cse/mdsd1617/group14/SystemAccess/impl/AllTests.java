package chalmers.cse.mdsd1617.group14.SystemAccess.impl;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AdministratorTaskTest.class, ReceptionistTaskTests.class })
public class AllTests {
	
}
