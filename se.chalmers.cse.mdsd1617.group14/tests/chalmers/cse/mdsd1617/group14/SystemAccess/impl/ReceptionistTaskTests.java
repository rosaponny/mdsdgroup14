package chalmers.cse.mdsd1617.group14.SystemAccess.impl;

import static org.junit.Assert.*;

import java.net.ConnectException;

import javax.xml.soap.SOAPException;

import org.eclipse.emf.common.util.BasicEList;
import org.junit.Before;
import org.junit.Test;

import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.IBookingTable;
import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.TwoIntTuple;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.Feature;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.IRoomTypeProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeFactory;
import chalmers.cse.mdsd1617.group14.SystemAccess.HotelAdministratorProvides;
import chalmers.cse.mdsd1617.group14.SystemAccess.IHotelCustomerProvides;
import chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessFactory;

public class ReceptionistTaskTests {
	IHotelCustomerProvides hcp;
	HotelAdministratorProvides  hap; 
	HotelReceptionistProvidesImpl hrp;
	
	
	IRoomTypeProvider rtp;
	IBookingProvider bp;
	IRoomProvider rp; 
	IBookingTable bt;
	
	public static String defaultFirstName = "TestFirstName";
	public static String defaultLastName = "TestLastName";
	public static String defaultStartDate = "20161218";
	public static String defaultEndDate = "20161220";
	public static String dateLongInFuture1 = "20201012";
	public static String dateLongInFuture2 = "20201020";
	public static String defaultCardNumber = "14000100";
	public static String defaultCCVNumber = "123";

	
	
	@Before
	public void initialization() {
		hcp      = SystemAccessFactory.eINSTANCE.createHotelCustomerProvides();
		hap = SystemAccessFactory.eINSTANCE.createHotelAdministratorProvides();
		hap.startup(5);
		
		rp      = RoomFactory.eINSTANCE.createRoomProvider();
		rtp 	= RoomTypeFactory.eINSTANCE.createRoomTypeProvider();
		bp		= BookingFactory.eINSTANCE.createBookingProvider();
		bt 		= BookingFactory.eINSTANCE.createBookingTable();
		
		rtp.createRoomType("Room1", 10, 2, new BasicEList<Feature>());
		rtp.createRoomType("Room2", 100, 3, new BasicEList<Feature>());
		rtp.updateTypeOfRoom("Default", "Room1", 0);
		rtp.updateTypeOfRoom("Room2", "Default", 1);
		hrp = (HotelReceptionistProvidesImpl) SystemAccessFactory.eINSTANCE.createHotelReceptionistProvides();
	}
	

	@Test 
	public void makeBookingTest() { //Use case 2.1.1
		int bookingID = hrp.initiateBooking(defaultFirstName, defaultStartDate,defaultEndDate,defaultLastName);
		assertTrue(bookingID != -1);
		hrp.addRoomToBooking("Default", bookingID);
		assertTrue(hrp.confirmBooking(bookingID));
		assertTrue(hrp.initiateBooking("Failure", "NotADay", "NotAnotherDay", "Failure") == -1); // Check that you can't add a booking with an invalid day.
		assertTrue(hrp.initiateBooking(defaultFirstName, defaultEndDate, defaultStartDate, defaultLastName) == -1); // Start day after end day
		
		assertTrue(hrp.addRoomToBooking("Default", bookingID));
		assertFalse(hrp.addRoomToBooking("NotARoom", 1337));
		assertTrue(hrp.getBooking("Default", defaultStartDate, defaultEndDate).size() > 0);
		assertFalse(hrp.getBooking("Default", dateLongInFuture1, dateLongInFuture2).size() > 0);
		
		
	}
	
	@Test
	public void searchFreeRooms() { //Use case 2.1.2
		assertTrue(!hrp.getFreeRooms(2, defaultStartDate,defaultEndDate).isEmpty());
		assertTrue(hrp.getFreeRooms(2, "NotADay", "notAnotherDay").isEmpty()); // Test with invalid Dates
		assertTrue(hrp.getFreeRooms(1000, defaultStartDate, defaultEndDate).isEmpty()); // Test with too many beds
	}
	
	@Test
	public void checkInBooking() { //Use case 2.1.3		
		int bookingID = hrp.initiateBooking(defaultFirstName, defaultStartDate,defaultEndDate,defaultLastName);
		hrp.addRoomToBooking("Default", bookingID);
		hrp.confirmBooking(bookingID);
		assertTrue(hrp.checkInBooking(bookingID));
		assertFalse(hrp.checkInBooking(bookingID));
		
		assertFalse(hrp.checkInBooking(-1)); // invalid bookingID

	}
	
	@Test
	public void checkOutBookingTest() {// Use case 2.1.4
		int bookingID = hrp.initiateBooking(defaultFirstName, defaultStartDate,defaultEndDate,defaultLastName);
		hrp.addRoomToBooking("Default", bookingID);
		hrp.confirmBooking(bookingID);
		hrp.checkInBooking(bookingID);
		assertTrue(hrp.checkOutBooking(bookingID));
		assertFalse(hrp.checkOutBooking(bookingID));
		
		assertFalse(hrp.checkOutBooking(-1)); //Invalid bookingID
		
	}
	
	@Test 
	public void editBookingTest() { // Use case 2.1.5
		int bookingID = hrp.initiateBooking(defaultFirstName, defaultStartDate,defaultEndDate,defaultLastName);
		hrp.checkInBooking(bookingID);
		assertTrue(hrp.updateDateOfBooking(bookingID, "161219", defaultEndDate));
		assertTrue(hrp.updateNameOfBooking(bookingID, "Name2", defaultLastName));
		assertFalse(hrp.updateNameOfBooking(-1, defaultFirstName, defaultLastName)); // invalid bookingID
	}
	@Test 
	public void cancelBookingTest() { // Use case 2.1.6
		int bookingID = hrp.initiateBooking(defaultFirstName, defaultStartDate,defaultEndDate,defaultLastName);
		hrp.addRoomToBooking("Default", bookingID);
		hrp.confirmBooking(bookingID);
		assertTrue(hrp.cancelBooking(bookingID));
		assertFalse(hrp.cancelBooking(bookingID)); // Already Cancelled
		
		assertFalse(hrp.cancelBooking(-1)); //Invalid ID
	}
	
	@Test 
	public void listBookingsTest() { // Use case 2.1.7
		int numOfBookingsBeforeAdd = hrp.listBookings().size();
		int bookingID = hrp.initiateBooking(defaultFirstName, defaultStartDate,defaultEndDate,defaultLastName);
		assertTrue(hrp.listBookings().size() == numOfBookingsBeforeAdd+1);

	}
	@Test
	public void listOccupiedRoomsForDateTest() { // Use case 2.1.8
		int bookingID = hrp.initiateBooking(defaultFirstName, defaultStartDate,defaultEndDate,defaultLastName);
		hrp.addRoomToBooking("Default", bookingID);
		assertTrue(hrp.addRoomToBooking("Default", bookingID));
		hrp.confirmBooking(bookingID);
		
		for(TwoIntTuple tuple: hrp.getOccupiedRooms(defaultStartDate)) {
			if(tuple.getI1() == bookingID) {
				assertTrue(tuple.getI2() == 0);
			}
		}
		
		hrp.checkInRoom("Default", bookingID);
		
		for(TwoIntTuple tuple: hrp.getOccupiedRooms(defaultStartDate)) {
			if(tuple.getI1() == bookingID) {
				assertTrue(tuple.getI2() == 1);
			}
		}
		
		assertTrue(hrp.getOccupiedRooms(dateLongInFuture1).isEmpty()); //Day in the future
		
		assertTrue(hrp.getOccupiedRooms("notADay").isEmpty()); //Invalid Day

	}
	
	@Test 
	public void listCheckInsForDayTest() { // Use case 2.1.9
		int bookingID = hrp.initiateBooking(defaultFirstName, defaultStartDate,defaultEndDate,defaultLastName);
		hrp.addRoomToBooking("Default", bookingID);
		hrp.confirmBooking(bookingID);
		
		assertTrue(!hrp.listCheckIns(defaultStartDate).contains(bookingID)); // Should not be here since has not checked in		
		hrp.checkInBooking(bookingID);
		assertTrue(hrp.listCheckIns(defaultStartDate).contains(bookingID)); // Should be here 
		
		assertTrue(hrp.listCheckIns(dateLongInFuture1).isEmpty()); // No checkins at a day long in the future
		
		assertTrue(hrp.listCheckIns("notADay").isEmpty()); // invalid Day
	}
	
	@Test 
	public void listCheckOutsForDayTest() { //Use case 2.1.10
		int bookingID = hrp.initiateBooking(defaultFirstName, defaultStartDate,defaultEndDate,defaultLastName);
		hrp.addRoomToBooking("Default", bookingID);
		hrp.confirmBooking(bookingID);
		hrp.checkInBooking(bookingID);
		hrp.checkOutBooking(bookingID);
		assertTrue(!hrp.listCheckOuts(defaultEndDate).isEmpty()); 
		assertTrue(hrp.listCheckOuts(defaultStartDate).isEmpty()); // No checkins at checkin day (in our tests)
		
		assertTrue(hrp.listCheckOuts("notADay").isEmpty()); // invalid Day
	}
	
	@Test 
	public void checkInRoomTest() { // Use case 2.1.11
		int bookingID = hrp.initiateBooking(defaultFirstName, defaultStartDate,defaultEndDate,defaultLastName);
		hrp.addRoomToBooking("Default", bookingID);
		hrp.confirmBooking(bookingID);
		assertTrue(hrp.checkInRoom("Default", bookingID) != -1);
		
		assertTrue(hrp.checkInRoom("Default", bookingID) == -1 ); // booking only has one room, should not be able to check in two rooms
		
		assertTrue(hrp.checkInRoom("NotARoomType", bookingID) == -1); //Not a valid room Type
	}
	
	@Test 
	public void checkOutAndPayRoomTest() { // Use case 2.1.12
		int bookingID = hrp.initiateBooking(defaultFirstName, defaultStartDate,defaultEndDate,defaultLastName);
		hrp.addRoomToBooking("Default", bookingID);
		hrp.confirmBooking(bookingID);
		int roomNumber = hrp.checkInRoom("Default", bookingID);
		double sum = hrp.initiateRoomCheckout(roomNumber, bookingID); // shady error with this one 
		  
		  try { //Connects to payment service. Only works through Chalmers network. Prints balance and deposits money.
				se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires bankingAdmin = se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires.instance();
				double resultBalance = bankingAdmin.makeDeposit(defaultCardNumber, defaultCCVNumber, 12, 19, defaultFirstName,
						defaultLastName, sum);
				if (resultBalance != -1.0) {
					System.out.println("Successfully made a deposit: " + resultBalance);
				} else {
					System.out.println("Error while making deposit.");
				}
			} catch (SOAPException e) {
				System.err.println("Error occurred while communicating with the bank administration");
				//e.printStackTrace();
			}
		
		assertTrue(hrp.payRoomDuringCheckout(roomNumber, defaultCardNumber, defaultCCVNumber, 12, 19, defaultFirstName, defaultLastName));		
		assertFalse(hrp.payRoomDuringCheckout(-1, defaultCardNumber, defaultCCVNumber, 12, 19, defaultFirstName, defaultLastName)); // Invalid r
		assertFalse(hrp.payRoomDuringCheckout(roomNumber+1, defaultCardNumber, defaultCCVNumber, 12, 19, defaultFirstName, defaultLastName)); // Invalid r
		
	}
	
	@Test 
	public void addExtraCostToRoomTest() { // Use Case 2.1.13
		int bookingID = hrp.initiateBooking(defaultFirstName, defaultStartDate,defaultEndDate,defaultLastName);
		hrp.addRoomToBooking("Default", bookingID);
		hrp.confirmBooking(bookingID);
		int roomNumber = hrp.checkInRoom("Default", bookingID);
		assertTrue(hrp.addExtraCostToRoom(bookingID, roomNumber, "Default", 10));
		assertFalse(hrp.addExtraCostToRoom(bookingID, 10000, "Default", 10)); // Invalid roomNumber
		// assertFalse(hrp.addExtraCostToRoom(bookingID, roomNumber, "notARoomType", 10)); // The string should represent the cost thats added! (This's wrong -> Invalid RoomType),
		
	}	
	
	
}
