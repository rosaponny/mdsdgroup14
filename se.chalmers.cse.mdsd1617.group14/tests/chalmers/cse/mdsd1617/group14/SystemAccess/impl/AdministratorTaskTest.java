package chalmers.cse.mdsd1617.group14.SystemAccess.impl;

import static org.junit.Assert.*;

import org.eclipse.emf.common.util.BasicEList;
import org.junit.Before;
import org.junit.Test;

import chalmers.cse.mdsd1617.group14.BookingSystem.Booking.BookingFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.IRoomProvider;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomFactory;
import chalmers.cse.mdsd1617.group14.BookingSystem.Room.RoomStatus;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.Feature;
import chalmers.cse.mdsd1617.group14.BookingSystem.RoomType.RoomTypeFactory;
import chalmers.cse.mdsd1617.group14.SystemAccess.SystemAccessFactory;

public class AdministratorTaskTest {
	
	private HotelAdministratorProvidesImpl hap;
	private IRoomProvider rp;
	

	@Before
	public void initialization() {
		hap = (HotelAdministratorProvidesImpl) SystemAccessFactory.eINSTANCE.createHotelAdministratorProvides();
		rp = RoomFactory.eINSTANCE.createRoomProvider();
	}

	@Test
	public void startUpTest() { // Use Case 2.2.9
		hap.startup(10);
		assertTrue(hap.getIroomprovider().listRooms().size() == 10); // should have the number of specified rooms
		
	}
	@Test 
	public void addRoomTypeTest() { //Use Case 2.2.1
		assertTrue(hap.addRoomType("RoomType1", 10, 2, new BasicEList<Feature>()));
		
		assertFalse(hap.addRoomType("NotValid", -1, 10, new BasicEList<Feature>())); //Invalid price
		
		assertFalse(hap.addRoomType("NotValid2", 10, -1, new BasicEList<Feature>())); //Invalid number of beds
		
		assertTrue(hap.getIroomtypeprovider().listRoomTypes().contains("RoomType1"));
		assertFalse(hap.getIroomtypeprovider().listRoomTypes().contains("NonExistingRoomType")); // should not contain type that has not been added
		
	}
	
	@Test 
	public void updateRoomTypeTest()  {//Use Case 2.2.2
		hap.addRoomType("RoomType1", 10, 2, new BasicEList<Feature>());
		assertTrue(hap.updateRoomType("RoomType1", "RoomType2", 10, 2,  new BasicEList<Feature>()));
		assertFalse(hap.updateRoomType("NotARoomType", "Not", 10, 2, new BasicEList<Feature>())); //Non existing roomtype
		
		assertFalse(hap.getIroomtypeprovider().listRoomTypes().contains("RoomType1"));
		assertTrue(hap.getIroomtypeprovider().listRoomTypes().contains("RoomType2")); 
		
		
	}

	@Test
	public void removeRoomTypeTest() { //Use Case 2.2.3
		hap.addRoomType("RoomType2", 10, 2, new BasicEList<Feature>());
		assertTrue(hap.removeRoomType("RoomType2")); 
		assertFalse(hap.removeRoomType("NotARoomType"));
		
		assertFalse(hap.getIroomtypeprovider().listRoomTypes().contains("RoomType2")); // Should be removed
	}
	
	@Test 
	public void addRoomTest() { // Use case 2.2.4
		hap.addRoomType("RoomType1", 10, 2, new BasicEList<Feature>());
		assertTrue(hap.addRoom(1, "RoomType1"));
		assertFalse(hap.addRoom(1, "RoomType1")); //Should only be able to add rooms with nonexisting roomNumbers
		assertFalse(hap.addRoom(2, "NotARoomType")); // Non existing room type
	}
	
	@Test
	public void changeTypeOfRoom() { // USe case 2.2.5
		hap.addRoomType("RoomType1", 10, 2, new BasicEList<Feature>());
		hap.addRoomType("RoomType2", 10, 2, new BasicEList<Feature>());
		hap.addRoom(1, "RoomType1");
		hap.setTypeOfRoom(1, "RoomType2");
		assertTrue(hap.getTypeOfRoom(1).equals("RoomType2"));
		hap.setTypeOfRoom(1, "NotExistingRoomType");
		assertFalse(hap.getTypeOfRoom(1).equals("NotExistingRoomType"));
		
	}
	
	@Test 
	public void removeRoomTest() { // Use case 2.2.6
		assertTrue(hap.removeRoom(1));
		assertFalse(hap.removeRoom(1)); // Should already be removed
		assertFalse(hap.removeRoom(100)); //Non Existing room number;
	}
	
	@Test
	public void blockRoomTest() { // Use case 2.2.7
		hap.addRoomType("RoomType1", 10, 2, new BasicEList<Feature>());
		hap.addRoom(1, "RoomType1");
		hap.blockRoom(1);
		assertTrue(rp.getStatus(1) == RoomStatus.BLOCKED);
	}
	
	@Test 
	public void unBlockRoomTest() { // Use Case 2.1.8
		hap.addRoomType("RoomType1", 10, 2, new BasicEList<Feature>());
		hap.addRoom(1, "RoomType1");
		hap.blockRoom(1);
		hap.unblockRoom(1);
		assertTrue(rp.getStatus(1) == RoomStatus.FREE);
	}
	
	

}
